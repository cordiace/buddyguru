<?php
/**
 * Created by PhpStorm.
 * User: Cordiace
 * Author URL : http://cordiace.com
 * Date: 15-03-2022
 * Time: 14:40
 */

 /* ------------------------------------------------------
  * | HTML HELPERS
  * ------------------------------------------------------ */

 if ( ! function_exists('date_compare')) {
     /**
      * @param int $count
      *
      * @return string
      */
      function date_compare($element1, $element2) {
          $datetime1 = strtotime($element1['date']);
          $datetime2 = strtotime($element2['date']);
          return $datetime1 - $datetime2;
      }
 }

 if(! function_exists('get_upcoming_date')){
   /**
    * @param array $sessions
    *
    * @return string
    */
   function get_upcoming_date($sessions)
   {
     foreach ($sessions as $key => $value) {
       $today = strtotime(date("Y-m-d"));
       if($today < strtotime($value['date']))
       {
         return $key;
       }
     }
   }
 }

 if ( ! function_exists('batch_date_compare')) {
     /**
      * @param int $count
      *
      * @return string
      */
      function batch_date_compare($element1, $element2) {
          $datetime1 = strtotime($element1['start']);
          $datetime2 = strtotime($element2['start']);
          return $datetime1 - $datetime2;
      }
 }

 if(! function_exists('get_upcoming_date_batch')){
   /**
    * @param array $sessions
    *
    * @return string
    */
   function get_upcoming_date_batch($batches)
   {
     foreach ($batches as $key => $value) {
       $today = strtotime(date("Y-m-d"));
       if($today < strtotime($value['start']))
       {
         return $key;
       }
     }
   }
 }


 if(! function_exists('nb_mois')){
   /**
    * @param array $sessions
    *
    * @return string
    */
    function nb_mois($date1, $date2)
    {
      $begin = new DateTime( $date1 );
      $end = new DateTime( $date2 );
      // $end = $end->modify( '+1 month' );

      $interval = DateInterval::createFromDateString('1 month');

      $period = new DatePeriod($begin, $interval, $end);
      $counter = 0;
      foreach($period as $dt) {
        $counter++;
      }

      return $counter;
    }
  }
