@extends('layouts.app')

@section('content')

<!-- Login HTML Starts Here -->
<div class="login-warp my-n4" @isset($banner) style="background-image: url({{asset('storage/admin-banner/'.$banner->image)}})" @endisset>
   <div class=" container">
      <div class="row ">
         <div class="col-md-12  col-12">
            <div class="login-warp-in">
               <h3 class="">Hi, Welcome Back!</h3>
               <div class="form-wrap">
                  <h2 class="main-head">{{ __('Login') }}</h2>
                  {{--<label class="control-label">Don't have an account <a href="{{route('register')}}">sign up</a> </label>--}}
                  <!-- Form Starts Here -->
                  <form method="POST" action="{{ route('login') }}" class="conatctform financeform">
                    @csrf
                     <div class="row">
                        <div class="col-md-12 mt-3">
                            <div class="form-group row">
                                <label for="email" class="col-md-4 px-0 col-form-label">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="pl-2 form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mt-3">
                            <div class="form-group row">
                                <label for="password" class="col-md-4  px-0 col-form-label">{{ __('Password') }}</label>
                                <input id="password" type="password" class="pl-2 form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                     </div>
                     <a href="{{route('common.terms')}}">Terms and conditions</a>
                     <a href="{{route('common.policy')}}">Privacy policy</a>
                     <div class="row">
                        <div class="col-md-12 mt-3">
                            <div class="form-group row">
                                <button type="submit" class="btn btn-primary form-control contact-sub">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </form>
                  <!-- Form Ends Here -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Login HTML End Here -->
@endsection
