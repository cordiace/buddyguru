@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">school</i>
            </div>
            <p class="card-category">Total Buddy</p>
            <h3 class="card-title">{{count($users)}}
            </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">record_voice_over</i>
            </div>
            <p class="card-category">Total Gurus</p>
            <h3 class="card-title">{{count($tutors)}}</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="material-icons">assessment</i>
            </div>
            <p class="card-category">Total Classes</p>
            <h3 class="card-title">{{count($sessions)}}</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="card card-chart">
          <div class="card-header card-header-success">
            <div class="ct-chart" id="completedTasksChart"></div>
          </div>
          <div class="card-body">
            <h4 class="card-title">Buddy Registration</h4>
          </div>
          <div class="card-footer">
            <div class="stats">
              {{-- <i class="material-icons">access_time</i> Last Registration {{date('d-m-Y H:i A',strtotime($last_student->created_at))}} --}}
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-chart">
          <div class="card-header card-header-warning">
            <div class="ct-chart" id="websiteViewsChart"></div>
          </div>
          <div class="card-body">
            <h4 class="card-title">Guru Registration</h4>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">access_time</i> last Registration {{date('d-m-Y H:i A',strtotime($last_tutor->created_at))}}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      {{-- <div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-warning" style="background: #048b92 !important;">
            <h4 class="card-title">Buddy Details</h4>
            <p class="card-category">New students</p>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
              <tr><th>ID</th>
                <th>Name</th>
                <th>Country</th>
              </tr></thead>
              <tbody>
              @if(isset($latest_students))
                @php
                  $i=1;
                @endphp
              @foreach($latest_students as $latest_student)
              <tr>
                <td>{{$i}}</td>
                <td>{{$latest_student->name}}</td>
                <td>{{$latest_student->location}}</td>
              </tr>
              @php
                $i++;
              @endphp
              @endforeach
              @endif

              </tbody>
            </table>
          </div>
        </div>
      </div> --}}
      {{--<div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-warning">
            <h4 class="card-title">Guru Details</h4>
            <p class="card-category">New tutors</p>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
              <tr><th>ID</th>
                <th>Name</th>
                <th>Country</th>
              </tr></thead>
              <tbody>
              @if(isset($latest_tutors))
                @php
                  $i=1;
                @endphp
                @foreach($latest_tutors as $latest_tutor)
                  <tr>
                    <td>{{$i}}</td>
                    <td>{{$latest_tutor->name}}</td>
                    <td>{{$latest_tutor->location}}</td>
                  </tr>
                  @php
                    $i++;
                  @endphp
                @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>--}}
    </div>
  </div>
</div>
@endsection
