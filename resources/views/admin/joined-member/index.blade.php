@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Joined Members</h4>
            <p class="card-category"> Here is a list of members</p>
            <!-- <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="">
                            <i class="material-icons">supervisor_account</i> Add
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul> -->

          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Date</th>
                  <th>From</th>
                  <th>Name</th>
                  <th>Email</th>
                  <!-- <th>Phone</th> -->
                  <th>Subject</th>
                  <th>Message</th>
                  <!-- <th class="text-right">Action</th> -->
                </thead>
                <tbody>
                  @isset($joinedMembers)
                  @foreach($joinedMembers as $joinedMember)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$joinedMember->created_at}}</td>
                    <td>{{($joinedMember->user_id) ? $joinedMember->user->name : 'Main Web site'}}</td>
                    <td>{{$joinedMember->name}}</td>
                    <td>{{$joinedMember->email}}</td>
                    <!-- <td>{{$joinedMember->phone}}</td> -->
                    <td>{{$joinedMember->subject}}</td>
                    <td>{{$joinedMember->message}}</td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".delete-banner" ).on( "click", function(e) {
    e.preventDefault();
    $('#deleteBanner').attr('action',$(this).data("route"));
    $('#deleteBanner').attr('method','POST');
    $('#delete-banner').modal('show');
    });
});
</script>
@endsection
