@php
    $issetAbout = isset($about) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Home About Us
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('admin.about.update') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{ isset($about->id) ? $about->id : ''  }}" >
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Title</label>
                    <input required type="text" name="title" class="form-control" value="{{ isset($about->title) ? $about->title : ''  }}">
                    @error ('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <textarea required class="form-control" name="description">{{ isset($about->description) ? $about->description : ''  }}</textarea>
                    @error ('description')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Additional text</label>
                    <input required type="text" name="additional_text" class="form-control" value="{{ isset($about->additional_text) ? $about->additional_text : ''  }}">
                    @error ('additional_text')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">(197 x 255) First image: {{isset($about->image1) ? $about->image1 :'' }}</label>
                    <input type="file" name="image1" class="form-control">
                    <input type="hidden" name="image1_temp" value="{{isset($about->image1) ? $about->image1 :'' }}">
                    @error ('image1')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">(122 x 122) Second image: {{isset($about->image2) ? $about->image2 :'' }}</label>
                    <input type="file" name="image2" class="form-control">
                    <input type="hidden" name="image2_temp" value="{{isset($about->image2) ? $about->image2 :'' }}">

                    @error ('image2')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">(205 x 223) Third image: {{isset($about->image3) ? $about->image3 :'' }}</label>
                    <input type="file" name="image3" class="form-control">
                    <input type="hidden" name="image3_temp" value="{{isset($about->image3) ? $about->image3 :'' }}">
                    @error ('image3')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <!-- Short Links -->
              <button type="submit" class="btn btn-primary btn-brown pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-category text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>

  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".add" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( ".add_social" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another_social_link').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="social_name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="social_link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( ".add_short" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another_short_link').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="short_name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="short_link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( "body" ).on( "click",".remove", function(e) {
    e.preventDefault();
    $(this).parent().parent().parent().remove();
  });
});
</script>
@endsection
