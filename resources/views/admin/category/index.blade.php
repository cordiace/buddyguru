@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            {{--<h4 class="card-title ">Categories</h4>--}}
            <ul class="nav nav-tabs" data-tabs="tabs">
              <li class="nav-item">
                <a class="nav-link active" href="{{route('category.create')}}" >
                  <i class="material-icons">add</i> Add Category Set
                  <div class="ripple-container"></div>
                  <div class="ripple-container"></div></a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="">
            <table id="example1" class="table table-bordered table-striped">
                <thead >
                  <th>ID</th>
                  <th>Skills Categories</th>
                  <!-- <th>Image</th> -->
                  <th class="text-center" >Action</th>
                </thead>
                <tbody>
                  @isset($categories)
                  @foreach($categories as $category)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$category->category_name}}</td>
                    <!-- <td><img src="{{url('/'.$category->image)}}" width="50" height="50"/></td> -->
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                      <a href="{{ route('category.edit', $category->id) }}" rel="tooltip" title="Edit Category" class="btn btn-primary  btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Edit Category" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-edit"></i>
                        <!-- </button> -->
                      </a>
                      <a href="{{ route('category.delete', $category->id) }}" rel="tooltip" title="Delete Category" class="btn btn-danger btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Delete Category" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-trash"></i>
                        <!-- </button> -->
                      </a>
</div>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
