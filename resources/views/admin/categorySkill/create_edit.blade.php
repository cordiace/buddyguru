@php
    $formAction = isset($skill) ? route('skill.update', $skill->id) : route('skill.store');
    $issetSkill = isset($skill) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
              @isset($skill)
                  Edit Skill Details
              @else
                  Add New Skill<Tutor></Tutor>
              @endisset
            </h4>
            {{--<p class="card-skill">Complete your profile</p>--}}
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($skill) @method('PUT') @endisset
              @csrf
              <div class="row">

              <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Category Type</label>
                    <select name="category_type" class="form-control">
                    @isset($category)
                      @foreach($category as $value)
                      <!-- <option value="{{$value->id}}">{{strtoupper($value->category_name)}}</option> -->
                      <option value="{{ $value['id'] }}"  {{  ($issetSkill && $skill->category_id == $value['id']) || (old('category_type')==$value['id'])  ? 'selected' : ''}}>{{ $value['category_name'] }}</option>
                      @endforeach
                      @endisset
                    </select>
                    @error ('category_type')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
</div>
<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Skill Name</label>
                    <input id="skill_name" type="text" name="skill_name" class="form-control"
                        value="{{ old('skill_name') ?? ($issetSkill ? $skill->skill_name : '')  }}">
                    @error ('skill_name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <a href="{{ route('skill.index') }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary float-left btn-link btn-sm">
                          Back
                        </button>
                      </a>
              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-skill text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>
   
  </div>
</div>
@endsection
