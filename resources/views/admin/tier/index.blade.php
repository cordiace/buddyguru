@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            <!-- <h4 class="card-title ">Users</h4>
            <p class="card-category"> Here is a subtitle for this table</p> -->
            <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="{{route('admin.tier.create')}}" >
                            <i class="material-icons">add</i> Add
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul>

          </div>
          <div class="card-body">
          
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <th>ID</th>
                  <th>Tier</th>
                  <th>Country</th>
                  <th>Currency Type</th>
                  <th class="text-center">Action</th>
                </thead>
                <tbody>
                  @isset($TierManagements)
                  @foreach($TierManagements as $TierManagement)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$TierManagement->tier->name}}</td>
                    <td>{{$TierManagement->country_name}}</td>
                    <td>{{strtoupper($TierManagement->currencySetting->symbol)}}</td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                    <a href="{{ route('admin.tier.delete', $TierManagement->id) }}" rel="tooltip" title="Delete package" class="btn btn-danger btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Delete package" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-trash"></i>
                        <!-- </button> -->
                      </a>
                      <a href="{{ route('admin.tier.edit', $TierManagement->id) }}" rel="tooltip" title="Edit Task" class="btn btn-info btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-edit"></i>
                        <!-- </button> -->
                      </a>
</div>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>

           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".delete-banner" ).on( "click", function(e) {
    e.preventDefault();
    $('#deleteBanner').attr('action',$(this).data("route"));
    $('#deleteBanner').attr('method','POST');
    $('#delete-banner').modal('show');
    });
});
</script>
@endsection
