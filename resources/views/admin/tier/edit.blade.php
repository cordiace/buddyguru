@php
    $formAction = route('admin.tier.update',$Tier->id);
    $issetBanner = isset($TierManagements) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
             
                  Edit Details
             
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($banner) @method('PUT') @endisset
              @csrf
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Tier</label>
                    <select name="tier" class="form-control">
                    @foreach ($Tiers as $key => $value)
                          <option value="{{ $value['id'] }}" {{ $value->id == $Tier->tier_id ? 'selected' : '' }} >{{ $value['name'] }}</option>
                          @endforeach
                    </select>
                    @error ('location')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Country</label>
                    <input name="country" id="heading" class="form-control" value="{{$Tier->country_name }}" required>
                    @error ('country')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Currency Type</label>
                    <select name="currency_type" class="form-control">
                    @isset($currency)
                      @foreach($currency as $value)
                      <option value="{{$value->id}}" {{ $value->id == $Tier->currency_type_id ? 'selected' : '' }}>{{$value->symbol}}</option>
                      @endforeach
                      @endisset
                    </select>
                    @error ('currency_type')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <a href="{{ route('admin.tier.index') }}"  class= "float-left">
                        <button type="button"  class="btn btn-primary float-left btn-link btn-sm">
                          Back
                        </button>
                      </a>

              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @isset($banner)
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this banner
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('banner.destroy', $banner->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection
