@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            {{--<h4 class="card-title ">Categories</h4>--}}
            <ul class="nav nav-tabs" data-tabs="tabs">
              <li class="nav-item">
                <a class="nav-link active" href="{{route('country.create')}}" >
                  <i class="material-icons">add</i> Add Country
                  <div class="ripple-container"></div>
                  <div class="ripple-container"></div></a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <th>ID</th>
                  <th>Region</th>
                  <th>Country</th>
                  <th class="text-center">Action</th>
                </thead>
                <tbody>
                  @isset($countries)
                  @foreach($countries as $country)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$country->region->name}}</td>
                    <td>{{$country->country_name}}</td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                      <a href="{{ route('country.edit', $country->id) }}"  rel="tooltip" title="Edit Country" class="btn btn-info  btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Edit Country" class="btn btn-primary btn-link btn-sm"> -->
                          <i class="material-icons">edit</i>
                        <!-- </button> -->
                      </a>
                      <!-- <a href="{{ route('country.delete', $country->id) }}">
                        <button type="button" rel="tooltip" title="Delete Country" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">delete</i>
                        </button>
                      </a> -->
</div>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
