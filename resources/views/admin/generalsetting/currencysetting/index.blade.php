@php
    $formAction =  route('admin.settings.currencyUpdate');
    $CurrencySetting = isset($CurrencySettings) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            <!-- <h4 class="card-title ">Users</h4>
            <p class="card-category"> Here is a subtitle for this table</p> -->
            <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="{{route('admin.currency_settings.createCurrency')}}" >
                            <i class="material-icons">add</i> Add
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul>

          </div>
          @isset($CurrencySetting)
          <div class="card-body">
            <form action="{{ $formAction }}" id="myform" method="POST" accept-charset="UTF-8">
             
              @csrf

              @foreach ($CurrencySettings as $key => $CurrencySetting)

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">{{strtoupper($CurrencySetting->symbol)}}$(Based on USD)</label>    <label  class="d-flex flex-row justify-content-end"><p class=" p-1 btn btn-outline-primary" style="width:130px;">Live Rate - {{$convertedAmounts[$key]}}</p></label>
                    <input name="commision[{{$CurrencySetting->id}}]" id="commision" class="form-control"  value="{{ old('commision') ?? ($CurrencySetting ? $CurrencySetting->value: '')  }}">  
                  
                    @error ('commision')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                 
                </div>
              </div>
@endforeach
             
<button type="button" id="clear-btn" onclick= "clearInput()" class="btn btn-primary btn-sm float-right">Clear All</button>
              <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
@endisset



        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".delete-banner" ).on( "click", function(e) {
    e.preventDefault();
    $('#deleteBanner').attr('action',$(this).data("route"));
    $('#deleteBanner').attr('method','POST');
    $('#delete-banner').modal('show');
    });
  });


function clearInput(){

  $("input[name^='commision']").val("");
 }
</script>
@endsection
