@php
$issetUser = isset($user) ? 1 : 0;
$issetpackageDetails = count($packageDetails) >0 ? 1 : 0;
    $formAction = route('tutor.package.store',[$id,'showNext'=>$ShowNext]);
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
      @if($ShowNext =='false')
      <div class="row">
          <div class="col-md-12">
      <a href="{{ route('tutor.package.view', $id) }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          Back
                        </button>
                      </a>
</div>
</div>
@endif
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
                  Guru Class
            </h4>
            <p class="card-category">Manage Guru Class</p>
          </div>
          @if (session()->has('message'))
    <div class="col-lg-3 " style="margin: 0 auto;">
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif
          <div class="card-body">
            <form action="{{ $formAction }}" id="form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
              <div class="col-md-6 class_type">
                  <div class="form-group">
                    <!-- <label class="bmd-label-floating">Select Class</label> -->
                    
                    <select id="batch_id"  type="hidden" name="batch_id" class="form-control" hidden>
                      @isset($userClasses)
                      @foreach($userClasses as $value)
                      <option value="{{$value->class_id}}">{{$value->class_name}}</option>
                      @endforeach
                      @endisset
                    </select>
                    
                    
                    @error('batch_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                  <!-- <div style="font-size: 12px;"> Not required,You can add it, Manage batch is in Guru Actions (Previous Menu).</div> -->
                </div>
              </div>
              <div class="row">
            
            
            
                <div class="col-md-4">
                  <div class="form-group">
                  
                    <label class="bmd-label-floating">Intro Title <span class="text-danger">*</span></label>
                    <input id="intro_title" type="text" name="intro_title" class="form-control"
                        value=""required>
                        <!-- <textarea id="intro_title"  rows="2"  name="intro_title" class="form-control-sm"
                        value="" required></textarea> -->
                        @error ('intro_title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>  
                
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Intro Description</label>
                    <textarea id="intro_description" cols="70" rows="5"  name="intro_description" class="form-control"
                        value="" ></textarea>
                    @error('intro_description')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
             
                <div class="col-md-3" id="introVideo" style="display:block">
                  <div class="form-group">
                  <label class="bmd-label-floating">Trailer <span class="text-danger">*</span></label>
                  <div class="custom-file">
                        <input type="file"  name="intro_video" class="custom-file-input" id="intro_video" required>
                        <label class="custom-file-label" for="intro_video">Choose file</label>
                      </div>
                  <!-- <input id="intro_video" type="file" name="intro_video" class="form-control"
                        value="" required> -->

                    @error ('intro_video')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div id="valid_intro_video" style="font-size: 13px;display: block;"><p class="text-danger">.MP4,.JPEG(not More than 1GB)</p></div>
                  <button type="button" id= "TrailerUpload" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar_trailer" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process_trailer" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image_trailer" class="alert  success-upload_trailer alert-dismissible fade show row mt-1" role="alert">  </div>
                </div>
</div>

 <div class="multiple" id="multiple">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}" required> 
                    <label class="bmd-label-floating">Title <span class="text-danger">*</span></label>
                    <input id="title" type="text" name="title[]" class="form-control"
                        value=""required>
                        @error ('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description <span class="text-danger">*</span></label>
                    <textarea id="description" cols="70" rows="5"  name="description[]" class="form-control"
                        value="" ></textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                    
                <div class="col-md-3" id="video1" style="display:block">
                  <div class="form-group">
                    <label class="bmd-label-floating">Video <span class="text-danger">*</span></label>
                    <div class="custom-file">
                        <input type="file"  name="video_url[]" class="custom-file-input" id="video_url" required>
                        <label class="custom-file-label" for="video_url">Choose file</label>
                      </div>
                    <!-- <input id="video_url" type="file" name="video_url[]" class="form-control"
                        value="" required> -->
                    @error ('video_url')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                    <div id="valid_video_url" style="font-size: 13px;display: block;"><p class="text-danger">.MP4,.JPEG(not More than 1GB)</p></div>
                    <button type="button" id= "upload" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert">  </div>
                  </div>
                </div>
                            
              
</div></div>
<div class="row">
<div class="col-md-2">
<div class="form-group">
<button type="button" name="add" id="add-btn" class="btn btn-primary  btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
<i class="material-icons">add</i>Add More</button>
</div>
</div>
</div>
<div class="row">

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Total Price(USD) <span class="text-danger">*</span></label>
                    <input id="total_price" type="number" name="total_price" class="form-control"
                    value=""required>
                    @error('total_price')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
</div>
                {{-- <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Subscription Fee</label>
                    <input id="subscription_fee" type="number" name="subscription_fee" class="form-control"
                    value="">
                    @error('subscription_fee')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div> --}}

               
              
                @if($ShowNext =='true')
                <!-- <a href="{{ route('tutor.portfolio.create',['id' =>$id,'showNext'=>'true']) }}" class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Previous
                        </button>
                      </a> -->
                      <a href="{{ route('tutor.module.create',['id' =>$id,'showNext'=>'true']) }}"  class= "float-right">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Skip
                        </button>
                      </a>
                  <button type="submit" id ="submit" name="submit_button" class="btn btn-primary btn-sm  float-right" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">Next</button>
              <!-- <button type="submit" class="btn btn-primary pull-right">{{($issetpackageDetails) ? 'Submit' : 'Next'}}</button> -->
              @else
              <!-- <a href="{{ route('tutor.package.view', $id) }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                        </button>
                      </a> -->
                     <button type="submit" id ="submit" name="submit_button" class="btn btn-primary btn-sm float-right" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">Submit</button>
              @endif
            </form>
           
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
  
   
    {{-- @isset($user)
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this Guru
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('tutor.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset --}}
  </div>
</div>

@endsection

@section('custom-scripts')
  <script type="text/javascript">
   window.addEventListener("DOMContentLoaded", function(e) {

var form_being_submitted = false;

var checkForm = function(e) {
  var form = e.target;
  if(form_being_submitted) {
    alert("The form is being submitted, please wait a moment...");
    form.submit_button.disabled = true;
    e.preventDefault();
    return;
  }
  form.submit_button.value = "Submitting form...";
      form_being_submitted = true;
    };
    document.getElementById("form").addEventListener("submit", checkForm, false);

  }, false);
      $( document ).ready(function() {
        $('.alert-success').fadeIn().delay(10000).fadeOut();
      



          document.getElementById('intro_video').addEventListener("change", function (e) {
            var fileName = document.getElementById("intro_video").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid_intro_video').hide();
           $('#TrailerUpload').show();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById("intro_video").value = null;
          
        }

});
$(document).on('click', '#TrailerUpload', function() {
  $('#TrailerUpload').hide();
  var file_element = document.getElementById('intro_video');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar_trailer');
var progress_bar_process = document.getElementById('progress_bar_process_trailer');

var uploaded_image = document.getElementById('uploaded_image_trailer');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo',$id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="alert alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload_trailer').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});
// document.getElementById('intro_video').addEventListener("change", function (e) {

// $('#valid_intro_video').hide();
// });

    document.getElementById('video_url').addEventListener("change", function (e) {


      var fileName = document.getElementById("video_url").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid_video_url').hide();
$('#upload').show();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/webm/mkv  files are allowed!");
            document.getElementById("video_url").value = null;
          
        }

  
});

$(document).on('click', '#upload', function() {

$('#upload').hide();
// let file_element =  $('#video_url').val();
var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar');
var progress_bar_process = document.getElementById('progress_bar_process');

var uploaded_image = document.getElementById('uploaded_image');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo',$id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="alert alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

});
   
      });
      var doc= '';


// $(document).ready(function() {
// $(document).on('click', '#upload'+count+'', function() {


// });
// }); 
var count ='';
      function getFile(val,getId){
        var a = document.getElementById(getId);
        var label = document.getElementById('fileLabel'+getId+'');
    if(a.value == "")
    {
      label.innerHTML = "Choose file";
    }
    else
    {
        var theSplit = a.value.split('\\');
        label.innerHTML = theSplit[theSplit.length-1];
    }
    count = getId;
    var fileName = document.getElementById(getId).value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#multiplevideo'+count+'').hide();
  $('#upload'+count+'').show();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById(getId).value = null;
            label.innerHTML = "Choose file";
          
        }

  console.log(getId);


}  

function fileUpload(val){
  count=val;
$('#upload'+count+'').hide();
// let file_element =  $('#video_url').val();
var file_element = document.getElementById(count);
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar'+count+'');
var progress_bar_process = document.getElementById('progress_bar_process'+count+'');

var uploaded_image = document.getElementById('uploaded_image'+count+'');

console.log("uploaded_image");
console.log(uploaded_image);
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('package.StoreVideo',$id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

  var percent_completed = Math.round((event.loaded / event.total) * 100);

  progress_bar_process.style.width = percent_completed + '%';

  progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div id="success'+count+'" class="alert alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('#success'+count+'').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
}
      document.getElementById('batch_id').addEventListener("change", function (e) {
            const classType =  $('#batch_id').val();
      });
      document.getElementById('batch_id').addEventListener("change", function (e) {
            const type =  $('#batch_id').val();
            // clearFields();
           if(type == 2)
            $('#add-btn').show();
           else
           $('#add-btn').hide();
        
          });
function clearFields(event){
  document.getElementById('title').value = '';
  document.getElementById('description').value = '';
  document.getElementById('video_url').value = '';
  document.getElementById('total_price').value = '';
  document.getElementById('class_type').value = '';
  document.getElementById('start_date').value = '';
  document.getElementById('end_date').value = '';
  document.getElementById('start_time').value = '';
  document.getElementById('end_time').value = '';
  document.getElementById('duration').value = '';
  document.getElementById('meeting_link').value = '';
  document.getElementById('no_of_class').value = '';
}

      
$(document).on('click', '#add-btn', function() {
addrow();

      });
     
      var i = 0;
      function addrow(){
       
        i++;

        var tr ='<div class="sub">'+
        '<div class="dynamic-border" >'+
        '<div class="row">'+
                '<div class="col-md-4">'+
                  '<div class="form-group">'+
                    '<input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}">'+
                    '<label class="bmd-label-floating">Title <span class="text-danger">*</span></label>'+
                    '<input id="title" type="text" name="title[]" class="form-control"'+
                        'value=""required>'+
                   '@error ('title')'+
                        '<p class="text-danger">{{ $message }}</p>'+
                    '@enderror'+
                  '</div>'+
                '</div>'+

                '<div class="col-md-5">'+
                  '<div class="form-group">'+
                    '<label class="bmd-label-floating">Description <span class="text-danger">*</span></label>'+
                    '   <textarea id="description"  cols="70" rows="5" name="description[]" class="form-control"'+
                       ' value="" ></textarea>'+
                    '@error('description')'+
                    '<span class="invalid-feedback" role="alert">'+
                                '<strong>{{ $message }}</strong>'+
                            '</span>'+
                    '@enderror'+
                  '</div>'+
                '</div>'+     

                '<div class="col-md-3"  style="display:block">'+
                  '<div class="form-group">'+
                    '<label class="bmd-label-floating">Video <span class="text-danger">*</span></label>'+
                    '<div class="custom-file">'+
                        '<input id="'+i+'" type="file" onchange="return getFile(this.value,this.id);" name="video_url[]" class="form-control" value="" required>'+
                        '<label id="fileLabel'+i+'" class="custom-file-label" for="'+i+'">Choose file</label>'+
                      '</div>'+
                      
                    '@error ('video_url')'+
                        '<p class="text-danger">{{ $message }}</p>'+
                    '@enderror'+
                    '</div>'+
                    '<div id="multiplevideo'+i+'" style="font-size:13px;display:block;"><p class="text-danger">.MP4,.JPEG(not more than 1GB)</p></div>'+
                    
                    '<button type="button" id= "upload'+i+'" value="'+i+'" onclick="fileUpload(this.value)" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">'+
                          '<i class="material-icons">upload</i>'+
'</button>'+
              
                       '<div class="progress" id="progress_bar'+i+'" style="display:none;height:20px; line-height: 20px;">'+

            '<div class="progress-bar" id="progress_bar_process'+i+'" role="progressbar" style="width:0%;">0%</div>'+

        '</div>'+

        '<div class="col-md-1" id="btn_remove">'+
                  '<div class="form-group">'+
                  '<label > </label>'+
                  '<button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-sm remove">'+
                 '<i class="material-icons">delete</i>'+
                        '</button>'+
                '</div >'+
                '</div >'+

        '<div id="uploaded_image'+i+'" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert"></div>'+
                  
                  '</div>'+


                
                '</div >'+
                '</div >'+

                '</div >'  
    $('#multiple').append(tr);
              };
              $('#multiple').on('click','.remove',function() {
                $(this).closest(".sub").remove();
    });
  </script>
@endsection
