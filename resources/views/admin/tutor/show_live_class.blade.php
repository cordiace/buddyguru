@extends('layouts.dashboard')

@section('content')
    @include('partials.nav')
    <div class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-12">
                <div class="row">
                <div class="col-md-12">
         
      <a href="{{ route('tutor.live_class.view', $LiveClass->user_id) }}">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                  </button></a>
</div>
</div>
                    <div class="card">
                        <div class="card-header card-header-info">
                            <h4 class="card-title">
                               Live Class
                            </h4>
                            <p class="card-category">Live Class Details</p>
                        </div>
                       
                        <div class="col-md-12   d-flex align-items-stretch flex-column " style=" margin-left: 143px!important; padding-right: 344px;">
              <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                        <div class="row col-md-12 justify-content-md-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Intro Title</label><br>
                                        {{ $LiveClass->intro_title }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Intro Description</label><br>
                                        {{ $LiveClass->intro_description }}
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12 justify-content-md-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Title</label><br>
                                        {{ $LiveClass->title }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Description</label><br>
                                        {{ $LiveClass->description }}
                                    </div>
                                </div>
                            </div>
                            

                        
<div class="row col-md-12 justify-content-md-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="bmd-label-floating">Trailer</label><br>
<a href="" class="view-portfolio" data-video_url="{{asset($LiveClass->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                    </a>
</div>
</div>

<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Price</label><br>
                                        USD ${{ $LiveClass->price }}
                                    </div>
                                </div>          
                            </div>

                            <div class="row col-md-12 justify-text-md-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Class Type</label><br>
                                        {{ ($LiveClass->class_type==1) ? 'one to one' :'Group' }}
                                    </div>
                                </div>
                                @if($LiveClass->class_type==2)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">No.Of Participants</label><br>
                                        {{ $LiveClass->no_of_participents }}
                                    </div>
                                </div>
                                @endif
                            </div>

                            <div class="row col-md-12 justify-content-md-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Start Date</label><br>
                                        {{ $LiveClass->start_date }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">End Date</label><br>
                                        {{ $LiveClass->end_date }}
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12 justify-content-md-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Start Time</label><br>
                                        {{ $LiveClass->start_time }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">End Time</label><br>
                                        {{ $LiveClass->end_time }}
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-12 justify-content-md-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Duration</label><br>
                                        {{ $LiveClass->duration }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">No Of classes</label><br>
                                        {{ $LiveClass->no_of_class }}
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="row">
          <div class="col-md-12">
          <div class="form-group">
      <a href="{{ route('tutor.live_class.view', $LiveClass->user_id) }}">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                  </button></a>
</div>
</div>
</div> -->

                        </div>
                       


                    </div>
                </div>
            </div>
            </div>
    </div>
        </div>
    </div>
    <div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
 $( document ).ready(function() {
    $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });
 });
    </script>
@endsection