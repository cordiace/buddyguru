@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            <!-- <h4 class="card-title ">Users</h4>
            <p class="card-category"> Here is a subtitle for this table</p> -->
            <ul class="nav nav-tabs primary" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="{{route('tutor.create')}}">
                            <i class="material-icons">supervisor_account</i> Add Guru
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul>

          </div>


          
          <!-- <div class="card-body">
            <div class="table-responsive">
              <table class="table ">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th class="text-right">Action</th>
                </thead>
                <tbody>
                  @isset($users)
                  @foreach($users as $user)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td class="td-actions text-right">
                    <a href="{{ route('admin.tutor.delete', $user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Delete Guru" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">delete</i>
                        </button>
                      </a>
                      <a href="{{ route('admin.tutor.edit', ['id' =>$user->id,'showNext'=>'false']) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Edit Guru" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                      <a href="{{ route('tutor.show', $user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="View Profile" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">assignment_ind</i>
                        </button>
                      </a> -->


                      <!-- <a href="{{ route('tutor.portfolio.create',  ['id' =>$user->id,'showNext'=>'false']) }}">
                        <button type="button" rel="tooltip" title="Manage Intro Video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">play_circle</i>
                        </button>
                      </a> -->

                      <!-- <a href="{{ route('tutor.package.view', $user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Manage Guru Classes" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">school</i>
                        </button>
                      </a>
                      <a href="{{ route('tutor.module.view',$user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Manage Module Classes" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">hotel_class</i>
                        </button>
                      </a>

                      <a href="{{ route('tutor.live_class.view',$user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Manage Live Class" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">live_tv</i>
                        </button>
                      </a> -->
                    <!-- </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div> -->

          <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
                  <thead class="custom">
                    <tr>
                    <th style="width: 8%">ID</th>
                  <th>Name</th>
                  <th style="width: 10%">Tutor</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @isset($users)
                  @foreach($users as $user)
                  <tr>
                    <td >{{$loop->index + 1}}</td>
                    <td>{{$user->name}}</td>
                    <td class="text-center py-0 align-middle"> 
                      <ul class="list-inline">
                              <li class="list-inline-item">
                                  <img alt="Avatar"  class="table-avatar" src="{{ asset($user->profile_image) }}">
                              </li>
                             
                            </ul> </td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                    <a href="{{ route('admin.tutor.delete', $user->id) }}" class="btn btn-danger" rel="tooltip" data-trigger="hover" title="Delete Profile">
                     
                        <i class="fas fa-trash"></i>
                       
                      </a>
                      <a href="{{ route('admin.tutor.edit', ['id' =>$user->id,'showNext'=>'false']) }}" class="btn btn-info" rel="tooltip" data-trigger="hover" title="Edit Profile">
                        <!-- <button type="button" rel="tooltip" data-trigger="hover" title="Edit Guru" class="btn btn-info "> -->
                        <i class="fas fa-edit"></i>
                       
                      </a>
                      <a href="{{ route('tutor.show', $user->id) }}" class="btn btn-primary" rel="tooltip" data-trigger="hover" title="View Profile">
                        <!-- <button type="button" rel="tooltip" data-trigger="hover" title="View Profile" class="btn btn-primary"> -->
                        <i class="fas fa-eye"></i>
                       
                      </a>
                      <!-- <a href="{{ route('tutor.portfolio.create',  ['id' =>$user->id,'showNext'=>'false']) }}">
                        <button type="button" rel="tooltip" title="Manage Intro Video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">play_circle</i>
                        </button>
                      </a> -->

                      <!-- <a href="{{ route('tutor.package.view', $user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Manage Guru Classes" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">school</i>
                        </button>
                      </a>
                      <a href="{{ route('tutor.module.view',$user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Manage Module Classes" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">hotel_class</i>
                        </button>
                      </a>

                      <a href="{{ route('tutor.live_class.view',$user->id) }}">
                        <button type="button" rel="tooltip" data-trigger="hover" title="Manage Live Class" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">live_tv</i>
                        </button>
                      </a> -->
</div>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                  </tbody>
                </table>
              </div>
             
              
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


