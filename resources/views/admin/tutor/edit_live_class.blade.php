@php
$issetUser = isset($user) ? 1 : 0;
$formAction = route('tutor.live_class.update', $packageDetail->id);
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
      <div class="row">
          <div class="col-md-12">
      <a href="{{ route('tutor.live_class.view', $packageDetail->user_id) }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          Back
                        </button>
                      </a>
</div>
</div>
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
                 Edit Live Class
            </h4>
            <p class="card-category">Manage Live Class</p>
          </div>
          @if (isset($res))
    <div class="col-sm-12">
        <div class="alert  alert-danger alert-dismissible fade show" role="alert">
          $res
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif
          <div class="card-body">
            <form action="{{ $formAction }}" id="form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
              <div class="col-md-6 class_type">
                  <div class="form-group">
                    <!-- <label class="bmd-label-floating">Select Class</label> -->
                    
                    <select id="batch_id" name="batch_id" class="form-control" hidden>
                      @isset($userClasses)
                      @foreach($userClasses as $value)
                      <option value="{{$value->class_id}}" {{ $value->class_id == $packageDetail->batch_id ? 'selected' : '' }} >{{$value->class_name}}</option>
                      @endforeach
                      @endisset
                    </select>
                    
                    
                    @error('batch_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  <!-- </div><div style="font-size: 12px;"> Not required,You can add it, Manage batch is in Guru Actions (Previous Menu).</div> -->
                </div>
              </div>
</div>
              
 <div class="main" id="main">
           
<div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                   
                    <label class="bmd-label-floating">Intro Tittle <span class="text-danger">*</span></label>
                    <input id="intro_title" type="text" name="intro_title" class="form-control @error('intro_title') is-invalid @enderror"
                        value="{{$packageDetail->intro_title}}">
                    @error ('intro_title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Intro Description <span class="text-danger">*</span></label>
                    <textarea id="intro_description" rows="5" cols="70"  name="intro_description" class="form-control"
                        value="" >{{$packageDetail->intro_description}}</textarea>
                    <!-- @error('description')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror -->
                  </div>
                </div>
                <div class="col-md-3" id="video1" style="display:block">
                  <div class="form-group">
                    <label class="bmd-label-floating">Trailer <span class="text-danger">*</span></label>
                  
                    <div class="custom-file">
                        <input type="file"  name="video_url" class="custom-file-input" id="video_url" >
                        <label class="custom-file-label" for="video_url">Choose file</label>
                      </div>
                    <!-- <input id="video_url" type="file" name="video_url" class="form-control"
                        value="{{ $packageDetail->video_file_name}}"> -->
                    @error ('video_url')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                    <div id="valid_video_url" style="font-size: 13px;display:block;"><p class="text-danger">.MP4,.JPEG(not More than 1GB)</p></div>
                    <a href="" class="view-portfolio" id="viewTrailerVideo" data-video_url="{{asset($packageDetail->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                  </a>

                  <button type="button" id= "TrailerUpload" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar2" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process2" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image2" class="alert  success-upload2 alert-dismissible fade show row mt-1" role="alert">  </div>

                  </div>
                </div> 
                      
</div>
                <div class="row">
               
                <div class="col-md-4">
                  <div class="form-group"> 
                  <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$packageDetail->user_id}}" required> 
                    <label class="bmd-label-floating">Title <span class="text-danger">*</span></label>
                    <input id="title" type="text" name="title" class="form-control @error('title') is-invalid @enderror"
                        value="{{$packageDetail->title}}">
                    @error ('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
               
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description <span class="text-danger">*</span></label>
                    <textarea id="description" rows="5" cols="70" name="description" class="form-control">{{$packageDetail->description}}</textarea>
                    <!-- @error('description')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror -->
                  </div>
                </div>    
                <div id="classType" class="col-md-3 class_type">
                  <div class="form-group">
                    <label class="bmd-label-floating">Select Class Type <span class="text-danger">*</span></label>
                    
                    <select id="class_type" name="class_type" class="form-control">
                      
                      <option value="1" {{ $packageDetail->class_type == 1 ? 'selected' : '' }}>One To One</option>
                      <option value="2" {{ $packageDetail->class_type == 2 ? 'selected' : '' }}>Group Class</option>
                    
                    </select>
                    
                    
                    @error('class_type')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
        </div>
        </div>      
</div>


<div class="row">
             
        <div id="startDate" class="col-md-3" >
                    <div class="form-group">
                        <label class="bmd-label-floating">Start Date</label>
                        <input id="start_date" type="date" name="start_date" class="form-control"
                               value="{{$packageDetail->start_date}}">
                        @error ('start_date')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div id="endDate" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">End Date</label>
                        <input id="end_date" type="date" name="end_date" class="form-control"
                               value="{{$packageDetail->end_date}}">
                        @error ('end_date')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div id="startTime" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">Start Time</label>
                        <input id="start_time" type="time" name="start_time"  class="form-control timeSet"
                               value="{{$packageDetail->start_time}}">
                        @error ('start_time')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div id="endTime" class="col-md-3" >
                    <div class="form-group">
                        <label class="bmd-label-floating">End Time</label>
                        <input id="end_time" type="time" name="end_time" class="form-control timeSet"
                               value="{{$packageDetail->end_time}}">
                        @error ('end_time')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
</div>

<div class="row">

                <div id="Duration" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">Duration</label>
                        <input id="duration" type="number" name="duration" class="form-control duration"
                               value="{{$packageDetail->duration}}">
                        @error ('duration')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div id="particpents" class="col-md-3 particpents" style="display:none">
                    <div class="form-group">
                        <label class="bmd-label-floating">No.of Participants <span class="text-danger">*</span></label>
                        <input id="particpents" type="number" name="no_of_participents" class="form-control timeSet"
                               value="{{$packageDetail->no_of_participents}}">
                        @error ('no_of_participents')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div id="noOfClass" class="col-md-3">
                    <div class="form-group">
                        <label class="bmd-label-floating">No.of Classes <span class="text-danger">*</span></label>
                        <input id="no_of_class" type="number" name="no_of_class" class="form-control timeSet"
                               value="{{$packageDetail->no_of_class}}">
                        @error ('no_of_class')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Price(USD) <span class="text-danger">*</span></label>
                    <input id="price" type="number" name="price" class="form-control"
                    value="{{$packageDetail->price}}">
                    @error ('price')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                  </div>
                </div>
</div>

<div class="row">
<div id ="meetingLinks" class="col-md-4" style="display:none">
                    <div class="form-group">
                        <label class="bmd-label-floating">Meeting Link</label>
                        <input id="meeting_link" type="text" name="meeting_link"  class="form-control timeSet"
                               value="{{$packageDetail->meeting_link}}">
                        @error ('meeting_link')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
              
</div>


  
<!-- <a href="{{ route('tutor.live_class.view', $packageDetail->user_id) }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                        </button>
                      </a> -->
               
              
                 
              <button type="submit" id="submit" name="submit_button" class="btn btn-primary btn-sm float-right" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">Submit</button>
              
            </form>
          
   

  </div>
</div>
<div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
@endsection

@section('custom-scripts')
  <script type="text/javascript">
   window.addEventListener("DOMContentLoaded", function(e) {

var form_being_submitted = false;

var checkForm = function(e) {
  var form = e.target;
  if(form_being_submitted) {
    alert("The form is being submitted, please wait a moment...");
    form.submit_button.disabled = true;
    e.preventDefault();
    return;
  }
  form.submit_button.value = "Submitting form...";
      form_being_submitted = true;
    };
    document.getElementById("form").addEventListener("submit", checkForm, false);

  }, false);
      $( document ).ready(function() {
        $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });

        const type =  $('#class_type').val();
       // clearFields();
      if(type == 1){
      //  $('#meetingLinks').show();
       $('#particpents').hide();
      }
      else{
      //  $('#meetingLinks').hide();
       $('#particpents').show();
      }

document.getElementById('class_type').addEventListener("change", function (e) {
       
       const type =  $('#class_type').val();
       // clearFields();
      if(type == 1){
      //  $('#meetingLinks').show();
       $('#particpents').hide();
      }
      else{
      //  $('#meetingLinks').hide();
       $('#particpents').show();
      }
    });
  
    
    document.getElementById('video_url').addEventListener("change", function (e) {

      var fileName = document.getElementById("video_url").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid_video_url').hide();
$('#TrailerUpload').show();
$('#viewTrailerVideo').hide();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/webm/mkv  files are allowed!");
            document.getElementById("video_url").value = null;
          
        }

     
});
$(document).on('click', '#TrailerUpload', function() {
 $('#TrailerUpload').hide();
 var file_element = document.getElementById('video_url');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar2');
var progress_bar_process = document.getElementById('progress_bar_process2');

var uploaded_image = document.getElementById('uploaded_image2');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('live.StoreVideo',$packageDetail->user_id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

 var percent_completed = Math.round((event.loaded / event.total) * 100);

 progress_bar_process.style.width = percent_completed + '%';

 progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div class="alert alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('.success-upload2').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});


});
      
  </script>
@endsection
