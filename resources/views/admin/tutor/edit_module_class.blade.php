@php
    $formAction = route('tutor.module.update',$package->id);
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12">
      <div class="row">
          <div class="col-md-12">
      <a href="{{ route('tutor.module.view', $package->user_id) }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">
                          Back
                        </button>
                      </a>
</div>
</div>
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
                  Edit Class
            </h4>
            <p class="card-category">Manage Module Class</p>
          </div>
          @if (isset($res))
    <div class="col-sm-12">
        <div class="alert  alert-danger alert-dismissible fade show" role="alert">
          $res
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif
          <div class="card-body">
            <form action="{{ $formAction }}" id="form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
             
              <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                  <!-- <label class="bmd-label-floating">Class</label> -->
                  <select id="batch_id" name="batch_id" class="form-control" hidden>
                    @isset($userClasses)
                    @foreach($userClasses as $value)
                    <option value="{{$value->class_id}}" {{ $value->class_id == $package->batch_id ? 'selected' : '' }} >{{$value->class_name}}</option>
                    @endforeach
                    @endisset
                  </select>
                
                </div>
              </div> 
                </div>
  
                <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                  
                    <label class="bmd-label-floating">Intro Tittle <span class="text-danger">*</span></label>
                    <input id="intro_title" type="text" name="intro_title" class="form-control"
                        value="{{$package->intro_title}}"required>
                        @error ('intro_title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Intro Description <span class="text-danger">*</span></label>
                    <textarea id="intro_description" cols="70" rows="5"  name="intro_description" class="form-control"
                        value="" >{{$package->intro_description}}</textarea>
                    @error('intro_description')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>

                <div class="col-md-3" id="introVideo" style="display:block">
                  <div class="form-group">
                    <label class="bmd-label-floating">Trailer <span class="text-danger">*</span></label>
                    <input id="intro_video" type="file" name="intro_video" class="form-control"
                        value="" >
                    @error ('intro_video')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                    <div id="valid_intro_video" style="font-size: 13px;display: block;"><p class="text-danger">.MP4,.JPEG(not More than 1GB)</p></div>
                    <a href="" class="view-portfolio" id="viewTrailerVideo" style="display:block;" data-video_url="{{asset($package->trailer_video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                  </a>

                  <button type="button" id= "TrailerUpload" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar2" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process2" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image_trailer" class="alert   alert-dismissible fade show row mt-1" role="alert">  </div>
                  </div>
                </div>
              
</div>
<div class="mainintro" id="intromain">



@foreach($packageDetails as $key=>$value)
                <div class="multiple" id="multiple">
               
                <!-- <div class="col-md-6" id="video1" style="display:block">
                  <div class="form-group">
                    <label class="bmd-label-floating">Trailer <span class="text-danger">*</span></label>
                    <input id="trailer{{$key}}" type="file" name="multi_trailer[{{ $key }}]" class="form-control"
                        value="{{$value->trailer}}"  onchange="return getFileTrailer(this.value,$key)">
                    @error ('multi_trailer')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                    <div id="valid_multiple_trailer{{$key}}" style="font-size: 13px;display: block;"><p class="text-danger">.MP4,.JPEG(not More than 1GB)</p></div>
                    <a href="" class="view-portfolio" id="viewVideoTrailer{{$key}}" style="display:block;"  data-video_url="{{asset($value->trailer)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                  </a>
                  <button type="button" id= "upload_multi_trailer{{$key}}" value="{{$key}}" onclick="fileUploadTrailer(this.value)" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar_multi_trailer{{$key}}" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process_multi_trailer{{$key}}" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image_multi_trailer{{$key}}" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert">  </div>
                  </div>
                </div> -->
            
           

                <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$value->user_id}}" required> 
                  <label class="bmd-label-floating">Title <span class="text-danger">*</span></label>
                  <input id="title" type="text" name="title[{{$key}}]" class="form-control" value="{{$value->title}}" required>
                </div>
              </div>
            
              <div class="col-md-5">
                <div class="form-group">
                  <label class="bmd-label-floating">Description <span class="text-danger">*</span></label>
                  <textarea id="description" type="text"  rows="5" cols="70" name="description[{{ $key }}]" class="form-control" value="">{{$value->description}}</textarea>
                </div>
              </div>

              <div class="col-md-3" id="video1" style="display:block">
                  <div class="form-group">
                    <label class="bmd-label-floating">Video <span class="text-danger">*</span></label>
                    <input id="{{$key}}" type="file" name="video_url[{{ $key }}]" class="form-control"
                        value="{{$value->video_url}}"  onchange="return getFile(this.value,this.id)">
                    @error ('video_url')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                    <div id="valid_video_url{{$key}}" style="font-size: 13px;display: block;"><p class="text-danger">.MP4,.JPEG(not More than 1GB)</p></div>
                    <a href="" class="view-portfolio" id="viewVideo{{$key}}" style="display:block;"  data-video_url="{{asset($value->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                  </a>
                  <button type="button" id= "upload{{$key}}" value="{{$key}}" onclick="fileUpload(this.value)" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">upload</i>
</button>
              
                        <div class="progress" id="progress_bar{{$key}}" style="display:none;height:20px; line-height: 20px;">

            <div class="progress-bar" id="progress_bar_process{{$key}}" role="progressbar" style="width:0%;">0%</div>

        </div>

        <div id="uploaded_image{{$key}}" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert">  </div>
                  </div>
                </div>
            </div>
     

                <!-- <div class="col-md-6">
<button type="button" name="add" id="add-btn" class="btn btn-primary btn-link btn-sm" ><i class="bi bi-plus">Add More</i></button>
</div> -->

@endforeach
<div class="row">
<div class="col-md-4">
              <div class="form-group">
                    <label class="bmd-label-floating"> Total Price(USD) <span class="text-danger">*</span></label>
                    <input id="total_price" type="number" name="total_price" class="form-control"
                    value="{{$package->total_price}}" required>
                    @error('total_price')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
</div>
<!-- <a href="{{ route('tutor.module.view', $package->user_id) }}"  class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                        </button>
                      </a> -->

              <button id ="submit" type="submit" name="submit_button" class="btn btn-primary btn-sm float-right" style=" background-color: #0c82be !important;
    border-color: #0c82be !important;">submit</button>
           
          </form>

          </div>
          
        </div>

      </div>
  
</div>
<div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">View Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
@endsection

@section('custom-scripts')
  <script type="text/javascript">
   window.addEventListener("DOMContentLoaded", function(e) {

var form_being_submitted = false;

var checkForm = function(e) {
  var form = e.target;
  if(form_being_submitted) {
    alert("The form is being submitted, please wait a moment...");
    form.submit_button.disabled = true;
    e.preventDefault();
    return;
  }
  form.submit_button.value = "Submitting form...";
      form_being_submitted = true;
    };
    document.getElementById("form").addEventListener("submit", checkForm, false);

  }, false);
      $( document ).ready(function() {
     
          $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });


        document.getElementById('intro_video').addEventListener("change", function (e) {
        
          var fileName = document.getElementById("intro_video").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="webm" || extFile=="mkv"){
          $('#valid_intro_video').hide();
$('#TrailerUpload').show();
$('#viewTrailerVideo').hide();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/mkv/webm  files are allowed!");
            document.getElementById("intro_video").value = null;
          
        }
        
         
});
$(document).on('click', '#TrailerUpload', function() {
 $('#TrailerUpload').hide();
 var file_element = document.getElementById('intro_video');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar2');
var progress_bar_process = document.getElementById('progress_bar_process2');

var uploaded_image = document.getElementById('uploaded_image_trailer');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('module.StoreVideo',$package->user_id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

 var percent_completed = Math.round((event.loaded / event.total) * 100);

 progress_bar_process.style.width = percent_completed + '%';

 progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div id="success-upload_trailer" class="alert alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('#success-upload_trailer').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);
});


      });
      var i ='';
      function getFile(val,getId){
i=getId;
var fileName = document.getElementById(getId).value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="mp4" || extFile=="jpeg"){
          $('#valid_video_url'+i+'').hide();
$('#upload'+i+'').show();
$('#viewVideo'+i+'').hide();
$('#submit').attr('disabled','disabled');
        }else{
            alert("Only mp4/jpeg  files are allowed!");
            document.getElementById(getId).value = null;
          
        }

}

function fileUpload(val){
  i=val;
$('#upload'+i+'').hide();
// let file_element =  $('#video_url').val();
var file_element = document.getElementById(i);
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar'+i+'');
var progress_bar_process = document.getElementById('progress_bar_process'+i+'');

var uploaded_image = document.getElementById('uploaded_image'+i+'');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('module.StoreVideo',$package->user_id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

 var percent_completed = Math.round((event.loaded / event.total) * 100);

 progress_bar_process.style.width = percent_completed + '%';

 progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div id="success'+i+'" class="alert alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('#success'+i+'').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

}

var trailer ='';
      function getFileTrailer(val,getId){
        trailer=getId;
$('#valid_multiple_trailer'+i+'').hide();
$('#upload_multi_trailer'+trailer+'').show();
$('#viewVideoTrailer'+trailer+'').hide();
$('#submit').attr('disabled','disabled');
}

function fileUploadTrailer(val){
  trailer=val;
$('#upload_multi_trailer'+trailer+'').hide();
// let file_element =  $('#video_url').val();
var file_element = document.getElementById('trailer'+trailer+'');
console.log("files");
console.log(file_element);
var progress_bar = document.getElementById('progress_bar_multi_trailer'+trailer+'');
var progress_bar_process = document.getElementById('progress_bar_process_multi_trailer'+trailer+'');

var uploaded_image = document.getElementById('uploaded_image_multi_trailer'+trailer+'');
var form_data = new FormData();

form_data.append('videoUrl', file_element.files[0]);

form_data.append('_token', document.getElementsByName('_token')[0].value);

progress_bar.style.display = 'block';

var ajax_request = new XMLHttpRequest();

ajax_request.open("POST", "{{ route('module.StoreVideo',$package->user_id) }}");   

ajax_request.upload.addEventListener('progress', function(event){

 var percent_completed = Math.round((event.loaded / event.total) * 100);

 progress_bar_process.style.width = percent_completed + '%';

 progress_bar_process.innerHTML = percent_completed + '% completed';

});
ajax_request.addEventListener('load', function(event){
console.log("response");
var file_data = JSON.parse(event.target.response);

console.log(file_data);

uploaded_image.innerHTML = '<div id="success'+trailer+'" class="alert alert-success" style="width: 203px;height: 54px">Files Uploaded Successfully</div>';

$('#submit').removeAttr('disabled');

progress_bar.style.display = 'none';
$('#success'+trailer+'').fadeIn().delay(10000).fadeOut();
});

ajax_request.send(form_data);

}
      document.getElementById('batch_id').addEventListener("change", function (e) {
            const classType =  $('#batch_id').val();
      });
      document.getElementById('batch_id').addEventListener("change", function (e) {
            const type =  $('#batch_id').val();
            // clearFields();
           if(type == 2)
            $('#add-btn').show();
           else
           $('#add-btn').hide();
          //  if(type == 3){
          //   $('#classType').show();
          //   $('#startDate').show();
          //   $('#endDate').show();
          //   $('#startTime').show();
          //   $('#endTime').show();
          //   $('#Duration').show();
          //   $('#meetingLink').show();
          //   $('#noOfClass').show();
          //  }else{
          //   $('#classType').hide();
          //   $('#startDate').hide();
          //   $('#endDate').hide();
          //   $('#startTime').hide();
          //   $('#endTime').hide();
          //   $('#Duration').hide();
          //   $('#meetingLink').hide();
          //   $('#noOfClass').hide();
          //  }
        
          });
function clearFields(event){
  document.getElementById('title').value = '';
  document.getElementById('description').value = '';
  document.getElementById('video_url').value = '';
  document.getElementById('total_price').value = '';
  document.getElementById('class_type').value = '';
  document.getElementById('start_date').value = '';
  document.getElementById('end_date').value = '';
  document.getElementById('start_time').value = '';
  document.getElementById('end_time').value = '';
  document.getElementById('duration').value = '';
  document.getElementById('meeting_link').value = '';
  document.getElementById('no_of_class').value = '';
}

      
$(document).on('click', '#add-btn', function() {
addrow();

      });
      function addrow(){
      
        var tr ='<div class="sub">'+
        '<div class="row">'+
                '<div class="col-md-6">'+
                  '<div class="form-group">'+
                    '<label class="bmd-label-floating">Tittle</label>'+
                    '<input id="title" type="text" name="title[]" class="form-control"'+
                        'value=""required>'+
                   '@error ('title')'+
                        '<p class="text-danger">{{ $message }}</p>'+
                    '@enderror'+
                  '</div>'+
                '</div>'+
                '<div class="col-md-6">'+
                  '<div class="form-group">'+
                    '<label class="bmd-label-floating">Description</label>'+
                    '<input id="description" type="text" name="description[]" class="form-control"'+
                    'value="">'+
                    '@error('description')'+
                    '<span class="invalid-feedback" role="alert">'+
                                '<strong>{{ $message }}</strong>'+
                            '</span>'+
                    '@enderror'+
                  '</div>'+
                '</div>'+
'</div>'+
                '<div class="row">'+
               
                '<div class="col-md-6" id="video1" style="display:block">'+
                  '<div class="form-group">'+
                    '<label class="bmd-label-floating">Video</label>'+
                    '<input id="video_url" type="file" name="video_url[]" class="form-control"'+
                        'value=""required>'+
                    '@error ('video_url')'+
                        '<p class="text-danger">{{ $message }}</p>'+
                    '@enderror'+
                 
                    '<button type="button" id= "upload" style="display:none;"class=" btn btn-primary btn-sm pull-right"  rel="tooltip" title="upload video" class="btn btn-primary btn-link btn-sm">'+
                          '<i class="material-icons">upload</i>'+
'</button>'+
              
                        '<div class="progress" id="progress_bar" style="display:none;height:20px; line-height: 20px;">'+

            '<div class="progress-bar" id="progress_bar_process" role="progressbar" style="width:0%;">0%</div>'+

        '</div>'+

        '<div id="uploaded_image" class="alert  success-upload alert-dismissible fade show row mt-1" role="alert">  </div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                  '<div class="form-group">'+
                    '<label class="bmd-label-floating">Total Price</label>'+
                    '<input id="total_price" type="number" name="total_price[]" class="form-control"'+
                    'value=""required>'+
                    '@error('total_price')'+
                    '<span class="invalid-feedback" role="alert">'+
                                '<strong>{{ $message }}</strong>'+
                            '</span>'+
                    '@enderror'+
                  '</div>'+
                '</div>'+
                    '</div>'+
                  '<div class="col-md-6" id="btn_remove">'+
                  '<div class="form-group">'+
                  '<button type="button" rel="tooltip" title="Delete video" class="btn btn-primary btn-link btn-sm remove">'+
                 '<i class="material-icons">delete</i>'+
                        '</button>'+
                '<div >'+
                '<div >'+
                '<div >'+
                '<div >'
    $('#multiple').append(tr);
              };
              $('#multiple').on('click','.remove',function() {
                $(this).closest(".sub").remove();
    });
  </script>
@endsection
