@php
    $formAction = isset($region) ? route('region.update', $region->id) : route('region.store');
    $issetRegion = isset($region) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title">
              @isset($region)
                  Edit Region Details
              @else
                  Add New Region<Tutor></Tutor>
              @endisset
            </h4>
          
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($region) @method('PUT') @endisset
              @csrf
              <div class="row">
               
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Region Name</label>
                    <input id="region" type="text" name="name" class="form-control"
                        value="{{ old('name') ?? ($issetRegion ? $region->name : '')  }}">
                    @error ('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <a href="{{ route('region.index') }}" class= "pull-left">
                        <button type="button"  class="btn btn-primary btn-link btn-sm">
                          Back
                  </button></a>
              <button type="submit" class="btn btn-primary btn-sm pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
     
    </div>
   
  </div>
</div>
@endsection
