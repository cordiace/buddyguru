@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            {{--<h4 class="card-title ">Regions</h4>--}}
            <ul class="nav nav-tabs" data-tabs="tabs">
              <li class="nav-item">
                <a class="nav-link active" href="{{route('region.create')}}" >
                  <i class="material-icons">add</i> Add Region
                  <div class="ripple-container"></div>
                  <div class="ripple-container"></div></a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="">
            <table id="example1" class="table table-bordered table-striped">
                <thead >
                  <th>ID</th>
                  <th>Region</th>
                  <!-- <th>Image</th> -->
                  <th class="text-center" >Action</th>
                </thead>
                <tbody>
                  @isset($regions)
                  @foreach($regions as $region)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$region->name}}</td>
                  
                    <td class="text-center py-0 align-middle">
                      <div class="btn-group btn-group-sm">
                      <a href="{{ route('region.edit', $region->id) }}" rel="tooltip" title="Edit Region" class="btn btn-info btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Edit Region" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-edit"></i>
                        <!-- </button> -->
                      </a>
                      <a href="{{ route('region.delete', $region->id) }}"  rel="tooltip" title="Delete Region" class="btn btn-danger btn-sm">
                        <!-- <button type="button" rel="tooltip" title="Delete Region" class="btn btn-primary btn-link btn-sm"> -->
                        <i class="fas fa-trash"></i>
                        <!-- </button> -->
                      </a>
</div>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
