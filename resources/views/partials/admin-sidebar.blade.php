<ul class="nav">
  <li class="nav-item  {{(strpos(\Request::route()->getName(),'admin.home')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('admin.home')}}">
      <i class="material-icons">dashboard</i>
      <p class="pointer">Dashboard</p>
    </a>
  </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'user')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('user.index')}}">
      <i class="material-icons">school</i>
      <p class="pointer">Buddy Profile</p>
    </a>
  </li> 
  <li class="nav-item {{(strpos(\Request::route()->getName(),'tutor')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('tutor.index')}}">
      <i class="material-icons">record_voice_over</i>
      <p class="pointer">Guru Profile</p>
    </a>
  </li>
   <!-- <li class="nav-item {{(strpos(\Request::route()->getName(),'sessions') !== false) ? 'active' :''}}">
  <a class="nav-link" href="{{route('sessions.index')}}">
  <i class="material-icons">assessment</i>
  <p class="pointer">Sessions</p>
  </a>
  </li>  -->
  {{--
  <li class="nav-item {{(strpos(\Request::route()->getName(),'payment') !== false) ? 'active' :''}}">
  <a class="nav-link" href="{{route('payment.index')}}">
  <i class="material-icons">money</i>
  <p class="pointer">Payments</p>
  </a>
</li> --}}
  <li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples" aria-expanded="{{(strpos(\Request::route()->getName(),'subcategory') !== false)||
              (strpos(\Request::route()->getName(),'category') !== false)||
              (strpos(\Request::route()->getName(),'skill') !== false)||
              (strpos(\Request::route()->getName(),'country') !== false)||
              (strpos(\Request::route()->getName(),'cmsfield') !== false)
              ? 'true' :'false'}}">
      <i class="material-icons">image</i>
      <p class="pointer"> CMS
        <b class="caret"></b>
      </p>
    </a>
    <div class="collapse {{(strpos(\Request::route()->getName(),'subcategory') !== false)||
              (strpos(\Request::route()->getName(),'category') !== false)||
              (strpos(\Request::route()->getName(),'skill') !== false)||
              (strpos(\Request::route()->getName(),'country') !== false)||
              (strpos(\Request::route()->getName(),'region') !== false)||
              (strpos(\Request::route()->getName(),'cmsfield') !== false)
              ? 'show' :''}}" id="pagesExamples" style="">
      <ul class="nav">
        <li class="nav-item {{(strpos(\Request::route()->getName(),'banner') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('banner.index')}}">
            <i class="material-icons">category</i>
            <p class="pointer">Banners</p>
          </a>
        </li>
        <!-- <li class="nav-item {{(strpos(\Request::route()->getName(),'category') !== false)&&(strpos(\Request::route()->getName(),'subcategory') !== 0) ? 'active' :''}}">
          <a class="nav-link" href="{{route('category.index')}}">
            <i class="material-icons">category</i>
            <p class="pointer">Skill Set</p>
          </a>
        </li> -->
        <li class="nav-item {{(strpos(\Request::route()->getName(),'category') !== false)&&(strpos(\Request::route()->getName(),'subcategory') !== 0) ? 'active' :''}}">
          <a class="nav-link" href="{{route('category.index')}}">
            <i class="material-icons">category</i>
            <p class="pointer">Category Set</p>
          </a>
        </li>

        <li class="nav-item {{(strpos(\Request::route()->getName(),'skill') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('skill.index')}}">
            <i class="material-icons">workspaces</i>
            <p class="pointer">Category Skills</p>
          </a>
        </li> 
        {{--<li class="nav-item {{(strpos(\Request::route()->getName(),'subcategory') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('subcategory.index')}}">
            <i class="material-icons">subdirectory_arrow_left</i>
            <p class="pointer">Sub Categories</p>
          </a>
        </li> --}}
        {{--<li class="nav-item {{(strpos(\Request::route()->getName(),'grade') !== false) ? 'active' :''}}">--}}
        {{--<a class="nav-link" href="{{route('grade.index')}}">--}}
        {{--<i class="material-icons">grade</i>--}}
        {{--<p>Grade</p>--}}
        {{--</a>--}}
        {{--</li>--}}
         <!-- <li class="nav-item {{(strpos(\Request::route()->getName(),'skill') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('skill.index')}}">
            <i class="material-icons">workspaces</i>
            <p>Skills</p>
          </a>
        </li>  -->
        <li class="nav-item {{(strpos(\Request::route()->getName(),'terms') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('terms.index')}}">
            <i class="material-icons">workspaces</i>
            <p class="pointer">Terms and Conditions</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'policy') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('policy.index')}}">
            <i class="material-icons">workspaces</i>
            <p class="pointer">Privacy Policy</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'country') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('country.index')}}">
            <i class="material-icons">map</i>
            <p class="pointer">Countries</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'region') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('region.index')}}">
            <i class="material-icons">map</i>
            <p class="pointer">Regions</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'cmsfield') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('cmsfield.index')}}">
            <i class="material-icons">picture_in_picture</i>
            <p class="pointer">Info Screens</p>
          </a>
        </li>
      </ul>
    </div>
  </li>




  <li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#generasettings" aria-expanded="{{(strpos(\Request::route()->getName(),'subcategory') !== false)||
              (strpos(\Request::route()->getName(),'tier') !== false)||
              (strpos(\Request::route()->getName(),'currency') !== false)
              ? 'true' :'false'}}">
      <i class="material-icons">image</i>
      <p class="pointer"> General Settings
        <b class="caret"></b>
      </p>
    </a>
    <div class="collapse {{(strpos(\Request::route()->getName(),'subcategory') !== false)||
              (strpos(\Request::route()->getName(),'tier') !== false)||
              (strpos(\Request::route()->getName(),'admin.settings.viewCurrency') !== false||
              (strpos(\Request::route()->getName(),'generalsetting.index') !== false))
              ? 'show' :''}}" id="generasettings" style="">
      <ul class="nav">
           <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.settings.viewCurrency') !== false)&&(strpos(\Request::route()->getName(),'subcategory') !== 0) ? 'active' :''}}">
          <a class="nav-link" href="{{route('admin.settings.viewCurrency')}}">
            <i class="material-icons">category</i>
            <p class="pointer">Currency Settings</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.tier.index') !== false)&&(strpos(\Request::route()->getName(),'subcategory') !== 0) ? 'active' :''}}">
          <a class="nav-link" href="{{route('admin.tier.index')}}">
            <i class="material-icons">category</i>
            <p class="pointer">Tier Management</p>
          </a>
        </li>

        <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.tier.currency_range.index') !== false)&&(strpos(\Request::route()->getName(),'subcategory') !== 0) ? 'active' :''}}">
          <a class="nav-link" href="{{route('admin.tier.currency_range.index')}}">
            <i class="material-icons">category</i>
            <p class="pointer">Currency Range Mapping</p>
          </a>
        </li>

        <li class="nav-item {{(strpos(\Request::route()->getName(),'generalsetting.index') !== false)&&(strpos(\Request::route()->getName(),'subcategory') !== 0) ? 'active' :''}}">
          <a class="nav-link" href="{{route('generalsetting.index')}}">
            <i class="material-icons">record_voice_over</i>
            <p class="pointer">Commission</p>
          </a>
        </li> 

        
      
      </ul>
    </div>
  </li>




   <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.helpdesk')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('admin.helpdesk')}}">
      <i class="material-icons">record_voice_over</i>
      <p class="pointer">Help Desk</p>
    </a>
  </li> 
  <!-- <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.generalsetting')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('generalsetting.index')}}">
      <i class="material-icons">record_voice_over</i>
      <p class="pointer">Commission</p>
    </a>
  </li> 
   <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.recentbooking')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('recentbooking.index')}}">
      <i class="material-icons">record_voice_over</i>
      <p class="pointer">Recent Booking</p>
    </a>
  </li> -->
  <li class="nav-item">
    <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
      <i class="material-icons">person</i>
      <p class="pointer">Log Out</p>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </a>
  </li>
</ul>
