-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2020 at 02:14 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buddy-guru`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_abouts`
--

CREATE TABLE `home_abouts` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `additional_text` text COLLATE utf8_unicode_ci,
  `image1` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_abouts`
--

INSERT INTO `home_abouts` (`id`, `title`, `description`, `additional_text`, `image1`, `image2`, `image3`) VALUES
(1, 'Online Worship', 'Art for a new sermon series starting this weekend at TVC going over the Gospel of John. Come learn with us, or listen via our podcast.Art for a new sermon series starting this weekend at TVC going over the Gospel of John. Come learn with us, or listen via our podcast.', 'Also there are some desktop and iPhone wallpapers attached if you are so inclined.', 'Group56image_1599825242.jpg', 'Group55image2_1599825242.png', 'Group55image3_1599825242.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learn_more_link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `watch_now_link` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `date`, `title`, `description`, `image`, `page`, `learn_more_link`, `watch_now_link`) VALUES
(1, '2020-09-24', 'Calling Over Comfort', 'There are some moments when God will call you to leave the familiar and step', 'banner_1599822286.jpg', 'home', 'link', 'link2'),
(2, '2020-10-15', 'Calling Over Comfort', 'There are some moments when God will call you to leave the familiar and step into the unknown. It may be uncomfortable, but if you’ll choose your calling over your comfort, God will make things happen that you couldn’t make happen on your own.   Learn More', 'b-1_1602570976.jpg', 'home', '#', '#'),
(3, '2020-10-16', 'Calling Over Comfort', 'There are some moments when God will call you to leave the familiar and step into the unknown. It may be uncomfortable, but if you’ll choose your calling over your comfort, God will make things happen that you couldn’t make happen on your own.   Learn More', 'b-3_1602570993.jpg', 'home', '#', '#');

-- --------------------------------------------------------

--
-- Table structure for table `home_headers`
--

CREATE TABLE `home_headers` (
  `id` int(11) NOT NULL,
  `logo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_links` text COLLATE utf8_unicode_ci,
  `menu` text COLLATE utf8_unicode_ci,
  `copy_right` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_links` text COLLATE utf8_unicode_ci,
  `min_amount` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_terms` text COLLATE utf8_unicode_ci,
  `description_terms` text COLLATE utf8_unicode_ci,
  `title_stream` text COLLATE utf8_unicode_ci,
  `description_stream` text COLLATE utf8_unicode_ci,
  `play_store_link` text COLLATE utf8_unicode_ci,
  `app_store_link` text COLLATE utf8_unicode_ci,
  `footer_logo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_headers`
--

INSERT INTO `home_headers` (`id`, `logo`, `social_links`, `menu`, `copy_right`, `short_links`, `min_amount`, `title_terms`, `description_terms`, `title_stream`, `description_stream`, `play_store_link`, `app_store_link`, `footer_logo`) VALUES
(1, 'online-workshop-logo_1599825901.svg', 'a:4:{i:0;a:2:{s:4:\"name\";s:13:\"icon-facebook\";s:4:\"link\";s:1:\"#\";}i:1;a:2:{s:4:\"name\";s:12:\"icon-twitter\";s:4:\"link\";s:1:\"#\";}i:2;a:2:{s:4:\"name\";s:13:\"icon-whatsapp\";s:4:\"link\";s:1:\"#\";}i:3;a:2:{s:4:\"name\";s:12:\"icon-youtube\";s:4:\"link\";s:1:\"#\";}}', 'a:4:{i:0;a:2:{s:4:\"name\";s:4:\"Home\";s:4:\"link\";s:25:\"http://localhost/worship/\";}i:1;a:2:{s:4:\"name\";s:12:\"Watch Online\";s:4:\"link\";s:37:\"http://localhost/worship/watch-online\";}i:2;a:2:{s:4:\"name\";s:11:\"Church List\";s:4:\"link\";s:33:\"http://localhost/worship/churches\";}i:3;a:2:{s:4:\"name\";s:4:\"Give\";s:4:\"link\";s:35:\"http://localhost/worship/donate-now\";}}', '2020 Onlineworship. All rights reserved.', 'a:3:{i:0;a:2:{s:4:\"name\";s:5:\"About\";s:4:\"link\";s:30:\"http://localhost/worship/about\";}i:1;a:2:{s:4:\"name\";s:15:\"Privacy & legal\";s:4:\"link\";s:45:\"http://localhost/worship/terms-and-conditions\";}i:2;a:2:{s:4:\"name\";s:7:\"Contact\";s:4:\"link\";s:32:\"http://localhost/worship/contact\";}}', '5.99', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `joined_members`
--

CREATE TABLE `joined_members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8_unicode_ci,
  `message` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `joined_members`
--

INSERT INTO `joined_members` (`id`, `user_id`, `name`, `email`, `phone`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, NULL, 'John', 'john@gmail.com', '9988776655', 'dfdf', 'This is a test message', '2020-09-02 05:26:29', '2020-09-02 05:26:29'),
(28, 5, 'sreenath k', 'test@gmail.com', NULL, 'test', 'test', '2020-09-18 04:11:54', '2020-09-18 04:11:54'),
(29, 5, 'sreenath k', 'test@gmail.com', '9605469651', NULL, 'vazhakkala', '2020-09-29 00:45:24', '2020-09-29 00:45:24'),
(30, 5, 'sreenath k', 'test@gmail.com', '9605469651', NULL, 'vazhakkala', '2020-09-29 00:50:32', '2020-09-29 00:50:32'),
(31, 5, 'sreenath k', 'test@gmail.com', '9605469651', NULL, 'vazhakkala', '2020-09-29 00:53:18', '2020-09-29 00:53:18'),
(32, 5, 'sreenath k', 'test@gmail.com', '9605469651', NULL, 'vazhakkala', '2020-09-29 00:53:36', '2020-09-29 00:53:36'),
(33, NULL, 'sreenath k', 'test@gmail.com', NULL, NULL, 'dssds', '2020-09-29 00:56:28', '2020-09-29 00:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `service_times`
--

CREATE TABLE `service_times` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `service_person_dp` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_time` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service_times`
--

INSERT INTO `service_times` (`id`, `user_id`, `date`, `service_person_dp`, `service_time`) VALUES
(1, 4, '2020-08-29', 'aaaaaa_1599821404.jpg', 'a:3:{i:0;a:2:{s:4:\"time\";s:5:\"17:27\";s:5:\"title\";s:5:\"time1\";}i:1;a:2:{s:4:\"time\";s:5:\"20:27\";s:5:\"title\";s:5:\"time2\";}i:2;a:2:{s:4:\"time\";s:5:\"16:27\";s:5:\"title\";s:5:\"time3\";}}'),
(2, 5, '2020-08-29', 'aaaaaa_1599821448.jpg', 'a:3:{i:0;a:2:{s:4:\"time\";s:5:\"19:21\";s:5:\"title\";s:5:\"time1\";}i:1;a:2:{s:4:\"time\";s:5:\"21:21\";s:5:\"title\";s:5:\"time2\";}i:2;a:2:{s:4:\"time\";s:5:\"16:27\";s:5:\"title\";s:5:\"time4\";}}'),
(4, 4, '2020-10-14', 'prof-slider-img01_1602571438.jpg', 'a:3:{i:0;a:2:{s:4:\"time\";s:5:\"10:13\";s:5:\"title\";s:5:\"time1\";}i:1;a:2:{s:4:\"time\";s:5:\"15:40\";s:5:\"title\";s:5:\"time3\";}i:2;a:2:{s:4:\"time\";s:5:\"15:50\";s:5:\"title\";s:5:\"time1\";}}'),
(5, 4, '2020-10-16', 'shutterstock_1288136518_1602571517.jpg', 'a:3:{i:0;a:2:{s:4:\"time\";s:5:\"08:14\";s:5:\"title\";s:5:\"time1\";}i:1;a:2:{s:4:\"time\";s:5:\"01:14\";s:5:\"title\";s:5:\"time2\";}i:2;a:2:{s:4:\"time\";s:5:\"17:15\";s:5:\"title\";s:5:\"time3\";}}'),
(6, 4, '2020-10-13', 'shutterstock_1288136518_1602572639.jpg', 'a:3:{i:0;a:2:{s:4:\"time\";s:5:\"05:33\";s:5:\"title\";s:5:\"time1\";}i:1;a:2:{s:4:\"time\";s:5:\"14:35\";s:5:\"title\";s:5:\"time2\";}i:2;a:2:{s:4:\"time\";s:5:\"16:33\";s:5:\"title\";s:5:\"time3\";}}');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `subscription` int(191) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `email`, `slug`, `phone_number`, `location`, `subscription`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin@gmail.com', 'admin', '1111111111', NULL, 1, NULL, '$2y$10$kEqj44q5crp1xdYKv5n2Ae3xXN8MX0zIho.DVMKocboLeAI.byP96', 'DHJlNU7oEox6AFDpG7aPZghakQe609VmBsfW5XONkFOwA4Ua62xXYrMC3nKe', '2020-08-26 03:44:43', '2020-09-02 01:09:35'),
(4, 'church3', 'church', 'church3@gmail.com', 'church3', '9605469651', NULL, 1, NULL, '$2y$10$vbUPasjBVJkO0o1pFtDPKeylQ0fTttVUXvZi3xDzRmkw1YhU1/s9u', NULL, '2020-08-26 06:16:08', '2020-08-27 23:46:24'),
(5, 'church4', 'church', 'church4@gmail.com', 'church4', '9605469653', NULL, 1, NULL, '$2y$10$vbUPasjBVJkO0o1pFtDPKeylQ0fTttVUXvZi3xDzRmkw1YhU1/s9u', NULL, '2020-08-26 06:17:44', '2020-08-26 06:17:44'),
(7, 'st joseph', 'church', 'stjoseph@gmail.com', 'st-joseph', NULL, NULL, 1, NULL, '$2y$10$Dwpj0t7Q/UFQR4YtJ6tF8OlqVz/PuKiHIHHQteowt1WzqyusYyFbi', NULL, '2020-09-14 23:47:47', '2020-09-14 23:47:47'),
(8, 'St johns', 'church', 'stjohn@gmail.com', 'st-johns', '1234568752', NULL, 1, NULL, '$2y$10$RHUPaLOI1Gex2IP4ab1ZsuEA4pl6CWJB7imfW96x8L7kclYI6O2xK', NULL, '2020-09-14 23:54:05', '2020-09-14 23:54:05'),
(9, 'Product', 'church', 'admin123@gmail.com', 'mobile-voice-and-data', '9874562441', NULL, NULL, NULL, '$2y$10$iWG.fASuKfWwM6eARZH2R.ZZsE9q8HiMfwhV2Ix6eC2ZCVOF.JblK', NULL, '2020-10-30 06:56:21', '2020-10-30 06:56:21'),
(11, 'student2', 'student', 'student2@ymail.com', 'stud-2', '3245697845', NULL, NULL, NULL, '$2y$10$pdtS2k0pRypOzIum8GB2N.OkpRZ5HgSHA8hB2NNPryYh4IuxYHDHK', NULL, '2020-10-30 07:19:49', '2020-10-30 07:24:17');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `user_id`, `title`, `name`, `video`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 5, 'Little boy praying at table', 'Micah Allen', 'file_example_MP4_480_1_5MG_1600154187.mp4', 'aaaaaa_1600155126.jpg', 'Art for a new sermon series starting this weekend at TVC going over the Gospel of John. Come learn with us, or listen via our podcast.', '2020-09-15 01:46:27', '2020-09-23 06:05:18'),
(2, 5, 'Calling Over Comfort', 'Celina Gomez', 'WATCH- Mississippi Pastor Speaks In Tongues_1600860377.mp4', 'thumb-video_1600860377.jpg', NULL, '2020-09-23 05:56:17', '2020-09-23 05:56:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `home_abouts`
--
ALTER TABLE `home_abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_headers`
--
ALTER TABLE `home_headers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `joined_members`
--
ALTER TABLE `joined_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `service_times`
--
ALTER TABLE `service_times`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_abouts`
--
ALTER TABLE `home_abouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `home_headers`
--
ALTER TABLE `home_headers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `joined_members`
--
ALTER TABLE `joined_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_times`
--
ALTER TABLE `service_times`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
