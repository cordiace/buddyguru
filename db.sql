-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: buddyguru
-- ------------------------------------------------------
-- Server version	8.0.29-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banners` (
  `id` int NOT NULL AUTO_INCREMENT,
  `location` varchar(191) DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `heading` text NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (2,'buddy-home','uploads/0gQW0BKksdE99Q5BqoJY5nrLudrLmeGSOla4Gbp8.png','',''),(4,'guru-home','uploads/P4gUteL0rOkOwriO2VJHhVB5HDHfuKZ9wJCDITcU.jpeg','Heading 2','Description 2');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_package`
--

DROP TABLE IF EXISTS `batch_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `batch_package` (
  `id` int NOT NULL AUTO_INCREMENT,
  `package_id` int NOT NULL,
  `batch_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_package`
--

LOCK TABLES `batch_package` WRITE;
/*!40000 ALTER TABLE `batch_package` DISABLE KEYS */;
INSERT INTO `batch_package` VALUES (42,33,18),(51,39,20),(52,41,20),(53,39,24),(54,40,25),(58,46,29),(60,50,30),(61,49,31),(62,53,32),(63,54,33),(64,65,34),(65,70,35),(67,74,37),(68,34,21),(69,66,38),(70,76,39),(71,77,40),(72,78,41),(73,79,42),(74,80,43),(75,73,36),(77,80,45),(78,82,46),(79,82,47),(80,82,48),(81,67,49),(82,51,50),(83,51,51),(84,51,52),(85,51,53),(86,83,54),(87,84,55),(88,85,56),(89,74,57),(90,83,58);
/*!40000 ALTER TABLE `batch_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batches`
--

DROP TABLE IF EXISTS `batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `batches` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `package_id` int NOT NULL,
  `batch_name` varchar(191) NOT NULL,
  `description` varchar(191) DEFAULT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `duration` int NOT NULL,
  `no_of_participants` int NOT NULL,
  `no_of_sessions` int NOT NULL,
  `payment_type` varchar(191) DEFAULT NULL,
  `subscription_fee` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batches`
--

LOCK TABLES `batches` WRITE;
/*!40000 ALTER TABLE `batches` DISABLE KEYS */;
INSERT INTO `batches` VALUES (1,102,31,'batch','this is batch 12\nNormally we have lot of issues coming with the description','2022-02-01','2022-03-10',30,10,102,'monthly',1500,'2021-10-27 10:20:58','2022-02-16 07:46:47'),(3,102,1,'batch 3','this is batch 1','2021-12-22','2021-12-30',30,10,10,'monthly',500,'2021-10-27 10:22:51','2021-12-10 17:05:19'),(9,138,0,'batch 1','sdsd','2021-11-13','2021-11-20',10,10,10,'monthly',1000,'2021-11-12 16:31:06','2021-11-12 16:31:06'),(12,147,0,'batch 5','sdsd','2021-11-04','2021-11-28',10,10,10,'monthly',1000,'2021-11-19 17:59:01','2021-11-19 17:59:01'),(13,147,0,'New batch','new batch','2021-11-20','2021-11-21',20,20,20,'monthly',1000,'2021-11-19 18:04:20','2021-11-19 18:04:20'),(18,171,0,'batch 2255','b2255','2021-12-21','2021-12-21',30,5,1,'monthly',2000,'2021-12-17 10:29:52','2021-12-17 10:29:52'),(20,189,36,'Dance','Cv','2022-03-01','2022-05-01',20,10,10,'monthly',20,'2022-02-28 16:33:53','2022-02-28 16:33:53'),(21,172,34,'Dance','This is to display dance steps','2022-03-04','2022-03-05',1,10,10,'monthly',100,'2022-03-03 17:29:26','2022-03-03 17:29:26'),(22,172,37,'Tes','As','2022-03-05','2022-03-05',1,1,1,'monthly',100,'2022-03-05 18:06:03','2022-03-05 18:06:03'),(23,102,16,'Session','Does','2022-03-07','2022-03-07',1,1,1,'monthly',100,'2022-03-07 04:18:26','2022-03-07 04:18:26'),(24,189,0,'Morning batch','one year plan','2022-03-17','2022-12-17',12,25,3,'monthly',500,'2022-03-07 12:55:03','2022-03-07 12:55:03'),(25,189,0,'bharatanatyam','new batch','2022-03-10','2022-08-07',8,25,3,'monthly',500,'2022-03-07 12:56:30','2022-03-07 12:56:30'),(29,185,0,'mohiniyattam 1','new batch','2022-03-15','2023-03-15',12,25,3,'monthly',500,'2022-03-07 13:11:23','2022-03-07 13:11:23'),(30,184,0,'bharatanatyam','new batch','2022-03-15','2022-07-12',8,25,3,'monthly',500,'2022-03-07 13:13:20','2022-03-07 13:13:20'),(31,184,0,'bharatanatyam','new batch','2022-03-16','2022-09-07',6,20,3,'monthly',500,'2022-03-07 13:21:51','2022-03-07 13:21:51'),(32,178,0,'oil painting','new batch of 6 months','2022-03-15','2022-09-07',6,30,3,'monthly',500,'2022-03-07 13:27:13','2022-03-07 13:27:13'),(33,178,0,'oil painting batch','new batch','2022-03-15','2022-10-07',8,30,3,'monthly',500,'2022-03-07 13:28:57','2022-03-07 13:28:57'),(34,177,0,'lite music','new batch','2022-03-15','2022-08-16',12,25,3,'monthly',500,'2022-03-07 13:31:41','2022-03-07 13:31:41'),(35,171,0,'batch 2','new batch','2022-03-15','2022-06-07',8,25,3,'monthly',500,'2022-03-07 13:45:16','2022-03-07 13:45:16'),(36,192,73,'Bharathanatyam','Dail class available and also at holidays','2022-03-21','2022-05-21',3,30,3,'monthly',1000,'2022-03-08 10:27:52','2022-03-08 10:27:52'),(37,192,74,'Mohiniyattam','Daily classes available','2022-03-14','2022-05-25',6,30,3,'monthly',500,'2022-03-08 10:29:22','2022-03-08 11:44:23'),(38,172,66,'Batch1','Dance','2022-03-09','2022-03-10',1,10,1,'One time',100,'2022-03-09 01:53:27','2022-03-09 01:53:27'),(39,195,0,'Dance Basic batch','basic dance batch','2022-03-12','2022-03-31',10,10,10,'monthly',1500,'2022-03-09 05:37:58','2022-03-09 05:37:58'),(40,195,0,'Batch Inprogress',NULL,'2022-03-01','2022-03-31',30,10,10,'monthly',1000,'2022-03-11 11:41:44','2022-03-11 11:41:44'),(41,195,0,'Batch Completed',NULL,'2022-02-10','2022-03-10',10,10,10,'monthly',1000,'2022-03-11 11:42:22','2022-03-11 11:42:22'),(42,195,0,'Batch Upcoming',NULL,'2022-04-01','2022-04-30',30,10,10,'monthly',1000,'2022-03-11 11:42:56','2022-03-11 11:42:56'),(43,192,80,'Mohiniyattam 1','New batch','2022-03-21','2023-03-16',12,25,3,'Monthly',500,'2022-03-16 04:57:26','2022-03-16 04:57:26'),(44,192,81,'Karhak','New form','2022-03-17','2023-01-18',1,25,3,'Monthly',500,'2022-03-16 05:05:00','2022-03-16 05:05:00'),(45,192,80,'Kathak','Kathak batch started','2022-03-17','2022-08-26',6,20,2,'Monthly',500,'2022-03-16 05:10:32','2022-03-16 05:11:26'),(46,213,82,'April Monthly','Monthly data details','2022-04-01','2022-07-19',3,100,15,'Monthly',10,'2022-03-19 13:20:54','2022-03-19 13:20:54'),(47,213,82,'Batch Past','Past Datas','2022-03-01','2022-03-10',1,10,1,'One time',10,'2022-03-19 13:23:12','2022-03-19 13:23:12'),(48,213,82,'Test','Test1','2022-03-20','2022-03-22',2,10,10,'One time',100,'2022-03-20 11:27:24','2022-03-20 11:27:24'),(49,172,67,'Batch','Details','2022-03-22','2022-04-22',31,1,1,'One time',12,'2022-03-21 11:06:36','2022-03-21 11:06:36'),(50,102,0,'old batch','old batch for test','2022-03-01','2022-03-10',10,10,10,'monthly',1500,'2022-03-22 04:48:25','2022-03-22 04:48:25'),(51,102,0,'New batch','new batch for testing','2022-04-01','2022-04-20',10,10,10,'monthly',1500,'2022-03-22 04:49:49','2022-03-22 04:49:49'),(52,102,0,'old batch 1','sdsd','2022-03-03','2022-03-06',10,10,10,'monthly',10,'2022-03-22 04:55:50','2022-03-22 04:55:50'),(53,102,0,'New batch 1',NULL,'2022-04-10','2022-04-20',10,10,10,'monthly',10,'2022-03-22 04:56:18','2022-03-22 04:56:18'),(54,172,83,'Saturday Batch','For employees','2022-04-01','2022-06-30',90,20,10,'Monthly',100,'2022-03-22 12:11:16','2022-03-22 12:11:16'),(55,103,0,'Primary batch','primary batch','2022-04-01','2022-07-31',10,10,10,'monthly',1500,'2022-03-24 10:42:07','2022-03-24 10:42:07'),(56,192,85,'New dance','New batch started','2022-04-01','2022-07-22',112,10,2,'Monthly',450,'2022-03-31 09:55:31','2022-03-31 09:55:31'),(57,192,74,'New','Demo','2022-04-01','2022-07-01',91,10,2,'Monthly',500,'2022-03-31 10:27:34','2022-03-31 10:27:34'),(58,172,83,'Special music','Musical basic','2022-04-08','2022-04-09',1,1,1,'One time',100,'2022-04-06 08:28:34','2022-04-06 08:28:34');
/*!40000 ALTER TABLE `batches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (2,'Dance','uploads/category/XQ1wO4WoQnfqIt78yqXGKm1r910cUc08bEWYTnoH.png',1,'2021-03-12 00:24:31','2022-03-22 09:11:47'),(3,'Music','uploads/category/knKcePDes3ICBxTu8gMurpxy04cP12S71UcYcCZE.png',1,'2021-03-12 00:24:41','2022-03-22 09:12:15'),(4,'Painting','uploads/category/qiCzRaRty0c4Rtom8hoESmUuzwsEM0rVI3wtI6O0.png',1,'2021-03-12 00:24:49','2022-03-22 09:21:44'),(5,'Sculpture','uploads/category/fh8Blp7qT8MnAzYbS6CkTsGBIcMCzb0eBg1DLbhj.png',1,'2021-03-12 00:24:58','2022-03-22 09:20:06'),(6,'Drawing','uploads/category/SslBjJpBIFmR71CL2ekUSaa4AtJNMFIqHTSXAZU1.png',1,'2021-12-17 10:31:11','2022-03-22 09:24:44');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_fields`
--

DROP TABLE IF EXISTS `cms_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cms_fields` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `cms_heading` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `cms_description` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `cms_image` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `display_order` tinyint NOT NULL,
  `button_text` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_fields`
--

LOCK TABLES `cms_fields` WRITE;
/*!40000 ALTER TABLE `cms_fields` DISABLE KEYS */;
INSERT INTO `cms_fields` VALUES (1,'heading2','desc<br>1322','cms/53O3niauhOiZPvey01wrlhCZB8XMAlm6qqP9cWyx.png',2,'button2',1,'2021-03-12 05:22:52','2021-03-17 01:18:09');
/*!40000 ALTER TABLE `cms_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `country_name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'India',1,'2021-03-12 05:00:22','2022-03-07 10:19:53'),(3,'USA',1,'2021-03-12 05:00:44','2022-03-07 10:20:10'),(4,'United Kingdom',1,'2021-03-12 05:00:50','2022-03-07 10:20:54'),(5,'Singapore',1,'2021-03-12 05:00:56','2022-03-07 10:21:34');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generalsettings`
--

DROP TABLE IF EXISTS `generalsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `generalsettings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `commision` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generalsettings`
--

LOCK TABLES `generalsettings` WRITE;
/*!40000 ALTER TABLE `generalsettings` DISABLE KEYS */;
INSERT INTO `generalsettings` VALUES (1,'10');
/*!40000 ALTER TABLE `generalsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `grades` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `grade_name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grades`
--

LOCK TABLES `grades` WRITE;
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
INSERT INTO `grades` VALUES (2,'Basic',1,'2021-03-12 00:24:31','2022-03-07 10:59:55'),(3,'Moderate',1,'2021-03-12 00:24:41','2022-03-07 11:00:04'),(4,'Advanced',1,'2021-03-12 00:24:49','2022-03-07 11:00:15'),(5,'Expert',1,'2021-03-12 00:24:58','2022-03-07 11:00:23');
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `help_desk`
--

DROP TABLE IF EXISTS `help_desk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `help_desk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `description` varchar(191) DEFAULT NULL,
  `status` varchar(191) DEFAULT 'Open',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `help_desk`
--

LOCK TABLES `help_desk` WRITE;
/*!40000 ALTER TABLE `help_desk` DISABLE KEYS */;
INSERT INTO `help_desk` VALUES (1,99,'Lorem Ipsum is simply dummy text','Open','2021-11-19 10:55:51','2021-11-19 10:55:51'),(2,102,'Lorem Ipsum is simply dummy text','Open','2021-11-19 10:55:51','2021-11-19 10:55:51'),(3,103,'Lorem Ipsum is simply dummy text','Open','2021-11-19 10:56:13','2021-11-19 10:56:13'),(4,107,'Lorem Ipsum is simply dummy text','Open','2021-11-19 10:56:13','2021-11-19 10:56:13');
/*!40000 ALTER TABLE `help_desk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_banners`
--

DROP TABLE IF EXISTS `home_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_banners` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `title` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `description` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `image` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `page` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `learn_more_link` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `watch_now_link` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_banners`
--

LOCK TABLES `home_banners` WRITE;
/*!40000 ALTER TABLE `home_banners` DISABLE KEYS */;
INSERT INTO `home_banners` VALUES (4,'2020-10-16','Login Banner','','login-banner.jpg','login','#','#');
/*!40000 ALTER TABLE `home_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_headers`
--

DROP TABLE IF EXISTS `home_headers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `home_headers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `logo` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_links` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `menu` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `copy_right` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_links` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `min_amount` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_terms` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `description_terms` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `title_stream` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `description_stream` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `play_store_link` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `app_store_link` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `footer_logo` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_headers`
--

LOCK TABLES `home_headers` WRITE;
/*!40000 ALTER TABLE `home_headers` DISABLE KEYS */;
INSERT INTO `home_headers` VALUES (1,'buddy-guru.png','a:4:{i:0;a:2:{s:4:\"name\";s:13:\"icon-facebook\";s:4:\"link\";s:1:\"#\";}i:1;a:2:{s:4:\"name\";s:12:\"icon-twitter\";s:4:\"link\";s:1:\"#\";}i:2;a:2:{s:4:\"name\";s:13:\"icon-whatsapp\";s:4:\"link\";s:1:\"#\";}i:3;a:2:{s:4:\"name\";s:12:\"icon-youtube\";s:4:\"link\";s:1:\"#\";}}','a:4:{i:0;a:2:{s:4:\"name\";s:4:\"Home\";s:4:\"link\";s:25:\"http://localhost/worship/\";}i:1;a:2:{s:4:\"name\";s:12:\"Watch Online\";s:4:\"link\";s:37:\"http://localhost/worship/watch-online\";}i:2;a:2:{s:4:\"name\";s:11:\"Church List\";s:4:\"link\";s:33:\"http://localhost/worship/churches\";}i:3;a:2:{s:4:\"name\";s:4:\"Give\";s:4:\"link\";s:35:\"http://localhost/worship/donate-now\";}}','2020 Onlineworship. All rights reserved.','a:3:{i:0;a:2:{s:4:\"name\";s:5:\"About\";s:4:\"link\";s:30:\"http://localhost/worship/about\";}i:1;a:2:{s:4:\"name\";s:15:\"Privacy & legal\";s:4:\"link\";s:45:\"http://localhost/worship/terms-and-conditions\";}i:2;a:2:{s:4:\"name\";s:7:\"Contact\";s:4:\"link\";s:32:\"http://localhost/worship/contact\";}}','5.99',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `home_headers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2021_09_24_083016_create_tutor_portfolio',2),(5,'2021_09_25_081205_add_rating_to_tutor_details',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `packages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `package_name` varchar(191) NOT NULL,
  `description` varchar(191) DEFAULT NULL,
  `subscription_fee` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` VALUES (25,133,'silverdfdf','akkjbavchadssd',23000,'2021-11-12 14:01:57','2021-11-12 14:50:06'),(33,171,'pkg','dec',NULL,'2021-12-17 10:14:14','2021-12-17 10:14:14'),(34,172,'Gold','Gold package',NULL,'2022-02-18 05:13:28','2022-02-18 05:13:28'),(39,189,'premium','one year',NULL,'2022-03-07 12:50:01','2022-03-07 12:50:01'),(40,189,'platinum','8 month',NULL,'2022-03-07 12:51:19','2022-03-07 12:51:19'),(41,189,'gold','6 month',NULL,'2022-03-07 12:51:36','2022-03-07 12:51:36'),(45,185,'silver','3',NULL,'2022-03-07 12:59:01','2022-03-07 12:59:01'),(46,185,'premium','mohiniyattam',NULL,'2022-03-07 13:09:27','2022-03-07 13:09:27'),(49,184,'gold','6 month package',NULL,'2022-03-07 13:20:10','2022-03-07 13:20:10'),(50,184,'silver','3 months',NULL,'2022-03-07 13:20:35','2022-03-07 13:20:35'),(51,102,'Premium','1 Year',NULL,'2022-03-07 13:21:07','2022-03-07 13:21:07'),(52,102,'Platinum','8 Months',NULL,'2022-03-07 13:23:35','2022-03-07 13:23:35'),(53,178,'gold','6 month',NULL,'2022-03-07 13:24:24','2022-03-07 13:24:24'),(54,178,'platinum','8 month',NULL,'2022-03-07 13:24:33','2022-03-07 13:24:33'),(55,103,'Gold','6 Months',NULL,'2022-03-07 13:25:07','2022-03-07 13:25:07'),(56,103,'Silver','3 Months',NULL,'2022-03-07 13:25:23','2022-03-07 13:25:23'),(57,138,'Premium','1 Year',NULL,'2022-03-07 13:26:06','2022-03-07 13:26:06'),(58,138,'Gold','6 Months',NULL,'2022-03-07 13:26:20','2022-03-07 13:26:20'),(59,145,'Platinum','8 Months',NULL,'2022-03-07 13:27:04','2022-03-07 13:27:04'),(60,145,'Silver','3 Months',NULL,'2022-03-07 13:27:11','2022-03-07 13:27:11'),(61,147,'Premium','1 Year',NULL,'2022-03-07 13:27:58','2022-03-07 13:27:58'),(62,147,'Silver','3 Months',NULL,'2022-03-07 13:28:09','2022-03-07 13:28:09'),(63,168,'Platinum','8 Months',NULL,'2022-03-07 13:29:11','2022-03-07 13:29:11'),(64,168,'Silver','3 Months',NULL,'2022-03-07 13:29:19','2022-03-07 13:29:19'),(65,177,'premium','12 month',NULL,'2022-03-07 13:30:29','2022-03-07 13:30:29'),(66,172,'Premium','1 Year',NULL,'2022-03-07 13:30:34','2022-03-07 13:30:34'),(67,172,'Gold','6 Months',NULL,'2022-03-07 13:31:27','2022-03-07 13:31:27'),(68,173,'Premium','1 Year',NULL,'2022-03-07 13:31:55','2022-03-07 13:31:55'),(69,173,'Platinum','8 Months',NULL,'2022-03-07 13:32:33','2022-03-07 13:32:33'),(70,171,'Platinum','8 Months',NULL,'2022-03-07 13:34:09','2022-03-07 13:34:09'),(71,171,'Silver','3 Months',NULL,'2022-03-07 13:34:18','2022-03-07 13:34:18'),(73,192,'Gold','6 month package ,daily sessions available',NULL,'2022-03-08 10:24:51','2022-03-16 05:03:32'),(74,192,'Silver','3 month package',NULL,'2022-03-08 10:25:51','2022-03-08 10:25:51'),(76,195,'Basic Package','this is a basic package',NULL,'2022-03-09 05:36:53','2022-03-09 05:36:53'),(77,195,'Package Inprogress','Inprogress',NULL,'2022-03-11 11:40:37','2022-03-11 11:40:37'),(78,195,'Package Completed','completed',NULL,'2022-03-11 11:40:46','2022-03-11 11:40:46'),(79,195,'Package Upcoming','upcoming',NULL,'2022-03-11 11:40:55','2022-03-11 11:40:55'),(80,192,'Premium','1 year package',NULL,'2022-03-16 04:56:01','2022-03-16 04:56:01'),(82,213,'Dance Package','Package Description',NULL,'2022-03-19 13:18:42','2022-03-19 13:18:42'),(83,172,'Weekend Music','Package for music lovers',NULL,'2022-03-22 12:08:59','2022-03-22 12:08:59'),(84,103,'Primary','primary package',NULL,'2022-03-24 10:40:09','2022-03-24 10:40:09'),(85,192,'Demo','6 month',NULL,'2022-03-31 09:54:39','2022-03-31 09:54:39');
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('admin@gmail.com','$2y$10$Xp1fLBp07Ep.qFvEm7lLHu2rouL..hxoXIxiOVQiBxaiws1yhCZQq','2020-10-31 00:12:48');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `tutor_id` int NOT NULL,
  `batch_id` int NOT NULL,
  `package_id` int DEFAULT NULL,
  `amount` int NOT NULL,
  `payment_method` varchar(191) NOT NULL,
  `card_number` int DEFAULT NULL,
  `card_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `expiry_date` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `cvv` int DEFAULT NULL,
  `payment_status` varchar(191) NOT NULL,
  `email_status` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (4,99,195,40,77,1000,'monthly',NULL,NULL,NULL,NULL,'1',NULL,'2022-03-11 11:49:14','2022-03-26 05:44:16'),(5,99,195,41,78,1000,'monthly',NULL,NULL,NULL,NULL,'1',NULL,'2022-03-11 11:49:14','2022-03-26 05:44:20'),(6,99,195,42,79,1000,'monthly',NULL,NULL,NULL,NULL,'1',NULL,'2022-03-11 11:49:57','2022-03-26 05:44:23'),(17,196,172,54,83,100,'Monthly',2147483647,'Navee',NULL,123,'1',NULL,'2022-03-25 09:23:29','2022-03-25 09:23:29'),(23,99,103,55,84,1500,'monthly',2147483647,'test',NULL,123,'1',NULL,'2022-03-26 04:44:20','2022-03-26 04:44:20'),(25,214,192,37,74,500,'monthly',2147483647,'Test',NULL,123,'1',NULL,'2022-03-31 09:25:48','2022-03-31 09:25:48'),(26,215,192,37,74,500,'monthly',2147483647,'Test',NULL,123,'1',NULL,'2022-03-31 09:31:00','2022-03-31 09:31:00'),(27,214,192,57,74,500,'Monthly',2147483647,'Test',NULL,123,'1',NULL,'2022-03-31 10:28:43','2022-03-31 10:28:43'),(28,196,172,58,83,100,'One time',2147483647,'Test',NULL,123,'1',NULL,'2022-04-06 11:12:24','2022-04-06 11:12:24');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policies`
--

DROP TABLE IF EXISTS `policies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `policies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `points` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policies`
--

LOCK TABLES `policies` WRITE;
/*!40000 ALTER TABLE `policies` DISABLE KEYS */;
INSERT INTO `policies` VALUES (1,'lorem ipsum');
/*!40000 ALTER TABLE `policies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int NOT NULL AUTO_INCREMENT,
  `guru_id` int NOT NULL,
  `title` varchar(191) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,25,'tt','msg'),(2,1002,'tt1','msg2');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skills` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (1,'Painting',1,'2021-03-12 04:52:36','2021-03-12 04:52:36'),(3,'Drawing',1,'2021-03-12 04:53:38','2021-03-12 04:53:38'),(4,'Sculpture',1,'2021-03-12 04:53:45','2022-03-21 06:40:50'),(6,'Dance',1,'2021-12-17 10:32:08','2022-03-21 06:39:54'),(7,'Music',1,'2022-03-07 10:17:12','2022-03-07 10:17:12');
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_details`
--

DROP TABLE IF EXISTS `student_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `parent_name` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_email` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mobile` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade` bigint unsigned NOT NULL,
  `skills` bigint unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_student` (`user_id`),
  KEY `grade` (`grade`),
  KEY `skills` (`skills`),
  KEY `grade_2` (`grade`),
  KEY `skills_2` (`skills`),
  CONSTRAINT `student_details_ibfk_1` FOREIGN KEY (`grade`) REFERENCES `grades` (`id`),
  CONSTRAINT `student_details_ibfk_2` FOREIGN KEY (`skills`) REFERENCES `skills` (`id`),
  CONSTRAINT `user_student` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_details`
--

LOCK TABLES `student_details` WRITE;
/*!40000 ALTER TABLE `student_details` DISABLE KEYS */;
INSERT INTO `student_details` VALUES (42,99,'Varghes','your.email+faker95491@gmail.com','9874563215',3,4,'2021-09-22 00:52:00','2022-03-25 07:52:35'),(43,107,'test parent','sreenath.sahadevan@cordiace.com','123456789',2,1,'2021-10-27 11:46:16','2021-10-27 11:46:16'),(44,112,'sahadevan','test134@gmail.com','123456789',2,1,'2021-10-30 09:58:01','2021-10-30 09:58:01'),(45,125,'sahadevan','test134@gmail.com','123456789',2,1,'2021-11-01 15:04:28','2021-11-01 15:04:28'),(46,135,'haridas','haridas@gmail.com','9527676544',3,3,'2021-11-12 12:42:14','2021-11-12 12:46:36'),(55,169,NULL,NULL,NULL,2,3,'2021-12-13 16:05:08','2021-12-13 16:05:08'),(56,170,NULL,NULL,NULL,3,3,'2021-12-17 09:40:50','2021-12-17 09:41:18'),(57,176,NULL,NULL,NULL,2,3,'2022-02-22 14:07:57','2022-02-22 14:07:57'),(58,187,NULL,NULL,NULL,2,1,'2022-02-25 07:30:35','2022-02-25 07:30:35'),(59,188,NULL,NULL,NULL,5,6,'2022-02-25 07:35:44','2022-02-25 07:35:44'),(60,190,NULL,NULL,NULL,3,3,'2022-03-02 09:45:14','2022-03-02 09:45:14'),(61,191,NULL,NULL,NULL,3,4,'2022-03-08 08:52:47','2022-03-16 05:56:35'),(62,196,NULL,NULL,NULL,4,1,'2022-03-09 16:38:27','2022-03-24 17:27:40'),(63,197,NULL,NULL,NULL,5,6,'2022-03-09 16:48:04','2022-03-09 16:48:04'),(64,198,NULL,NULL,NULL,5,4,'2022-03-09 16:49:51','2022-03-09 16:49:51'),(65,199,NULL,NULL,NULL,5,6,'2022-03-09 16:52:11','2022-03-09 16:52:11'),(66,200,NULL,NULL,NULL,5,4,'2022-03-09 16:53:41','2022-03-09 16:53:41'),(70,206,NULL,NULL,NULL,3,1,'2022-03-15 06:32:13','2022-03-15 06:32:13'),(71,207,NULL,NULL,NULL,5,6,'2022-03-15 06:38:23','2022-03-15 06:38:23'),(72,208,NULL,NULL,NULL,5,6,'2022-03-15 17:59:52','2022-03-15 17:59:52'),(73,209,NULL,NULL,NULL,5,6,'2022-03-15 18:07:18','2022-03-15 18:07:18'),(74,210,NULL,NULL,NULL,2,1,'2022-03-16 04:18:55','2022-03-16 04:18:55'),(75,212,NULL,NULL,NULL,3,6,'2022-03-19 12:42:07','2022-05-31 17:11:09'),(76,214,NULL,NULL,NULL,2,6,'2022-03-21 05:23:52','2022-03-31 09:19:33'),(77,215,NULL,NULL,NULL,2,6,'2022-03-22 03:55:40','2022-03-22 03:56:05'),(78,216,NULL,NULL,NULL,5,7,'2022-03-22 12:02:33','2022-03-22 12:02:33'),(79,217,NULL,NULL,NULL,5,6,'2022-04-11 14:49:26','2022-04-11 14:49:26'),(80,218,NULL,NULL,NULL,5,6,'2022-05-09 06:47:20','2022-05-09 06:47:20'),(81,219,NULL,NULL,NULL,2,6,'2022-05-14 06:16:30','2022-05-14 06:16:30'),(82,220,NULL,NULL,NULL,2,6,'2022-05-18 07:16:31','2022-05-18 07:16:31'),(83,221,NULL,NULL,NULL,3,6,'2022-05-18 07:33:32','2022-05-18 07:33:32'),(84,222,NULL,NULL,NULL,4,1,'2022-05-31 15:18:04','2022-05-31 15:18:04');
/*!40000 ALTER TABLE `student_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint unsigned NOT NULL,
  `sub_category_name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_categories`
--

LOCK TABLES `sub_categories` WRITE;
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` VALUES (1,2,'LKG',1,'2021-03-12 00:45:43','2021-03-12 00:45:43'),(2,4,'maths',1,'2021-03-12 01:40:16','2021-03-12 01:40:16'),(3,3,'UKG',1,'2021-03-12 03:15:26','2022-03-07 10:22:34'),(4,6,'Others',1,'2021-12-17 10:31:41','2021-12-17 10:31:41');
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscribers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tutor_id` int NOT NULL,
  `user_id` int NOT NULL,
  `status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
INSERT INTO `subscribers` VALUES (1,102,99,1),(2,103,99,1),(3,172,196,1),(4,192,214,1),(5,192,215,1);
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terms`
--

DROP TABLE IF EXISTS `terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `terms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `points` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terms`
--

LOCK TABLES `terms` WRITE;
/*!40000 ALTER TABLE `terms` DISABLE KEYS */;
INSERT INTO `terms` VALUES (1,'lorem ipsum rss');
/*!40000 ALTER TABLE `terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_details`
--

DROP TABLE IF EXISTS `tutor_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tutor_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `category` bigint unsigned NOT NULL,
  `sub_category` bigint unsigned NOT NULL,
  `skills` bigint unsigned NOT NULL,
  `about` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `experience` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` enum('0','1','2','3','4','5') CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_tutor` (`user_id`),
  KEY `category` (`category`),
  KEY `sub_category` (`sub_category`),
  KEY `skills` (`skills`),
  CONSTRAINT `tutor_details_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories` (`id`),
  CONSTRAINT `tutor_details_ibfk_2` FOREIGN KEY (`sub_category`) REFERENCES `sub_categories` (`id`),
  CONSTRAINT `tutor_details_ibfk_3` FOREIGN KEY (`skills`) REFERENCES `skills` (`id`),
  CONSTRAINT `user_tutor` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_details`
--

LOCK TABLES `tutor_details` WRITE;
/*!40000 ALTER TABLE `tutor_details` DISABLE KEYS */;
INSERT INTO `tutor_details` VALUES (25,102,5,1,1,'Repudiandae adipisci ut est incidunt id soluta magnam et laboriosam. Ok','[{\"name\":\"in accusantium beatae\",\"designation\":\"Manager\"}]','2021-09-22 01:32:10','2022-03-16 04:37:07','5'),(26,103,2,1,3,'Numquam qui aut nostrum soluta.','[{\"name\":\"dolor expedita et\",\"designation\":\"Manager\"},{\"name\":\"a perspiciatis et\",\"designation\":\"Manager\"}]','2021-09-23 01:14:23','2022-03-16 13:39:06','4'),(32,138,3,1,1,'test about','[{\"name\":\"test 1\",\"designation\":\"test  description\"},{\"name\":\"test 2\",\"designation\":\"test 2 description\"}]','2021-11-12 16:28:52','2022-03-07 10:28:29','0'),(37,145,4,1,1,NULL,NULL,'2021-11-19 17:42:53','2022-03-07 10:08:30','0'),(38,147,2,1,1,NULL,NULL,'2021-11-19 17:51:26','2021-11-19 17:51:35','0'),(40,168,3,1,1,NULL,NULL,'2021-12-13 15:56:37','2022-03-07 10:09:48','0'),(41,171,2,1,1,NULL,NULL,'2021-12-17 09:42:27','2022-03-07 10:12:19','0'),(42,172,3,1,1,'This is about details with description',NULL,'2022-02-14 13:43:20','2022-05-13 12:19:58','3'),(43,173,5,1,1,NULL,NULL,'2022-02-22 14:03:49','2022-03-15 20:03:38','4'),(44,177,3,1,1,NULL,NULL,'2022-02-22 14:09:13','2022-03-07 10:25:39','0'),(45,178,4,1,1,NULL,NULL,'2022-02-22 14:10:38','2022-03-07 10:27:27','0'),(48,184,2,3,1,NULL,NULL,'2022-02-23 04:45:25','2022-02-23 04:45:25','0'),(49,185,5,1,1,NULL,NULL,'2022-02-23 04:48:00','2022-03-07 10:23:26','0'),(50,189,4,1,1,'Phone',NULL,'2022-02-28 16:19:56','2022-03-07 10:30:14','0'),(51,192,2,3,1,'MA.bharathanatyam',NULL,'2022-03-08 10:23:03','2022-03-16 05:27:07','4'),(54,195,5,3,1,NULL,NULL,'2022-03-09 05:35:12','2022-03-09 05:35:12','0'),(55,201,5,3,1,NULL,NULL,'2022-03-09 16:54:39','2022-03-09 16:54:39','0'),(56,202,5,3,1,NULL,NULL,'2022-03-09 16:57:29','2022-03-09 16:57:29','0'),(57,211,2,3,1,NULL,NULL,'2022-03-16 04:25:28','2022-03-16 04:25:28','0'),(58,213,2,3,1,NULL,NULL,'2022-03-19 13:17:03','2022-03-19 13:17:03','0'),(59,223,4,3,1,NULL,NULL,'2022-05-31 15:21:43','2022-05-31 15:21:43','0'),(60,224,6,3,1,NULL,NULL,'2022-06-01 07:30:18','2022-06-01 07:30:18','0');
/*!40000 ALTER TABLE `tutor_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_portfolio`
--

DROP TABLE IF EXISTS `tutor_portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tutor_portfolio` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tutor_portfolio_user_id_foreign` (`user_id`),
  CONSTRAINT `tutor_portfolio_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_portfolio`
--

LOCK TABLES `tutor_portfolio` WRITE;
/*!40000 ALTER TABLE `tutor_portfolio` DISABLE KEYS */;
INSERT INTO `tutor_portfolio` VALUES (3,87,'Hi this test description for portfolio.','My Portfolio 1','uploads/tutor/portfolio/S3l8mkobk0E5ksi7m7zgfjOqw7NK1v9TcGDRUY0m.mp4',NULL,'2021-09-25 01:16:22','2021-09-25 01:16:22'),(53,138,'keyboard,guitar teacher','keyboard','uploads/tutor/portfolio/HWuMdMujWJdD6yEOXeCefgxA9jpMjYp3TFJ8QEs6.mp4','uploads/tutor/portfolio/VUfA3Q3Y6di4m6Rdmv4MjPu5oUSkOjYg0KRKIHtz.jpg','2022-03-07 11:46:17','2022-03-07 11:46:17'),(54,168,'keyboard player','keyboard','uploads/tutor/portfolio/ebsaPpILLGva31nqY8K2Q7h0yMglvtHn8oxLxmlI.mp4','uploads/tutor/portfolio/CPMkfPgX1PG3OSlMfOJnIhczPTqlQopaYRAKOCYf.jpg','2022-03-07 11:50:29','2022-03-07 11:50:29'),(56,177,'keyboard,guitar teacher','keyboard','uploads/tutor/portfolio/2M5R9OzmjwTN7YGXMe4lJcxWfkZvq8S15qvKxpel.mp4','uploads/tutor/portfolio/Crvn5cSMHFUDS2nXul7E43d5b0FjtMyeIBe6U7Ne.jpg','2022-03-07 11:53:17','2022-03-07 11:53:17'),(57,147,'bharathanatyam','classical dance','uploads/tutor/portfolio/SgHClkXwipgPrWKOp506RuI714Yclt4rByuUoc1u.mp4','uploads/tutor/portfolio/bjBki0S3HNU9vOa6iv11HjEsaAGR0v6JGZJCAr1u.jpg','2022-03-07 12:10:23','2022-03-07 12:10:23'),(58,103,'bharathanatyam','classical dance','uploads/tutor/portfolio/if65a3dvTS9W9xe0t77bth61JCajlqKegjimdZWQ.mp4','uploads/tutor/portfolio/7xQtGRhSTmsCFAM1TKGiD0CX8QJ4w86nAFehYYEl.jpg','2022-03-07 12:11:25','2022-03-07 12:11:25'),(60,171,'bharathanatyam','classical dance','uploads/tutor/portfolio/9lwReaFV7D60YWL8ajzpo6thmay7StqZrbGOxbHO.mp4','uploads/tutor/portfolio/4Dqe4rBCSi12IqRP2RARibVFIvXk49CbpkYha7MC.jpg','2022-03-07 12:13:29','2022-03-07 12:13:29'),(61,184,'bharathanatyam','classical','uploads/tutor/portfolio/PrObUxp5jCCHaoD6VwypwnFBPx90iQmWgCszEQkA.mp4','uploads/tutor/portfolio/dKh4MBU5Q8TO1vwV4pZRP0TRX3RR75SKejVshmFu.jpg','2022-03-07 12:14:54','2022-03-07 12:14:54'),(62,102,'pottery','new sculpture','uploads/tutor/portfolio/JQ2n4yZCZA8XsimX8AZtxiA6I9AJKIo3xMwaXHei.mp4','uploads/tutor/portfolio/QS6U6ViZ1gjIXY43FryRkLGDEwJL0gwHfSKV1APV.jpg','2022-03-07 12:17:59','2022-03-07 12:17:59'),(63,173,'pottery','new sculpture','uploads/tutor/portfolio/GB7uEGxqkGJy9t9Hd9NyYtzcJrFH6z7nm546AMpV.mp4','uploads/tutor/portfolio/GWJOSjD8u984hEyb0GMKSr4PG5KE3xsyGu8FKS5U.jpg','2022-03-07 12:20:50','2022-03-07 12:20:50'),(64,145,'Latest drawing','Pencil Drawing','uploads/tutor/portfolio/Q688YoqB3oixySxiOxolVAeDaIxiOWKK1Sl2Y9Mq.mp4','uploads/tutor/portfolio/Hz4QDKIFsuVGfVzmFjpNU3MvudBiPhgo2p0lqNyk.jpg','2022-03-07 12:24:33','2022-03-07 12:24:33'),(65,172,'Latest drawing','Pencil Drawing','uploads/tutor/portfolio/ok9dc6hKhgGRBvaZEBcDsLRGsrynmzBSwdkdXSW3.mp4','uploads/tutor/portfolio/8N0s8aluHB8Xb8fRiCcpW8PFU26CjqGoKPGVp7yE.jpg','2022-03-07 12:25:34','2022-03-07 12:25:34'),(66,189,'Latest drawing','Pencil Drawing','uploads/tutor/portfolio/vHymUImvoxdbbr4gXvcX93GWX028yOD9wMZQPPPn.mp4','uploads/tutor/portfolio/V2Cr8n3CK4sIR7xXfvaXkyx2xrtvYnYCKzc4sVTI.jpg','2022-03-07 12:26:17','2022-03-07 12:26:17'),(67,178,'Latest drawing','Pencil Drawing','uploads/tutor/portfolio/yZaLO0jUzwmqNS8j5d9eHWAZN1aCnSnO5jd3Dw3K.mp4','uploads/tutor/portfolio/nxJSsevyt7rJoxkRTXZhyBjWMvO7bR2CNtreAnb3.jpg','2022-03-07 12:27:50','2022-03-07 12:27:50'),(68,185,'pottery sculpture','new pottery','uploads/tutor/portfolio/UQf2s6OT5PCT1684gSmvd8Ozm5chAjXN3naUqbMJ.mp4','uploads/tutor/portfolio/brI0nTlirAte6ow0odyXyuD5m9YfS70rFGmooNRE.jpg','2022-03-07 12:41:42','2022-03-07 12:41:42'),(69,172,'Bts','Sample','uploads/tutor/portfolio/gcCdUfc0COmG1cmVoQnFcBNq7GSyMmeOc9SlMEuS.mp4','uploads/tutor/portfolio/gcCdUfc0COmG1cmVoQnFcBNq7GSyMmeOc9SlMEuS.png','2022-03-09 01:56:58','2022-03-09 01:56:58'),(73,195,'Custom Dance Steps','Dance Steps','uploads/tutor/portfolio/eZTwasfe3UZvaxmthrSuUWFBnOti1aO9L0x4lgTD.mp4','uploads/tutor/portfolio/eZTwasfe3UZvaxmthrSuUWFBnOti1aO9L0x4lgTD.png','2022-03-09 05:35:44','2022-03-09 05:35:44'),(74,195,'Dance Steps 2','Dance Steps 2','uploads/tutor/portfolio/wLmsP5f9qjOp05pc3yN3hPCAtPAMUHEQOAsGrWJ0.mp4','uploads/tutor/portfolio/wLmsP5f9qjOp05pc3yN3hPCAtPAMUHEQOAsGrWJ0.png','2022-03-09 05:36:17','2022-03-09 05:36:17'),(76,192,'Bharathanatyam, classical dance form','Bharathanatyam','uploads/tutor/portfolio/uH9JvTTIZ8JKP10qE9Qk6iLJGnXWu1HuoyJZ3C2N.mp4','uploads/tutor/portfolio/uH9JvTTIZ8JKP10qE9Qk6iLJGnXWu1HuoyJZ3C2N.png','2022-03-16 05:22:22','2022-03-16 05:22:22');
/*!40000 ALTER TABLE `tutor_portfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_sessions`
--

DROP TABLE IF EXISTS `tutor_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tutor_sessions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `grade_id` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `batch_id` int NOT NULL,
  `session_name` varchar(191) NOT NULL,
  `description` varchar(191) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `meeting_link` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `start_url` text,
  `meeting_id` varchar(191) DEFAULT NULL,
  `what_learn` varchar(191) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_sessions`
--

LOCK TABLES `tutor_sessions` WRITE;
/*!40000 ALTER TABLE `tutor_sessions` DISABLE KEYS */;
INSERT INTO `tutor_sessions` VALUES (10,102,NULL,NULL,3,'Lite Music','Evening batch starts from 5pm to 8pm','2022-03-10','05:00:00',NULL,NULL,NULL,'a:3:{i:0;s:17:\"what we learned 1\";i:1;s:17:\"what we learned 2\";i:2;s:17:\"what we learned 3\";}','2021-11-19 11:01:25','2022-03-07 13:44:41'),(11,147,NULL,NULL,12,'Oil Painting','Afternoon batch starts from 2pm to 6pm','2022-03-25','14:00:00',NULL,NULL,NULL,'N;','2021-11-19 18:05:38','2022-03-28 06:06:32'),(17,138,NULL,NULL,9,'Mohiniyattam','Morning batch starts from 10am to 1pm','2022-04-01','10:00:00',NULL,NULL,NULL,'a:3:{i:0;s:17:\"what we learned 1\";i:1;s:17:\"what we learned 2\";i:2;s:17:\"what we learned 3\";}','2022-02-15 09:23:54','2022-03-07 13:48:40'),(18,102,NULL,NULL,3,'Bharatanatyam','Morning batch starts from 10am to 1pm','2022-04-23','10:00:00',NULL,NULL,NULL,'N;','2022-02-15 11:50:15','2022-03-07 13:49:54'),(19,138,NULL,NULL,9,'Lite Music','Morning batch starts from 10am to 1pm','2022-03-29','10:00:00',NULL,NULL,NULL,'N;','2022-02-15 12:04:29','2022-03-07 13:58:26'),(20,185,NULL,NULL,29,'new morning session','mohiniyatam morning session','2022-03-24','06:00:00',NULL,NULL,NULL,'N;','2022-02-15 12:06:07','2022-03-07 13:50:46'),(21,184,NULL,NULL,30,'new session','afternoon session','2022-03-23','14:00:00',NULL,NULL,NULL,'N;','2022-02-15 13:07:00','2022-03-07 13:49:54'),(22,177,NULL,NULL,34,'new session','starting new lite music session','2022-03-25','07:00:00',NULL,NULL,NULL,'N;','2022-02-15 14:56:17','2022-03-07 13:49:07'),(23,178,NULL,NULL,32,'new session','new painting session','2022-03-18','09:00:00',NULL,NULL,NULL,'N;','2022-02-16 04:31:10','2022-03-07 13:47:53'),(24,189,NULL,NULL,25,'upcoming session','morning session','2022-03-16','10:30:00',NULL,NULL,NULL,'N;','2022-02-16 05:05:52','2022-03-07 13:47:04'),(26,171,NULL,NULL,35,'new session','morning session','2022-03-23','10:00:00',NULL,NULL,NULL,'N;','2022-02-22 14:19:29','2022-03-07 13:46:16'),(28,138,NULL,NULL,9,'mohiniyattam','morning session','2022-03-22','10:00:00',NULL,NULL,NULL,'N;','2022-03-04 13:16:44','2022-03-07 13:43:19'),(39,138,NULL,NULL,9,'mohiniyattam','mohiniyattam morning batch','2022-03-19','08:59:00',NULL,'https://us05web.zoom.us/s/81640906601?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxNjQwOTA2NjAxIiwiZXhwIjoxNjQ2NTk5NzM2LCJpYXQiOjE2NDY1OTI1MzYsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.pGhC_QjO1C8e2Q-CZl48j9qWyT2_0csTsPJ0hAlt7Fo','81640906601','N;','2022-03-06 18:48:56','2022-03-07 13:41:26'),(41,102,NULL,NULL,3,'sculpture','sculpture third batch','2022-03-18','17:30:00',NULL,'https://us05web.zoom.us/s/85402654304?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg1NDAyNjU0MzA0IiwiZXhwIjoxNjQ2NjAwMDQ1LCJpYXQiOjE2NDY1OTI4NDUsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.anRh-dthB23LzArtzvyV3lZNkQM5eoUG1fgKvkrpay0','85402654304','N;','2022-03-06 18:54:05','2022-03-07 13:40:33'),(42,102,NULL,NULL,3,'oil painting','oil painting from 1PM to 4 PM','2022-03-15','13:00:00',NULL,'https://us05web.zoom.us/s/88603886533?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg4NjAzODg2NTMzIiwiZXhwIjoxNjQ2NjMzOTMwLCJpYXQiOjE2NDY2MjY3MzAsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.FmjWe0Owci9W8GUvCyxdYm8voDGq8jq4375kNrzZu3Y','88603886533','N;','2022-03-07 04:18:50','2022-03-07 13:38:59'),(43,138,NULL,NULL,9,'bharatanatyam','morning batch from 8 to 12','2022-03-15','08:00:00',NULL,'https://us05web.zoom.us/s/82983216036?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgyOTgzMjE2MDM2IiwiZXhwIjoxNjQ2NjUzNjIyLCJpYXQiOjE2NDY2NDY0MjIsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.EWBwyFGf00ImcwAGAPba6UefzZmoZc6Eubr_D0Nlu4o','82983216036','N;','2022-03-07 09:47:02','2022-03-07 13:37:38'),(44,192,NULL,NULL,37,'Morning session','From 7 to 10','2022-03-11','19:00:32','https://us05web.zoom.us/j/89038513916?pwd=Q0NCWG9vTXBZY3E4OWhQWENrWWNlUT09','https://us05web.zoom.us/s/89038513916?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg5MDM4NTEzOTE2IiwiZXhwIjoxNjQ2NzQ3MDI0LCJpYXQiOjE2NDY3Mzk4MjQsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.M8Mmhqeb8f7bXbI8CUebIZNiniSQ1ioBUGvmY-S8P0w','89038513916','N;','2022-03-08 11:43:44','2022-03-08 11:44:40'),(46,172,NULL,NULL,38,'Session1','Dance','2022-03-10','08:00:20','https://us05web.zoom.us/j/81098103872?pwd=eU5kZXRjSmRLeFFiRytzSTZjRkxLdz09','https://us05web.zoom.us/s/81098103872?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxMDk4MTAzODcyIiwiZXhwIjoxNjQ2Nzk4MDY5LCJpYXQiOjE2NDY3OTA4NjksImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.GXAG5S6XxbM4XUUVg8H_NIo3y6hPV3tvKIzNrBK8ljc','81098103872','N;','2022-03-09 01:54:29','2022-03-09 01:54:29'),(47,195,NULL,NULL,39,'First session basic dance steps','learn basic dance steps','2022-03-13','12:30:00','https://us05web.zoom.us/j/83302865415?pwd=N3J1Qy9lQWMrOHRtcHczTXZhbGdYUT09','https://us05web.zoom.us/s/83302865415?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzMzAyODY1NDE1IiwiZXhwIjoxNjQ2ODExNTQ1LCJpYXQiOjE2NDY4MDQzNDUsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.68V7-sG0G2iV-V5yF3lqy0j8ySje_QfBB6uOUL07NyQ','83302865415','N;','2022-03-09 05:39:05','2022-03-09 05:39:05'),(48,195,NULL,NULL,39,'Second session basic steps','Learn second session','2022-03-14','00:30:02','https://us05web.zoom.us/j/83569023738?pwd=WVdhbFlDdGF3VFpMbVlSZ2lkaEUrQT09','https://us05web.zoom.us/s/83569023738?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzNTY5MDIzNzM4IiwiZXhwIjoxNjQ2ODExNzMxLCJpYXQiOjE2NDY4MDQ1MzEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.cXmzMySB3EL5wS0CG9UFiB_MzP4fL6O-2HX4F879cRU','83569023738','N;','2022-03-09 05:42:11','2022-03-09 05:42:11'),(49,195,NULL,NULL,41,'completed session 1','completed session 1','2022-02-20','17:14:00','https://us05web.zoom.us/j/88105817456?pwd=TFN3Vk5ML2Zya3BtUHB6UnFjWnZ2Zz09','https://us05web.zoom.us/s/88105817456?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg4MTA1ODE3NDU2IiwiZXhwIjoxNjQ3MDA2MjU5LCJpYXQiOjE2NDY5OTkwNTksImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.kLowgM2fvD3p0Ql9H3UXt-f-5Y7iBk606WaemSZyUPo','88105817456','N;','2022-03-11 11:44:19','2022-03-11 11:44:19'),(50,195,NULL,NULL,41,'completed session 2','completed session 2','2022-03-01','17:14:00','https://us05web.zoom.us/j/81263236311?pwd=dDY2aEdYV01vbU44U3JBa0ZaVGhBUT09','https://us05web.zoom.us/s/81263236311?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxMjYzMjM2MzExIiwiZXhwIjoxNjQ3MDA2Mjg2LCJpYXQiOjE2NDY5OTkwODYsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.njp4Ob7pl56bJcaaolLASdL-jIkdFO8O4nzYDfoW5d4','81263236311','N;','2022-03-11 11:44:46','2022-03-11 11:44:46'),(51,195,NULL,NULL,40,'inprogress session 1','inprogress session 1','2022-03-05','17:17:00',NULL,'https://us05web.zoom.us/s/87659364138?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg3NjU5MzY0MTM4IiwiZXhwIjoxNjQ3MDA2MzE5LCJpYXQiOjE2NDY5OTkxMTksImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.93_hw5OeTRw6XSTAzZnXlMpLLrw7gNYoMwGafH4IPKM','87659364138','N;','2022-03-11 11:45:19','2022-03-11 11:45:28'),(52,195,NULL,NULL,40,'inprogress session 2','inprogress session 2','2022-03-20','17:20:00','https://us05web.zoom.us/j/88984419857?pwd=M1AvWHNra2VnWnJDMWFKOThIN3JhUT09','https://us05web.zoom.us/s/88984419857?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg4OTg0NDE5ODU3IiwiZXhwIjoxNjQ3MDA2MzUwLCJpYXQiOjE2NDY5OTkxNTAsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.Gx9egNCLdMk3t3amAFsCOtRR9qEPwgatYwuMfj05TTU','88984419857','N;','2022-03-11 11:45:50','2022-03-11 11:45:50'),(53,195,NULL,NULL,42,'upcoming session 1','upcoming session 1','2022-04-02','17:20:00','https://us05web.zoom.us/j/83708322159?pwd=OHFsSURYaDNkVjR3dXFyamYwdFpTdz09','https://us05web.zoom.us/s/83708322159?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzNzA4MzIyMTU5IiwiZXhwIjoxNjQ3MDA2MzgxLCJpYXQiOjE2NDY5OTkxODEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.oYBvQzABVYpXq2KyKzyc0kt0y9hZ8SJMzZ6qSCJTQHY','83708322159','N;','2022-03-11 11:46:21','2022-03-11 11:46:21'),(54,195,NULL,NULL,42,'upcoming session 2','upcoming session 2','2022-04-10','17:18:00','https://us05web.zoom.us/j/83193280364?pwd=Yjd2VkNPb1RIS2ZQYlFrbVMvaS9MQT09','https://us05web.zoom.us/s/83193280364?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzMTkzMjgwMzY0IiwiZXhwIjoxNjQ3MDA2NDA0LCJpYXQiOjE2NDY5OTkyMDQsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.IcdOke_rERiPv3NI0J97GgjJsj8510rKIxikfzjXdLc','83193280364','N;','2022-03-11 11:46:44','2022-03-11 11:46:44'),(55,195,NULL,NULL,42,'upcoming session 3','upcoming session 3','2022-03-31','10:14:00','https://us05web.zoom.us/j/84923885774?pwd=Y21qOFdlb2hWVzFLWDhBS2ljZVhYQT09','https://us05web.zoom.us/s/84923885774?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg0OTIzODg1Nzc0IiwiZXhwIjoxNjQ3MzI2NTgzLCJpYXQiOjE2NDczMTkzODMsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.z4uR-_lJbIoU2XwWwdIp1q75Zs_emKza3aqjO-6ULZg','84923885774','N;','2022-03-15 04:43:03','2022-03-15 04:43:03'),(56,195,NULL,NULL,42,'upcoming session 4','upcoming session 4','2022-03-28','10:16:00','https://us05web.zoom.us/j/87628555964?pwd=Q1FvaUUxQ1N2WjYydkgzL0dobGx0QT09','https://us05web.zoom.us/s/87628555964?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg3NjI4NTU1OTY0IiwiZXhwIjoxNjQ3MzI2NjA0LCJpYXQiOjE2NDczMTk0MDQsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.1maZvC26SqP4QNDuDsqvp7KjybmmH7Iz6k3GM8QTGcc','87628555964','N;','2022-03-15 04:43:24','2022-03-15 04:43:24'),(57,195,NULL,NULL,40,'inprogress session 3','inprogress session 3','2022-03-10','10:20:00','https://us05web.zoom.us/j/81773867008?pwd=Ykphb3cxODhLTmZhbDdRNUo1Nlhudz09','https://us05web.zoom.us/s/81773867008?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxNzczODY3MDA4IiwiZXhwIjoxNjQ3MzI2ODM3LCJpYXQiOjE2NDczMTk2MzcsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.tugxXsHjGzS9lujO2LJ09vDkUB4JYevrXkTUudY_6YM','81773867008','N;','2022-03-15 04:47:17','2022-03-15 04:47:17'),(58,195,NULL,NULL,40,'inprogress session 4','inprogress session 4','2022-03-22','10:20:00','https://us05web.zoom.us/j/86964516613?pwd=VUdWckIvaEhtVFBUb2xJL21UNGR6Zz09','https://us05web.zoom.us/s/86964516613?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg2OTY0NTE2NjEzIiwiZXhwIjoxNjQ3MzI2ODcyLCJpYXQiOjE2NDczMTk2NzIsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.YcBe1VST7C9U7q-lS-mIzZmV70BfygRWQ9qtuEcVfTs','86964516613','N;','2022-03-15 04:47:52','2022-03-15 04:47:52'),(60,192,NULL,NULL,45,'Session 2','New session,11 to 2','2022-03-23','11:00:23','https://us05web.zoom.us/j/83905592363?pwd=NGRrbXZ1dEJGSDdYanMxaFZaVlkyQT09','https://us05web.zoom.us/s/83905592363?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzOTA1NTkyMzYzIiwiZXhwIjoxNjQ3NDE0ODA4LCJpYXQiOjE2NDc0MDc2MDgsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.XofKUo2zwG5bo-qh2FA_8YeE1IB1xEu_k4O2bx0OxXs','83905592363','N;','2022-03-16 05:13:28','2022-03-16 05:13:28'),(61,213,NULL,NULL,46,'Session 1','Dance Basics','2022-04-19','18:56:06','https://us05web.zoom.us/j/81185175460?pwd=cnU4UmNET1ZKTG9sMG9NenR3Tk9UQT09','https://us05web.zoom.us/s/81185175460?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxMTg1MTc1NDYwIiwiZXhwIjoxNjQ3NzAzNDU2LCJpYXQiOjE2NDc2OTYyNTYsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.ZSZOaQXwlkruZQ6NeMzh15_z2-066ZSd6CPtzViBdCs','81185175460','N;','2022-03-19 13:24:16','2022-03-19 13:24:16'),(62,213,NULL,NULL,47,'Past session','Session Data','2022-03-19','18:55:21','https://us05web.zoom.us/j/81054437565?pwd=SE1OelZSRWFFYlpwdllUT2F2Y3dsQT09','https://us05web.zoom.us/s/81054437565?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxMDU0NDM3NTY1IiwiZXhwIjoxNjQ3NzAzNTI2LCJpYXQiOjE2NDc2OTYzMjYsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.fpVqrLjC-UY3wDGVoyXRbeaKCWQhvKnvL4UCehjYEb8','81054437565','N;','2022-03-19 13:25:26','2022-03-19 13:25:26'),(63,213,NULL,NULL,48,'Session1','Session2','2022-03-21','17:14:00','https://us05web.zoom.us/j/83945742563?pwd=UG9qVzRuamtRM2tiY1h1a1E1WHVuZz09','https://us05web.zoom.us/s/83945742563?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgzOTQ1NzQyNTYzIiwiZXhwIjoxNjQ3NzgzODUxLCJpYXQiOjE2NDc3NzY2NTEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.oIbPSBfICOh43T8JHoGsCLOnfu_RjsYhVcHi7gjUjMA','83945742563','N;','2022-03-20 11:44:11','2022-03-20 11:44:11'),(64,102,NULL,NULL,1,'session new','this is session 1','2022-03-21','12:56:00','https://us05web.zoom.us/j/81526986860?pwd=Smp4QkhOdmFWVFdpNWlkSDFreFlrZz09','https://us05web.zoom.us/s/81526986860?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxNTI2OTg2ODYwIiwiZXhwIjoxNjQ3ODU0Njc3LCJpYXQiOjE2NDc4NDc0NzcsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.LtQ40iYIdG66ML4XYLJjnm8bRnGScfn4DpC-qoTFSXU','81526986860','N;','2022-03-21 07:24:37','2022-03-21 07:24:37'),(65,172,NULL,NULL,49,'A','W2e','2022-03-23','16:39:39','https://us05web.zoom.us/j/86422375181?pwd=R3dxT1Fqd2hvQkEzVmZlbG9ZNFJXUT09','https://us05web.zoom.us/s/86422375181?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg2NDIyMzc1MTgxIiwiZXhwIjoxNjQ3ODY4MTgyLCJpYXQiOjE2NDc4NjA5ODIsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.GG98MsF46-Q93bf7vLIQMOn9VyHyaBfg163-qh23KiM','86422375181','N;','2022-03-21 11:09:42','2022-03-21 11:09:42'),(66,172,NULL,NULL,54,'Session 1','Sample','2022-05-01','17:42:52','https://us05web.zoom.us/j/88277909774?pwd=RVBDSi9JUnNIRytHbTVKZ1FvYkxJdz09','https://us05web.zoom.us/s/88277909774?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg4Mjc3OTA5Nzc0IiwiZXhwIjoxNjQ3OTU4NDAxLCJpYXQiOjE2NDc5NTEyMDEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.EFXebkoZ5JXa0DKAR6Zh2148xG2dCsp1CXJwVrvhSCA','88277909774','N;','2022-03-22 12:13:21','2022-03-22 12:13:21'),(67,103,NULL,NULL,55,'new session','sdsdfff','2022-03-31','12:40:00','https://us05web.zoom.us/j/82971325404?pwd=em1tblRXVk5NZGpqd1IxbStmNHpWZz09','https://us05web.zoom.us/s/82971325404?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgyOTcxMzI1NDA0IiwiZXhwIjoxNjQ4NDU0ODI4LCJpYXQiOjE2NDg0NDc2MjgsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.4xPc4ODen35hVP4Kur65xzKI85AmSG1r5tkq7ufwRRg','82971325404','N;','2022-03-28 06:07:08','2022-03-28 06:07:08'),(68,102,NULL,NULL,1,'session news','Day 1','2022-04-08','14:00:00','https://us05web.zoom.us/j/89966844754?pwd=SG1WLzZrcE1MSys0ZG1DL0hoUmRnUT09','https://us05web.zoom.us/s/89966844754?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg5OTY2ODQ0NzU0IiwiZXhwIjoxNjQ4NDU2MDY2LCJpYXQiOjE2NDg0NDg4NjYsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.F2IwQ-aH4Yt99ugniHYNgnLa0HA4C1I1Knjrm7VIfLI','89966844754','N;','2022-03-28 06:27:46','2022-03-28 06:27:46'),(69,102,NULL,NULL,1,'session news','Day 1','2022-04-08','14:00:00','https://us05web.zoom.us/j/81841984357?pwd=RmVhK3dudGJqT0VkYzJXOTEzdVRkdz09','https://us05web.zoom.us/s/81841984357?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6IjgxODQxOTg0MzU3IiwiZXhwIjoxNjQ4NDU2MDgyLCJpYXQiOjE2NDg0NDg4ODIsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.BWiSCRwjRiU5_hZODj9SbzsJa2TOStnQrXQCb4mHTkU','81841984357','N;','2022-03-28 06:28:02','2022-03-28 06:28:02'),(70,195,NULL,NULL,42,'new session','sdsdfff','2022-04-10','12:04:00','https://us05web.zoom.us/j/84234160712?pwd=eDgrYThSZFpvQklUSUdteHVITitYZz09','https://us05web.zoom.us/s/84234160712?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg0MjM0MTYwNzEyIiwiZXhwIjoxNjQ4NDU2MzIxLCJpYXQiOjE2NDg0NDkxMjEsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.Rpi781vigGB0lReMYijck_2o1Nm9Viv7bDT7YyypMQs','84234160712','N;','2022-03-28 06:32:01','2022-03-28 06:32:01'),(71,195,NULL,NULL,42,'new session','sdsdfff','2022-04-11','12:04:00',NULL,'https://us05web.zoom.us/s/87304961070?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg3MzA0OTYxMDcwIiwiZXhwIjoxNjQ4NDU2MzQ4LCJpYXQiOjE2NDg0NDkxNDgsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.JKcD1a3yCVzSaLZuRL_Ja-wSMA-hbI0RycLrcQZPtZo','87304961070','N;','2022-03-28 06:32:28','2022-03-28 06:35:47'),(72,195,NULL,NULL,42,'Test session','Test','2022-04-16','17:30:52','https://us05web.zoom.us/j/86596087369?pwd=Uld4TGZEYWlOOGQxV0k2WkQzM25TUT09','https://us05web.zoom.us/s/86596087369?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg2NTk2MDg3MzY5IiwiZXhwIjoxNjQ4NDU2ODU4LCJpYXQiOjE2NDg0NDk2NTgsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.cPd7BV-8IlD2l_X07OgpXdqdglPnTZh2nKSaAZYAtoQ','86596087369','N;','2022-03-28 06:40:58','2022-03-28 06:43:15'),(73,172,NULL,NULL,58,'Dance','Session1','2022-04-08','16:36:11','https://us05web.zoom.us/j/85935609245?pwd=UkZ2WHNPMkdXVmJEVU5zSy9zVWpoQT09','https://us05web.zoom.us/s/85935609245?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6ImRBWncweUVVUzZDOXZ4T0licUdKNGciLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEwMCwid2NkIjoidXMwNSIsImNsdCI6MCwibW51bSI6Ijg1OTM1NjA5MjQ1IiwiZXhwIjoxNjQ5MjUwMzc1LCJpYXQiOjE2NDkyNDMxNzUsImFpZCI6Ik1rOWdPdS1ZU2hLTkw1ZF9QMUZuU1EiLCJjaWQiOiIifQ.4qvzLgYt08WAWyP2OcWTCOG1bVL_tXwm-esQCJ5m5H0','85935609245','N;','2022-04-06 11:06:15','2022-04-06 11:06:15');
/*!40000 ALTER TABLE `tutor_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_subscriptions`
--

DROP TABLE IF EXISTS `user_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_subscriptions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `batch_id` int NOT NULL,
  `package_id` int NOT NULL,
  `stripe_subscription_id` varchar(191) NOT NULL,
  `stripe_customer_id` varchar(191) NOT NULL,
  `sub_schedule_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `created` varchar(191) NOT NULL,
  `plan_period_start` varchar(191) DEFAULT NULL,
  `plan_period_end` varchar(191) DEFAULT NULL,
  `status` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_subscriptions`
--

LOCK TABLES `user_subscriptions` WRITE;
/*!40000 ALTER TABLE `user_subscriptions` DISABLE KEYS */;
INSERT INTO `user_subscriptions` VALUES (12,99,55,84,'sub_1KhRU8FYYVrPIFCxlK3JmdK1','cus_LODvJT9PF8ixS2','sub_sched_1KhRU8FYYVrPIFCxGve4ngoy','1648269860','1648269860','1658810660','active'),(14,214,37,74,'sub_1KjKGGFYYVrPIFCxUulu2WrK','cus_LQAb16uzeZKDan','sub_sched_1KjKGGFYYVrPIFCxfpsGRYsP','1648718748','1648718748','1656581148','active'),(15,215,37,74,'sub_1KjKLIFYYVrPIFCxsXtgqso7','cus_LQAgL7TUz2fTBr','sub_sched_1KjKLIFYYVrPIFCxjxTrZmTO','1648719060','1648719060','1656581460','active');
/*!40000 ALTER TABLE `user_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` bigint unsigned NOT NULL,
  `subscription` int DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp_verification` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `token` (`token`),
  KEY `location` (`location`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`location`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (87,'admin','admin','admin@gmail.com',NULL,'',NULL,1,NULL,NULL,'$2y$10$jmUVJjBFRYO1yUbfLmIk7.VZr/SAoT.ahsvYUnDOeLM8rNESpgYh6','wv1AqPwhbAcYOVbnECW2DyCt',NULL,NULL,NULL,NULL,'2021-09-25 00:44:49'),(99,'Sreenath','student','sreenath.sahadevan@gmail.com','1995-01-31','','9605469651',1,NULL,NULL,'$2y$10$1ndoZVufPaueKKI1.a2nsO1lARCAfPAvCM.h.BBzQ9vEwMlnn3jjm','Ov6UxB2vHkRgvLBVPvwn2enm',NULL,'uploads/PWtTTJtz7hwrrEO15awQu6u3nw2X7R2X5lcT815a.png',NULL,'2021-09-22 00:52:00','2022-05-09 07:00:01'),(102,'Ravi Varma','tutor','cordiacetracker@gmail.com','1992-02-22','','9874563219',3,NULL,NULL,'$2y$10$pGq6vVbRMTH0GSN2dGSECu6czusfUXhwCYNG2L/wmINNoJoZ/F.Ba','AaLZVk4t4BjxJbkdwadrExh2',NULL,'uploads/BJOVka2T6e0h90ILS1LsH20xh5o1DtYy8yARqlDx.jpg',NULL,'2021-09-22 01:32:09','2022-03-22 06:16:35'),(103,'Tijo Joseph','tutor','tijojoseph823@gmail.com','1992-02-22','','9874563215',1,NULL,NULL,'$2y$10$8nGQmkHD7d.Ibu12W5Ixq.PT7/ERaroTyJ84EmG5ne/GOzvxX9Rj.','5d2fCtNpnB7cZFDiTPSvO2e9',NULL,'',NULL,'2021-09-23 01:14:22','2022-03-07 09:59:49'),(107,'Arjun','student','arjunalex65@gmail.com','2014-12-30','','0123471258',1,NULL,NULL,'$2y$10$c1S.Y1PD/RuKlr68EtoBteUSnjxWhT1PMwdGVVIG86ff6zaxGjzVS','uMVfXedu396HyzYfaWLGU0db',NULL,'',NULL,'2021-10-27 11:46:16','2022-03-07 10:52:26'),(112,'Manu Mohan','student','manumohan@gmail.com','1995-01-31','','9687675345',3,NULL,NULL,'$2y$10$iKsiSwI.sj9maNSGCkb6hOuezlhziyGoKlfwmiqPwgJlS5POSw2k2','q6zHPRszsT2GJYwyzfCZKmr0',NULL,'',NULL,'2021-10-30 09:58:01','2022-03-10 04:08:21'),(125,'Sam Thomas','student','samthomas44@gmail.com','1995-01-31','','9605469652',1,NULL,NULL,'$2y$10$/Aq8FGkLivWmGUubsPEQkOVmCdd6iRo1OWttTn/bZ5Dy5z7TIJW9.','x2xLlONSXnuoHz3DO1fBUq9s',NULL,'',NULL,'2021-11-01 15:04:28','2022-03-07 11:33:57'),(135,'Sneha','student','sneha@gmail.com','2021-11-14','','9098787666',1,NULL,NULL,'$2y$10$2KE8YquI7G4W7OL1J6xwAO8sPF9.52wEqprGwLYF.45mkpDGeCQvm','0eSpQWfBhtfcOAAtWINA42h0',NULL,'',NULL,'2021-11-12 12:42:14','2022-03-07 11:33:19'),(138,'John','tutor','john1995@gmail.com','1992-02-22','','0123456789',1,NULL,NULL,'$2y$10$C61UTrHFPMuVtcyb3Et1tetpFidNYg4BAcxUhGvzHTdrAFRRzJk4a','Oz86GOwGayvBQ7D3DGObtrly',NULL,'',NULL,'2021-11-12 16:28:52','2022-03-11 05:19:15'),(145,'Dhruv','tutor','druvjohnson@gmail.com','2021-11-10','','1240523658',1,NULL,NULL,'$2y$10$WDl7swb0Vy7XTF9UN5WpU.lfhwYS9QnxKUo1/LrQTDjMPNreexhmq','4xRRVy2L16nSxhQkNtzH5ZS8',NULL,'',NULL,'2021-11-19 17:42:53','2022-03-07 10:08:30'),(147,'Rahul Raj','tutor','rahulrajguru@gmail.com','2003-01-08','','9852410236',1,NULL,NULL,'$2y$10$zUUg8HLXk5UHJiv2JL30qeCNKcFGqqxRtktCmSBV5eT3O06D2xPh.','2yVoAFO6gG0BDBFaOkB3aemT',NULL,'',NULL,'2021-11-19 17:51:26','2021-11-19 17:51:26'),(168,'Anika Davis','tutor','anikadavis67@gmail.com','1995-10-10','','8899665544',3,NULL,NULL,'$2y$10$WoFTcC9dkmWCREFehes1A.pKhoGBfiGfK.Fp.JzLquNo32sX6AE8C','axvpLsbznvRbNJM4tnOW4E52','5576','',NULL,'2021-12-13 15:15:02','2022-03-07 10:13:00'),(169,'Praveen','student','gokulst@gmail.com','1995-01-10','gokulst','9988774455',1,NULL,NULL,'$2y$10$yTmKuJpbarZIetbPLfmIpu6QQ756D82YQAfH1VGhwMVhtgE.Y9NoS','6rdlhTo2Q6XiztL4uDuhOFEl','4409',NULL,NULL,'2021-12-13 16:01:15','2022-03-26 09:52:50'),(170,'Ajith','student','ajith@gmail.com','1995-10-10','','9988776655',3,NULL,NULL,'$2y$10$i9GEp1mhJi.vnGe9af7CdOgjyM1fbUtZnbKnme5xO/Bp4KR.Ii9S2','gAW6MpNlSDd6UT3X6JauQptV',NULL,'',NULL,'2021-12-17 09:40:50','2022-03-07 11:33:36'),(171,'Ajith Babu','tutor','ajithguru@gmail.com','1995-10-10','','9988775566',1,NULL,NULL,'$2y$10$Rn0HTTRuC7rA1UVIg6RN9u9AFtlKSNRJ.pTbQiPH/.VyZtoyInufa','Gg2InMSPxlGOs5c5BKEG0wAU',NULL,'',NULL,'2021-12-17 09:42:27','2022-03-07 10:12:19'),(172,'Juan','tutor','juan@gmail.com','2022-02-14','','1231231231',1,NULL,NULL,'$2y$10$Caq4ltFtX847ygZ/SLbTMO5qq5GZ4ZwCCwwY9muwcQoV2K.qhwzdy','y61DHjk1Ajdszlr546ZJeEkC',NULL,'uploads/Al4CyLh5wrFEoI67WG1vUGEjDzyj8A4fGLtnWMH3.jpg',NULL,'2022-02-14 13:43:20','2022-06-02 19:03:03'),(173,'Naveen','tutor','naveenexperts007@gmail.com','1990-02-02','','1234567890',1,NULL,NULL,'$2y$10$RFKjkjM7KfksH0tLpBPK9eAddk2422rc2l.mGvs..5k4C4objpbpm','rlFQb75HR8QN2P5JLbMNzEDO',NULL,'',NULL,'2022-02-22 14:03:48','2022-06-02 03:28:50'),(176,'Shima','student','shima99@gmail.com','2022-02-22','','1234568888',1,NULL,NULL,'$2y$10$xsMGDyOTMdyes7TJjRlpp.B3jdvNgc..eluYeg/xZUftf0dB3NYo6','qApOm33qB3NtzRZmIhNDaoi6',NULL,'',NULL,'2022-02-22 14:07:57','2022-03-07 11:11:58'),(177,'Jacob','tutor','jacob990@gmail.com','2022-02-22','','1234565555',1,NULL,NULL,'$2y$10$1YWucfyKtRAnPHBB7JeAdelumwLHs0aa1W6jZ4gldTmIbOUX.5aVG','neEwtYkQ3If4JJqRjHchhAPn',NULL,'',NULL,'2022-02-22 14:09:13','2022-03-07 10:25:39'),(178,'Ram Manohar','tutor','rammanohar98@gmail.com','2022-02-22','','2131231231',1,NULL,NULL,'$2y$10$Ne1RaOuoGI4F8Wv7ZDvD2uYYflmhC050btInAYBP/S8u5WCcglJlm','3trt9UE1u4WbaMt06HjXvaUt',NULL,'',NULL,'2022-02-22 14:10:38','2022-03-07 10:27:27'),(184,'Kurian','tutor','kurian7896@gmail.com','1995-01-01','Kurian','7845213645',1,NULL,NULL,'$2y$10$V/o1D/9KncXG7n1EcQQNh.Q/lpETBettDZ4cEUZosmflYac1YQJty','EXZa8Go7KkWjiFMEndR2sjxq',NULL,NULL,NULL,'2022-02-23 04:45:25','2022-02-23 04:45:25'),(185,'Arya','tutor','aryamathav78@gmail.com','1995-01-01','','7845213645',1,NULL,NULL,'$2y$10$Cb4QOovg/6SCHUJJiDzNIuYSOsSX1dIMkw1xeJTKmPsOT1mrQpHFO','aTwU1U5jBZv9sFq2879umyBM',NULL,'',NULL,'2022-02-23 04:48:00','2022-03-07 10:24:58'),(187,'Vivek Venugopal','student','vivek100@gmail.com','1995-01-31','','9605469658',1,NULL,NULL,'$2y$10$RguipDrOSBAfhKiitLQ/QOF18cAd.2tnd4yic2bd9z8OBOUapaSoi','idTKMfQcoKukcz83LS1ZaDoL',NULL,'',NULL,'2022-02-25 07:30:35','2022-03-07 11:34:22'),(188,'Naveen','student','naveen@gmail.com','2022-02-25','','9567432541',1,NULL,NULL,'$2y$10$0QMg29WVH6ennwHuniTLi.oq7nxys5Rx/WzcbDIpvXr8rHm3Vf9d2','GIcWs7aTWpFBSFe5DMCQliq8',NULL,'',NULL,'2022-02-25 07:35:44','2022-03-07 11:05:48'),(189,'Prakash','tutor','prakash666@gmail.com','1974-06-27','','9487932011',5,NULL,NULL,'$2y$10$aiBDF8CSQTvDN98RY21sROD69DL6eKQKj8AzWqbCA/t24cEuN.vjO','sBn2XuVc4Oy5ORQ1XtBwvL02',NULL,'',NULL,'2022-02-28 16:19:56','2022-03-07 10:14:40'),(190,'Johnson','student','johnson1992@gmail.com','1902-03-05','','9847512610',1,NULL,NULL,'$2y$10$kYLIpShpo9bDlwlaUmWXHeDoK.T//Jlzl.qmnsoYGIM7C4zJeHBoy','yOmZhfKbRSsRAlzWbYmNTU1Z',NULL,'',NULL,'2022-03-02 09:45:14','2022-03-07 11:34:33'),(191,'Sreekutty p j','student','sreekuttypj10@gmail.com','2021-03-08','Sreekutty p j','9526665413',1,NULL,NULL,'$2y$10$V5ozoftmtEIxTb1Uf.uVjOivyexoC6JKWIoVdSmaW25iM9nMzCn0W','r2itDiy5jCigQqQc8MbMjnqp',NULL,'uploads/piumHZcS422PETXTxWbv58Tfon6lcfQgQSzTGwvV.jpg',NULL,'2022-03-08 08:52:47','2022-03-16 05:57:38'),(192,'Shifna peter','tutor','Shifnapeter77@gmail.com','2022-01-10','Shifna peter','9526665413',1,NULL,NULL,'$2y$10$h6RNIr08ibWXDWr9dQrQWuA8.yyvxUJJYPhj8OC.Xfg82tWM85ONu','od177d7PVtrL2caDCYg6XLob',NULL,'uploads/yj6LqqO0D23jAEwcfn7UmyCbP3TUVRfQaa4coOUt.jpg',NULL,'2022-03-08 10:23:03','2022-03-31 10:26:47'),(195,'Rahul K','tutor','rahulk@gmail.com','1995-02-08','','9632145874',1,NULL,NULL,'$2y$10$jjjOwr9JA3wd54ilvQWXLu.sw5dWctTh0mvMJJuOr.0sCnnvQ22Ou','c8s2GjGm4NkZ9l1kEmVqJiaz',NULL,'',NULL,'2022-03-09 05:35:12','2022-03-28 06:40:05'),(196,'Naveen Eluvathingal','student','Naveen+1@gmail.com',NULL,'Naveen','1231231231',1,NULL,NULL,'$2y$10$tfgTgvug/oYKxpzS2CEp8eTz3oFQcjbJOqBGvMgCzzBGYTbzSVX4a','INyMHEHeuuHvFVhQqD7Dcd60',NULL,'uploads/8pZf1ZytBfqOTgnu7OzE26dE1QO45axddBtrMUJ3.jpg',NULL,'2022-03-09 16:38:27','2022-04-11 14:52:11'),(197,'Naveen','student','Naveen+22@gmail.com','2002-03-09','Naveen','1231231231',1,NULL,NULL,'$2y$10$LU3xw0N2iQXdK0HLdcQcHe77iYQUJhfNYSMOJVe7u9x4uLfrA5mZG','yO1ODZ5dXJwzFqpRDQY4bXk2',NULL,NULL,NULL,'2022-03-09 16:48:04','2022-03-09 16:48:04'),(198,'Naveen','student','naveenexperts007+1@gmail.com','2022-03-01','Naveen','1233123123',1,NULL,NULL,'$2y$10$vb/inLNjY7OiJGAg6ROme.NJUArbg4JDS5Xu2Kw3B38sOHw6oHqlG','Izv1HjZ29iUjYDcENOFBegnu',NULL,NULL,NULL,'2022-03-09 16:49:51','2022-03-09 16:49:51'),(199,'Naveen','student','Naveen+33@gmail.com','2013-03-09','Naveen','1231231231',1,NULL,NULL,'$2y$10$M18uFs6kQQf1HeeZxX4F2.U/E7GNa.dzwnUBpIZd.PM//pHKJZiTa','QD9Q3WSvIbRS0aWm6BM98bQv',NULL,NULL,NULL,'2022-03-09 16:52:11','2022-03-09 16:52:11'),(200,'Naveen','student','naveenexperts007+2@gmail.com','2022-03-09','Naveen','1531588569',1,NULL,NULL,'$2y$10$NS2CyaHTLCczqej6XulKHu4dpJTBUnxMMmafjnaYMlnXl8NMZvAIy','hKFrzVstmZ6xIOAwuuVW0Jbn',NULL,NULL,NULL,'2022-03-09 16:53:41','2022-03-09 16:53:41'),(201,'Naveen','tutor','naveenexperts007+5@gmail.com','2022-03-01','Naveen','123',1,NULL,NULL,'$2y$10$vxn1ua4Oh6Xrbo1tHLvRGuoKb/LNRj08FJwSzvdIUwN8M2hXOvnNC','aGk1RBiX1yBPZL0NepCWl8Qq',NULL,NULL,NULL,'2022-03-09 16:54:39','2022-03-09 16:54:39'),(202,'Naveen','tutor','Naveen+111@gmail.com','2013-02-09','Naveen','1231231231',1,NULL,NULL,'$2y$10$kllLFhdOTjcjWud.g.fn2O5DLjvisK.qC1ElfG9mzXEbZn/nsxpDO','ljEsNDZwFbEgpvgmXNR9inJi',NULL,NULL,NULL,'2022-03-09 16:57:29','2022-03-09 16:59:51'),(206,'anu','student','anu@gmail.com','2016-03-17','anu','9658740236',1,NULL,NULL,'$2y$10$fl6uqfoRkoEqutl7495dNOAxJ6gWPkXw4dQqBahfG.bKydxVKIPdO','aCa9IpCtl0IjAMKGuPc6c2bO',NULL,NULL,NULL,'2022-03-15 06:32:13','2022-03-15 06:32:13'),(207,'Sreenath','student','sreenath.sahadevan@cordiace.com','2005-03-09','Sreenath','9645123698',1,NULL,NULL,'$2y$10$TPlpDoaD1iZdGDwGurKVWOfAiSQ47wRCBVzDQutkGpJlvqt5KOgZO','2PWnW85T2elattr85hX2knVH',NULL,NULL,NULL,'2022-03-15 06:38:21','2022-03-15 06:38:21'),(208,'Shone','student','naveenexperts007+33@gmail.com','2011-03-15','Shone','1234567899',1,NULL,NULL,'$2y$10$8loceVH57/MwkYYvtqAPUe4/N506Xuoag4GN0UWzYcvqVEjj18VkC','uSmhvd0bJcuBYGYyPnBTbg6K',NULL,NULL,NULL,'2022-03-15 17:59:52','2022-03-15 17:59:52'),(209,'Sample','student','Sample@gmail.com','2022-03-15','Sample','1231231231',1,NULL,NULL,'$2y$10$bA8Lf0apSfaWquvdn0xLdui5589HMvevH.7vUuVnFfnUudzUouDAG','1R6FizGC9KjGfGW0eLs3dU3t',NULL,NULL,NULL,'2022-03-15 18:07:18','2022-03-15 18:07:18'),(210,'sreenath','student','wewedds@gmail.com','1995-01-31','sreenath','9605469652',1,NULL,NULL,'$2y$10$/3P8nX/S8hml8BqWAA1oCe3iwr9AmF0f6V/dzOtC2gMEjYlXfmkzy','R995nEgYkGiRnfVveIgg7vgw',NULL,NULL,NULL,'2022-03-16 04:18:55','2022-03-16 04:18:55'),(211,'Kurian','tutor','kurian789688@gmail.com','1995-01-01','Kurian','7845213645',1,NULL,NULL,'$2y$10$s6hRC0wAI6heQJK/4ZQEJ.FxlGNc/GjjNK3PCH3IR7VSqRMba3Vzi','WbmZNSFIVlW1WCgm6tmNf1Wu',NULL,NULL,NULL,'2022-03-16 04:25:28','2022-03-16 04:25:28'),(212,'Naveen student','student','Naveenexperts007+123@gmail.com','2006-03-12','Naveen student','1231231231',1,NULL,NULL,'$2y$10$hbKhiHvRCAT09qySUIzjp.2P8te58omZUD3Q6WNM/yVJLW31U4GAu','ksda5RFWh3KCbtsc2WkJSdPB',NULL,NULL,NULL,'2022-03-19 12:42:07','2022-06-02 01:56:05'),(213,'John','tutor','Naveenexperts007+1234@gmail.com','2000-03-19','John','1231231231',1,NULL,NULL,'$2y$10$cFsers5hH3mEetxcfw2IAOp6oOQ76IKcPgQRxCPQHAWRQSq31nJ9O','s0HXPJUMFZSapEO1GXXB4adX',NULL,NULL,NULL,'2022-03-19 13:17:03','2022-03-20 10:51:57'),(214,'Unnimaya','student','Unnimayajoshy@gmail.con','2022-03-19','Unnimaya','9546326523',1,NULL,NULL,'$2y$10$RdnvZ2ECP9qn6NY2NzNE.ujYGpc7XTeRdAn1wjWjvlHBKTsEcvbVe','WLsDqyaXvQUzt2NpT1yiId2S',NULL,NULL,NULL,'2022-03-21 05:23:52','2022-03-31 10:28:00'),(215,'Abhi','student','Abhijithnp999@gmail.com','2022-03-13','Abhi','75236589',1,NULL,NULL,'$2y$10$VmVLS2Jp2Hq17TyCFPLBPe.ESWtTmyIhBgl1k2bbw2gbLjQc0evVS','idPhRekgRJXzAq68bMbXTELE',NULL,NULL,NULL,'2022-03-22 03:55:40','2022-03-31 10:11:46'),(216,'Nareen Student','student','Naveenexperts007+444@gmail.com','2010-03-22','Nareen Student','9898989898',1,NULL,NULL,'$2y$10$E4K5UfRDduF23wDr2Q2g2OmHnusDo6i2Ea.zhTESxxbcq122FffVG','9LrS3K4DiQ8x6N70xyi46keI',NULL,NULL,NULL,'2022-03-22 12:02:33','2022-03-22 12:02:33'),(217,'Joe','student','naveenexperts007+555@gmail.com','2022-01-01','Joe','1234567899',1,NULL,NULL,'$2y$10$IvSpKzUxkVcF8pC3rffdMelpvkPEbBC3eFHA59ag6q6xbQl/LQreW','z0BUnY2PbTyiProbpfOZkKXw',NULL,NULL,NULL,'2022-04-11 14:49:26','2022-04-11 14:49:26'),(218,'Shima','student','Naveenexperts007+88@gmail.com','2013-05-09','Shima','1231231231',1,NULL,NULL,'$2y$10$UrlvotKyVo/PbmLQ8sralO8PhYT0y42WKkbct2omXDwm1FVofgNmS','nAwEV1crRUK48FX9yqnyMapT',NULL,NULL,NULL,'2022-05-09 06:47:20','2022-05-09 07:09:09'),(219,'Adarsh','student','adarshs78@gmail.com','1979-05-14','Adarsh','9995008977',1,NULL,NULL,'$2y$10$cFrOd/b1H9SSJN4tV34Quu5OPAae9ysK3LgoZf84sX/UL0H5FDSKi','bCmJOHYoYzJOTEhJsiA6rdv1',NULL,NULL,NULL,'2022-05-14 06:16:30','2022-05-14 06:16:30'),(220,'Suvarna','student','suvarnamishra08@gmail.com','1979-06-08','Suvarna','9819853622',1,NULL,NULL,'$2y$10$IPQhc7qs92dxigV3mMsQzuGHtWkJtE2Tqr1Lm2zt7lwaVNjUTgupW','sqex9Te2ff4p1Yo0gQQ9zWTj',NULL,NULL,NULL,'2022-05-18 07:16:31','2022-05-18 07:16:31'),(221,'Sejal','student','rebellosejal@gmail.com','1996-09-18','Sejal','7021896518',1,NULL,NULL,'$2y$10$wqDutdnWIecuZj6QzEZInOyLbwAes47WO5Y/qevR79mLOg1a6GucG','DAVZ1P181CcQfOawrYp0RiMm',NULL,NULL,NULL,'2022-05-18 07:33:32','2022-05-18 07:33:32'),(222,'ABHIJITH','student','abhijith12348@gmail.com','1999-09-03','ABHIJITH','8113033985',1,NULL,NULL,'$2y$10$9nUeKS1QzNl1AXb/CraTJON..ShROqp/NEW.zbDhKRfu3v8nZNPJ6','jeWjTna79XDY3vMlIndfVIU1',NULL,NULL,NULL,'2022-05-31 15:18:04','2022-05-31 17:53:00'),(223,'ABHIJITH','tutor','pubgnasty157@gmail.com','1999-09-03','ABHIJITH','8113033985',1,NULL,NULL,'$2y$10$YmH4uxiS4wk8wSoD0fLD6uk7g7gGtn4HKvitwJatHbKER912lKVOm','8bYWklp49pjpafojboT6gD4Y',NULL,NULL,NULL,'2022-05-31 15:21:43','2022-05-31 15:21:43'),(224,'Nishil p s','tutor','psnishilps@gmail.com','1994-05-25','Nishil p s','9995175993',1,NULL,NULL,'$2y$10$zF62NRX26AtDak29x2tYBuQdAoEPqS96XzkoBXBFZgblPQU7HspXe','4jf1auoP17pVp24B1Otz6Hgj',NULL,NULL,NULL,'2022-06-01 07:30:18','2022-06-01 07:30:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-04  4:43:14
