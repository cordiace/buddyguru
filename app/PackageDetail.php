<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class PackageDetail extends Model
{
    use Notifiable;
    public $table = "package_detail";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id','package_id','batch_id', 'title','description','video_url','thumbnail'
        // 'class_type','start_date','end_date',
        // 'start_time','end_time','duration','meeting_link','no_of_class', 'total_price'
    ];

    //SessionDetail.php
    // public function user()
    // {
    //     return $this->belongsTo('App\User');
    // }
    // //SessionDetail.php
    // public function tutor()
    // {
    //     return $this->belongsTo('App\User');
    // }
    // select * from `packages` where `packages`.`id` = 118
    public function classes()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Classes','class_id', 'batch_id');
    }
    public function package()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Package','id', 'package_id');
    }
    // public function packages()
    // {
    //     return $this->hasMany('App\Package', 'id','package_id');
    // }

     public function liveClass()
    {
        return $this->hasOne('App\LiveClass','user_id','user_id');
    }


}
