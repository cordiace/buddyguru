<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use MohamedGaber\SanctumRefreshToken\Traits\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','commision','description','email','dob', 'slug','password','role','phone_number','location','token','otp_verification','profile_image','image_file_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['dob'];

    //User.php
    public function payments()
    {
        return $this->hasMany(\App\Payment::class);
    }

    // public function country()
    // {
    //     return $this->hasOne('\App\Country', 'id','location');
    // }

    public function student_detail()
    {
        return $this->hasOne(\App\StudentDetail::class);
    }

    public function tutor_detail()
    {
        // return $this->hasOne(\App\TutorDetail::class);
        return $this->belongsTo('\App\TutorDetail', 'id', 'user_id');
    }

    public function session_detail()
    {
        return $this->hasMany(\App\Sessions::class);
    }
    public function country()
    {
        return $this->belongsTo('\App\Country', 'location', 'id');
    }
    public function userskill()
    {
        return $this->hasOne('App\Skill', 'id','skills');
    }
  
    public function user_category()
    {
        return $this->hasOne('App\Category', 'id','category');
    }
    public function student_category()
    {
        return $this->hasOne('App\Category', 'id','grade');
    }
    public function user_sub_category()
    {
        return $this->hasOne('App\SubCategory', 'id','sub_category');
    }
}