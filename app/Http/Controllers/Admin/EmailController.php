<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use App\Sessions;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Mail;


class EmailController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function send()
    {
        date_default_timezone_set("Asia/Calcutta");
        $sessions = Sessions::where([
            ['session_date','=',date('Y-m-d')],
            ])->whereBetween(
                'session_time', [date("H:i"), date("H:i", strtotime('+30 minutes'))]
        )->join('payments', 'sessions.id', '=', 'payments.session_id')->where('email_status',1)->select('payments.*', 'sessions.session_date','sessions.session_time','sessions.session_link')->get();
        $tutor_emails=array();
        foreach ($sessions as $session) {
            $tutor_emails[$session->session_id]=$session->tutor->email;
            $to=$session->user->email;
            $subject='Buddy Guru Session Reminder';
            $name=$session->user->name;
            $data="Hi You have an upcoming session in Buddy Guru today on ".$session->session_time.isset($session)&&$session->session_link!=''?('link for session is'.$session->session_link):'';
            $session_time=$session->session_time;
            $session_link=$session->session_link!=''?(' link for session is : '.$session->session_link):'';
            $mail = Mail::send([], [], function ($message) use ($to, $name, $session_time, $session_link) {
                $message->to($to)
                    ->subject('Buddy Guru Session Reminder')
                    ->from('buddyguru@cordiace.com', 'Buddy Guru Support')
                    ->setBody('Hi '.$name.', You have an upcoming session in Buddy Guru today on : '.date('h:i a', strtotime($session_time)).'. '.$session_link, 'text/plain');
            });
            $payment=Payment::where('id',$session->id)->first();
            $payment->email_status=0;
            $payment->save();
        }

        foreach ($tutor_emails as $id => $tutor_email) {

            $to=$tutor_email;
            $session= Sessions::where('id',$id)->first();
            if($session->session_email_status==1) {
                $session->session_email_status = 0;
                $session->save();
                $subject = 'Buddy Guru Session Reminder';
                $name = $session->user->name;
                $data = "Hi You have an upcoming session in Buddy Guru today on " . $session->session_time . isset($session) && $session->session_link != '' ? ('link for session is' . $session->session_link) : '';
                $session_time = $session->session_time;
                $session_link = $session->session_link != '' ? (' link for session is : ' . $session->session_link) : '';
                $mail = Mail::send([], [], function ($message) use ($to, $name, $session_time, $session_link) {
                    $message->to($to)
                        ->subject('Buddy Guru Session Reminder')
                        ->from('buddyguru@cordiace.com', 'Buddy Guru Support')
                        ->setBody('Hi ' . $name . ', You have an upcoming session in Buddy Guru today on : ' . date('h:i a', strtotime($session_time)) . '. ' . $session_link, 'text/plain');
                });
            }
        }

        return view('admin.email.send', \compact('tutor_emails','sessions'));
    }
}



