<?php

namespace App\Http\Controllers\Admin;

use App\CmsField;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CmsFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cmsfields=CmsField::all();
        return view('admin.cmsfield.index',\compact('cmsfields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cmsfield.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cms_heading' => ['required', 'string', 'max:255'],
            'cms_description' => ['required', 'string'],
            'display_order' => ['required', 'numeric'],
            'button_text' => ['required','string', 'max:255'],
        ]);

        $file = $request->file('cms_image');
        if($file!="") {
            $path = $file->store('/cms', ['disk' => 'public']);
        }

        $category =  CmsField::create([
            'cms_heading' => $request->cms_heading,
            'cms_description' => $request->cms_description,
            'cms_image' => isset($path)?$path:'',
            'display_order' => $request->display_order,
            'button_text' => $request->button_text,

        ]);
        return redirect()->route('cmsfield.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsfield = CmsField::findOrFail($id);
        return view('admin.cmsfield.create_edit', \compact('cmsfield'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cms_heading' => ['required', 'string', 'max:255'],
            'cms_description' => ['required', 'string'],
            'display_order' => ['required', 'numeric'],
            'button_text' => ['required','string', 'max:255'],
        ]);

        $file = $request->file('cms_image');
        if($file!="") {
            $path = $file->store('/cms', ['disk' => 'public']);
        }

        $cmsfield =  CmsField::updateOrCreate(
            ['id' =>$id],
            [
                'cms_heading' => $request->cms_heading,
                'cms_description' => $request->cms_description,
                'cms_image' => ($request->hasFile('cms_image')) ? $path : ($request->image_url?$request->image_url:null),
                'display_order' => $request->display_order,
                'button_text' => $request->button_text,
            ]
        );
        return redirect()->route('cmsfield.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsfields = CmsField::find($id);
        $cmsfields->delete();

        return redirect()->route('cmsfield.index');
    }
}
