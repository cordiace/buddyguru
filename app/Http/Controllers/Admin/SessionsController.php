<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Grade;
use App\Skill;
use App\SubCategory;
use App\Http\Controllers\Controller;
use App\Sessions;
use App\User;
use App\Batch;
use App\TutorSession;
use App\Payment;
use Illuminate\Http\Request;
use App\Traits\ZoomJWT;
use Mail;

class SessionsController extends Controller
{
    use ZoomJWT;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('is.admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sessions = TutorSession::all();

        return view('admin.sessions.index',\compact('sessions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $gurus = User::where('role', 'tutor')->get();
        $grades = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });

        $batches = Batch::all();
        return view('admin.sessions.create_edit',\compact('batches','category','grades','gurus'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'category_id' => ['required'],
            // 'grade_id' => ['required'],
            'batch_id' => ['required'],
            'session_name' => ['required'],
            'description' => ['required'],
            'date' => ['required', 'date'],
            'time' => ['required', 'date_format:H:i'],
            // 'meeting_link' => ['required'],
        ]);

        $batch = Batch::findOrFail($request->batch_id);

        $data = [
            'topic' => $request->session_name,
            'start_time' => $request->date .'T'.$request->time,
            'agenda' => $request->description,
        ];

        $path = 'users/me/meetings';
        $zoom_response = $this->zoomPost($path, [
            'topic' => $data['topic'],
            'type' => self::MEETING_TYPE_SCHEDULE,
            'start_time' => $this->toZoomTimeFormat($data['start_time']),
            'duration' => 30,
            'agenda' => $data['agenda'],
            'settings' => [
                'host_video' => false,
                'participant_video' => false,
                'waiting_room' => true,
            ]
        ]);

        $result = json_decode($zoom_response->body(), true);

        if($zoom_response->status() === 201)
        {
            $meeting_link = $result['join_url'];
            $start_url = $result['start_url'];
            $meeting_id = $result['id'];
        }

        $session =  TutorSession::create([
            'user_id' => $batch->user_id,
            'category_id' => $request->category_id,
            'grade_id' => $request->grade_id,
            'batch_id' => $request->batch_id,
            'session_name' => $request->session_name,
            'date' => $request->date,
            'time' => $request->time,
            'meeting_link' => $meeting_link,
            'start_url' => $start_url,
            'meeting_id' => $meeting_id,
            'description' => $request->description,
            'what_learn' => serialize($request->what_learn),

        ]);

        $bookings = Payment::where('batch_id',$session->batch_id)->get();
        if($bookings && count($bookings) > 0)
        {
          $batch = Batch::find($session->batch_id);
          foreach ($bookings as $value) {
            $user = User::find($value->user_id);

            $to = $user->email;
            $meeting_link = $session->meeting_link;
            $batch_name = $batch->batch_name;
            $date = $session->date;
            $time = $session->time;
            $mail = Mail::send([], [], function ($message) use ($to, $meeting_link,$batch_name,$date,$time) {
                $message->to($to)
                    ->subject('Buddy Guru Session Notification')
                    ->from('buddyguru@cordiace.com', 'Buddy Guru')
                    ->setBody('A new session created under batch '.$batch_name.' on '.$date.' at '.$time.'. Click on the attached meeting link to join ' . $meeting_link, 'text/plain');
            });
          }
        }

        return redirect()->route('sessions.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($session_id)
    {
        $sessions = TutorSession::findOrFail($session_id);

        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $gurus = User::where('role', 'tutor')->get();
        $grades = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });

        $batches = Batch::all();

        return view('admin.sessions.create_edit', \compact('sessions','batches','category','grades','gurus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // 'category_id' => ['required'],
            // 'grade_id' => ['required'],
            'batch_id' => ['required'],
            'session_name' => ['required'],
            'description' => ['required'],
            'date' => ['required', 'date'],
            'time' => ['required', 'date_format:H:i'],
            // 'meeting_link' => ['required'],
        ]);

        $batch = Batch::findOrFail($request->batch_id);

            $data = [
                'user_id' => $batch->user_id,
                'category_id' => $request->category_id,
                'grade_id' => $request->grade_id,
                'batch_id' => $request->batch_id,
                'session_name' => $request->session_name,
                'date' => $request->date,
                'time' => $request->time,
                'meeting_link' => $request->meeting_link,
                'description' => $request->description,
            ];

        $session =  TutorSession::updateOrCreate(
            ['id' =>$id],
            $data
        );

        $bookings = Payment::where('batch_id',$session->batch_id)->get();
        if($bookings && count($bookings) > 0)
        {
          $batch = Batch::find($session->batch_id);
          foreach ($bookings as $value) {
            $user = User::find($value->user_id);

            $to = $user->email;
            $meeting_link = $session->meeting_link;
            $batch_name = $batch->batch_name;
            $date = $session->date;
            $time = $session->time;
            $mail = Mail::send([], [], function ($message) use ($to, $meeting_link,$batch_name,$date,$time) {
                $message->to($to)
                    ->subject('Buddy Guru Session Notification')
                    ->from('buddyguru@cordiace.com', 'Buddy Guru')
                    ->setBody('A session details updated under batch '.$batch_name.' on '.$date.' at '.$time.'. Click on the attached meeting link to join ' . $meeting_link, 'text/plain');
            });
          }
        }

        return redirect()->route('sessions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sessions = TutorSession::find($id);
        $sessions->delete();

        return redirect()->route('sessions.index');
    }
}
