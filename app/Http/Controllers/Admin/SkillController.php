<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CategorySkills;
use App\Category;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $CategorySkills=CategorySkills::all();
        return view('admin.categorySkill.index',\compact('CategorySkills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.categorySkill.create_edit',\compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'category_type' => ['required'],
            'skill_name' => ['required', 'string', 'max:255'],

        ]);
        $skill =  CategorySkills::create([
           'category_id' =>$request->category_type,
            'skill_name' => $request->skill_name,

        ]);
        return redirect()->route('skill.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skill = CategorySkills::findOrFail($id);
        $category = Category::all();
        return view('admin.categorySkill.create_edit', \compact('skill','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_type' => ['required'],
            'skill_name' => ['required', 'string', 'max:255'],
        ]);
        $skill =  CategorySkills::updateOrCreate(
            ['id' =>$id],
            [  'category_id' =>$request->category_type,],
            [ 'skill_name' => $request->skill_name,]
        );
        return redirect()->route('skill.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
    public function delete($id)
    {
        $skills = CategorySkills::find($id);
        try {
          $skills->delete();
        } catch (\Exception $e) {
          return redirect()->route('skill.index');
        }



        return redirect()->route('skill.index');
    }
}
