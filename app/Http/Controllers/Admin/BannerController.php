<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners=Banner::all();
        return view('admin.banner.index',\compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'location' => ['required', 'string', 'max:255'],
            'image' => ['required', 'mimes:jpeg,jpg,png,gif'],
        ]);
        $file = $request->file('image');
        if ($file) {
            $path = $file->store('/uploads', ['disk' => 'public']);
        }
        $banner =  Banner::create([
            'location' => $request->location,
            'image' => $path,
            'heading' => $request->heading,
            'description' => $request->description,


        ]);
        return redirect()->route('banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('admin.banner.create_edit', \compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'location' => ['required', 'string', 'max:255'],
            'image' => ['sometimes','mimes:jpeg,jpg,png,gif'],
        ]);
        $file = $request->file('image');
        if ($file) {
            $path = $file->store('/uploads', ['disk' => 'public']);
        }
        $banner =  Banner::updateOrCreate(
            ['id' =>$id],
            [ 
                'location' => $request->location,
                'image' => ($file) ? $path : $request->temp_image,
               'heading' => $request->heading,
               'description' => $request->description,

            ]
        );
        return redirect()->route('banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Banner::find($id);
        $categories->delete();

        return redirect()->route('banner.index');
    }
}
