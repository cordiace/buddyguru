<?php
namespace Pion\Laravel\ChunkUpload\Receiver;
namespace App\Http\Controllers\Admin;
use DB;
use \Crypt;
use Session;
use Exception;
use App\Category;
use App\CategorySkills;
use App\Country;
use App\Skill;
use App\SubCategory;
use App\Http\Controllers\Controller;
use App\TutorDetail;
use App\TutorSkills;
use App\TutorPortfolio;
use App\TutorSession;
use Illuminate\Http\Request;
use App\User;
use App\Package;
use App\PackageDetail;
use App\LiveClass;
use App\PackageBatch;
use App\Batch;
use App\HelpDesk;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Traits\VideoFFMpeg;


class TutorController extends Controller
{
  use VideoFFMpeg;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tutorRegister(Request $request)
    {
       
        $data = $request->all();
       

        do {
            $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
        } while (User::where("token", "=", $token_key)->first() instanceof User);

        $path_image = '';
        if($request->hasFile('profile_image')){
            $pro_image = $request->file('profile_image');
            // $path_image = $pro_image->store('/uploads/tutor',['disk' => 'public']);

            $filename =  $pro_image->getClientOriginalName();
            $storage_path = $pro_image->storeAs('tutor/'.$request->user_id,$filename,'s3');
 
            $path_image = Storage::cloud()->url($storage_path);
            $imagefile = $request->file('profile_image');
            $imageFileName= $imagefile->getClientOriginalName();
        }
      
        $user =  User::create([
            'name' => $request->name,
            'commision' => $request->commision,
            'description'=>$request->description,
            'slug' => Str::slug($request->slug),
            'dob' => $request->dob,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'location' => $request->location,
            'token' => $token_key,
            'role' => 'tutor',
            'password' => Hash::make($request->password),
            'profile_image' => $path_image,
            'image_file_name' => $imageFileName
        ]);

        $path_letter = null;
        $cover_letter_fileName = null;
        if($request->hasFile('cover_letter')) {
            $cover_letter = $request->file('cover_letter');
            $filename =  $cover_letter->getClientOriginalName();
            $storage_path = $cover_letter->storeAs('tutor/'.$request->user_id,$filename,'s3');
 
            $path_letter = Storage::cloud()->url($storage_path);
        
   
        //    $path_letter= $request->cover_letter->store('/uploads/tutor/', ['disk' => 'public']);
           $coverletterfile = $request->file('cover_letter');
           $cover_letter_fileName= $coverletterfile->getClientOriginalName();

        }

        if ($request->hasFile('video_url')) {
            $video = $request->file('video_url');

            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor/'.$request->user_id,$filename,'s3');
 
            $path_video = Storage::cloud()->url($storage_path);

            $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
            $thumbnail = "uploads/tutor/" . $filenameWithoutExtenstion . ".png";
            $sec = 10;
            $videofile = $request->file('video_url');
            $video_file_name= $videofile->getClientOriginalName();
           // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }
        if($request->skills){
        foreach ($request->skills as $key=>$value){
            $tutor_skills=TutorSkills::create([
'tutor_id'=> $user->id,
'skill_id'=>$value
            ]);
        }
    }
      
        $tutor_detail=TutorDetail::create([
            'user_id'=>$user->id,
            'category'=>2,
            'sub_category'=>3,
            'skills'=>1,
            'cover_letter'=>$path_letter,
            'cover_letter_fileName'=>$cover_letter_fileName,
            'video_url' => $path_video,
            'video_thumbnail' => $thumbnail,
            'video_file_name' => $video_file_name,
            // 'about'=>$request->about,
            // 'experience'=>json_encode($experience),
        ]);
        return response([
            'status'=> "true",
            "role"=>"tutor" ,
            'token' => $token_key, 'message' => 'Created successfully'
        ], 201);
    }
    
    public function tutorUpdate(Request $request)
    {

$id = $request->id;
        $request->validate([
            'email' => 'required|email|unique:users,email,'.$id,
//            'slug' => 'required|unique:users,slug,'.$id,
//            'phone_number' => 'required|unique:users,phone_number,'.$id,
// 'category' => ['required'],
'dob' => 'required',
            // 'sub_category' => ['required'],
            'name' => 'required',
            'description'=>'required',
            'commision'=>'required',
            'location' => 'required',
            'password' => 'sometimes|confirmed',
            // 'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
            // 'video_url' => 'required|mimes:mp4,mov,ogg,qt|max:20000',
         
        ]);
       
        $user = User::find($id);
        $path_image = '';
        if($request->hasFile('profile_image')){
            if(User::where('id', $id)->exists()){
                $user = User::find($id);
                if($user->profile_image){
                    if(Storage::disk('s3')->exists($user->profile_image)){
                        Storage::disk('s3')->delete($user->profile_image);
                    }
                }
            }
            $pro_image = $request->file('profile_image');
            
            $filename =  $pro_image->getClientOriginalName();
            $storage_path = $pro_image->storeAs('tutor/'.$id,$filename,'s3');
 
            $path_image = Storage::cloud()->url($storage_path);


        }

        if($request->password)
        {
            $data = [
                'name' => $request->name,
                'commision' => $request->commision,
                'description'=>$request->description,
                'email' => $request->email,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'password' => Hash::make($request->password),
                // 'profile_image' => $path_image,
            ];
        }
        else{
            $data = [
                'name' => $request->name,
                'commision' => $request->commision,
                'description'=>$request->description,
                'email' => $request->email,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                // 'profile_image' => $path_image,
            ];
        }
      
    if($request->hasFile('cover_letter')) {
        if(User::where('id', $id)->exists()){
            $TutorDetail = TutorDetail::where('user_id',$user->id)->first();
            if($TutorDetail->cover_letter){
                if(Storage::disk('s3')->exists($TutorDetail->cover_letter)){
                    Storage::disk('s3')->delete($TutorDetail->cover_letter);
                }
            }
        }
        $coverletter = $request->file('cover_letter');
            
        $filename =  $coverletter->getClientOriginalName();
        $storage_path = $coverletter->storeAs('tutor/'.$request->user_id,$filename,'s3');

        $path_letter = Storage::cloud()->url($storage_path);


    

    }

    if ($request->hasFile('video_url')) {
        if(User::where('id', $id)->exists()){
            $TutorDetail = TutorDetail::where('user_id',$user->id)->first();
            if($TutorDetail->video_url){
                if(Storage::disk('s3')->exists($TutorDetail->video_url)){
                    Storage::disk('s3')->delete($TutorDetail->video_url);
                }
            }
        }
     
        // $path_video = $video->store('/uploads/tutor/', ['disk' => 'public']);

        $video = $request->file('video_url');
            
        $filename =  $video->getClientOriginalName();
        $storage_path = $video->storeAs('tutor/'.$request->user_id,$filename,'s3');

        $path_video = Storage::cloud()->url($storage_path);

        $filename = str_replace("uploads/tutor/", "", $path_video);
        $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
        $thumbnail = "uploads/tutor/" . $filenameWithoutExtenstion . ".png";
        $sec = 10;

       // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

    }

        $tutor_data = [
            'category'=>2,
            'sub_category'=>1,
            // 'cover_letter'=>$path_letter,
            // 'video_url' => $path_video,
            // 'video_thumbnail' => $thumbnail,
            // 'skills'=>$request->skills,
            // 'about'=>$request->about,
            // 'experience'=>json_encode($experience),
        ];

        $user =  User::updateOrCreate(
            ['id' =>$id],
            $data
        );
        if ($request->hasFile('profile_image')) {
            $user->update([
                'profile_image' => $path_image
            ]);
        }
      
        $tutor_skills=TutorSkills::where('tutor_id',$user->id)->get();
        if (!empty($tutor_skills)) {
            $tutor_skills=TutorSkills::where('tutor_id',$user->id)->delete();
      }
     
      if($request->skills){
        foreach ($request->skills as $key=>$value){
            $tutor_skills=TutorSkills::create([
'tutor_id'=> $user->id,
'skill_id'=>$value
            ]);
        }
    }
        $tutor_detail =  TutorDetail::updateOrCreate(
            ['user_id' =>$id],
            $tutor_data
        );
        if ($request->hasFile('cover_letter')) {
            $tutor_detail->update([
                'cover_letter'=>$path_letter
            ]);
        }
        if ($request->hasFile('video_url')) {
            $tutor_detail->update([
                'video_url' => $path_video,
                'video_thumbnail' => $thumbnail
            ]);
        }
        return response([
            'status'=> "true",
            'message' => 'updated successfully'
        ], 201);

    }

    public function tutorDelete(Request $request,$id)
    {
       
$TutorUser =  User::find($id)->delete();
return response([
    'status'=> "true",
    'message' => 'deleted successfully'
], 200);
    }


    public function portfolioView(Request $request){
        $TutorPortfolio = TutorPortfolio::where([
            ['user_id',$request->id]
            ])->get();
            return response([
                'status'=> "true",
                
                'data' => $TutorPortfolio,
            ], 201);
    }

    public function portfolioCreate(Request $request){
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'description' => 'required',
             'video_url' => 'required|mimes:mp4,mov,ogg,qt',
           
        ]);

        // if ($request->video_url) {
        //     $video = $request->file('video_url');
        //     $path_video = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);
        // }
        if ($request->hasFile('video_url')) {
            $video = $request->file('video_url');
           
$filename =  $video->getClientOriginalName();
$filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
           
$thumbnail = "uploads/tutor/portfolio/" . $filenameWithoutExtenstion . ".png";
$sec = 10;
$path_video = $video->storeAs('tutor/'.$request->user_id,'portfolio/'.$filename,'s3');

$storage_path = Storage::cloud()->url($path_video);

     
        }

        $userPortfolio =  TutorPortfolio::create([
            'user_id' => $request->user_id,
            'title' => $request->title,
            'description' => $request->description,
            'video_url' => $storage_path,
            'video_thumbnail' => $thumbnail,
        ]);
        return response([
            'status'=> "true",
            'Message' =>"Created successfully",

            'data' => $userPortfolio,
        ], 201);

    }
    public function portfolioUpdate(Request $request,$id){
        $request->validate([
            'title' => 'required',
            'description' => 'required',
             'video_url' => 'sometimes|mimes:mp4,mov,ogg,qt|max:20000',
          
        ]);
        if ($request->hasFile('video_url')) {
            if(User::where('id', $id)->exists()){
                $TutorPortfolio = TutorPortfolio::where('user_id',$user->id)->first();
                if($TutorPortfolio->video_url){
                    if(Storage::disk('s3')->exists($TutorPortfolio->video_url)){
                        Storage::disk('s3')->delete($TutorPortfolio->video_url);
                    }
                }
            }
           
            $video = $request->file('video_url');
            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor/'.$request->user_id,'portfolio/'.$filename,'s3');

            $path_video = Storage::cloud()->url($storage_path);
        }

        if ($request->video_thumbnail) {
            $video = $request->file('video_thumbnail');
            $path_video_thumbnail = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);
        }
        $userPortfolio =  TutorPortfolio::updateOrCreate(
            ['id' => $request->id],
        [
            'title' => $request->title,
            'description' => $request->description,
            'video_url' => ($request->video_url) ? $path_video : $request->temp_video_url,
            'video_thumbnail' => ($request->video_thumbnail) ? $path_video_thumbnail : $request->temp_video_thumbnail,
        ]);

        return response([
            'status'=> "true",
            'Message' =>"Updated successfully",

            'data' => $userPortfolio,
        ], 201);
    }
    public function portfolioDelete($id){
        $userPortfolio = TutorPortfolio::find($id);
        $userPortfolio->delete();
        return response([
            'status'=> "true",
            'Message' =>"Deleted successfully",
        ]);
    }

    public function storeProfileVideo(Request $request)
    {
       
        $video = request()->videoUrl;
        $file = $request->file('videoUrl');
    
          
        $filename =  $video->getClientOriginalName();
        $filePath = 'tutor/'.$filename;
     
        // $path = Storage::disk('s3')->put($filePath, file_get_contents($file));
        // $path_video = Storage::disk('s3')->url($path);
        // $storage_path = $video->storeAs('tutor/profile/'.$filename,'s3');  
        $storage_path = $video->storeAs('tutor','profile/'.$filename,'s3');  
    
    
        $path_video = Storage::cloud()->url($storage_path);
        return response()->json(['path_video'=>$path_video]);   
        // return response()->json(['success'=>'You have successfully upload file.']);
    
    }
        public function storePortFolioVideo(Request $request,$id)
        {
           
            $video = request()->videoUrl;
              
            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor/'.$id,'portfolio/'.$filename,'s3');  
    
            $path_video = Storage::cloud()->url($storage_path);
            return response()->json(['path_video'=>$path_video]);   
            // return response()->json(['success'=>'You have successfully upload file.']);
       
        }
        public function storePackageVideo(Request $request,$id)
        {
           
            $video = request()->videoUrl;
              
            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor/'.$id,'guruclass/'.$filename,'s3');  
    
            $path_video = Storage::cloud()->url($storage_path);
            return response()->json(['path_video'=>$path_video]);   
            // return response()->json(['success'=>'You have successfully upload file.']);
       
        }
        public function storeModuleVideo(Request $request,$id)
        {
           
            $video = request()->videoUrl;
              
            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor/'.$id,'moduleclass/'.$filename,'s3');  
    
            $path_video = Storage::cloud()->url($storage_path);
            return response()->json(['path_video'=>$path_video]);   
            // return response()->json(['success'=>'You have successfully upload file.']);
       
        }
        public function storeLiveVideo(Request $request,$id)
        {
           
            $video = request()->videoUrl;
              
            $filename =  $video->getClientOriginalName();
            $storage_path = $video->storeAs('tutor/'.$id,'liveclass/'.$filename,'s3');  
    
            $path_video = Storage::cloud()->url($storage_path);
            return response()->json(['path_video'=>$path_video]);   
            // return response()->json(['success'=>'You have successfully upload file.']);
       
        }
    public function index()
    {
        //
        $users = User::where('role','tutor')->orderBy('id', 'DESC')->get();
        return view('admin.tutor.index',\compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function create(Request $request)
    {
        $showNext = $request->showNext;
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $sub_category=array();
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $skill_subset=CategorySkills::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        // dd( $skills);
        return view('admin.tutor.create_edit',\compact('category','sub_category','countries','skills','skill_subset','showNext'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     
    public function store(Request $request)
    {
      
    $validate =  $request->validate([
          
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'category' => ['required'],
            'description' => ['required'],
            'commision' => ['required'],
            'dob' => ['required', 'date'],
            // 'sub_category' => ['required'],
//            'slug' => ['required', 'string', 'max:255', 'unique:users'],
//            'phone_number' => ['required', 'numeric', 'digits:10','unique:users'],
            'location' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'profile_image' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            'video_url' => 'required|mimes:mp4,jpeg',
            // 'video_url' => 'required|mimes:mp4,mov,ogg,qt|max:20000',
            // 'cover_letter' => 'required|mimes:csv,txt,xlx,xls,pdf|max:2048'
    ]);
   

   
        do {
            $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
        } while (User::where("token", "=", $token_key)->first() instanceof User);

        $path_image = '';
        // if($request->hasFile('profile_image')){
        //     $pro_image = $request->file('profile_image');
        //     // $path_image = $pro_image->store('/uploads/tutor',['disk' => 'public']);

        //     $filename =  $pro_image->getClientOriginalName();
        //     $storage_path = $pro_image->storeAs('tutor'.$filename,'s3');
 
        //     $path_image = Storage::cloud()->url($storage_path);
 
        //     $imagefile = $request->file('profile_image');
        //     $imageFileName= $imagefile->getClientOriginalName();
        // }
      
        $user =  User::create([
            'name' => $request->name,
            'commision' => $request->commision,
            'description'=>$request->description,
            'slug' => Str::slug($request->slug),
            'dob' => $request->dob,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'location' => $request->location,
            'token' => $token_key,
            'role' => 'tutor',
            'password' => Hash::make($request->password),
            // 'profile_image' => $path_image,
            // 'image_file_name' => $imageFileName
        ]);
        if($request->hasFile('profile_image')){
            $pro_image = $request->file('profile_image');
            // $path_image = $pro_image->store('/uploads/tutor',['disk' => 'public']);

            $filename =  $pro_image->getClientOriginalName();
            $storage_path = $pro_image->storeAs('tutor/'. $user->id,$filename,'s3');
 
            $path_image = Storage::cloud()->url($storage_path);
 
            $imagefile = $request->file('profile_image');
            $imageFileName= $imagefile->getClientOriginalName();
        }

        $user->update([
             'profile_image' => $path_image,
            'image_file_name' => $imageFileName
        ]);
        if(isset($request->college_name))
        {
            foreach ($request->college_name as $key=>$value){
                if($value){
                    $experience[$key]=[
                        'name' => $value,
                        'designation' => $request->designation[$key]
                    ];
                }
            }
        }
        $path_letter = null;
        $cover_letter_fileName = null;
        // if($request->hasFile('cover_letter')) {
        //     $cover_letter = time().'.'.$request->cover_letter->extension();  
        //     $letter_name = $request->cover_letter.$request->cover_letter->extension();
        //     // return $letter_name;
   
        // //    $path_letter= $request->cover_letter->store('/uploads/tutor/', ['disk' => 'public']);

        //    $coverLetter = $request->file('cover_letter');
        //    $filename =  $coverLetter->getClientOriginalName();
        //    $storage_path = $coverLetter->storeAs('tutor/'. $user->id,$filename,'s3');

        //    $path_letter = Storage::cloud()->url($storage_path);

        //    $coverletterfile = $request->file('cover_letter');
        //    $cover_letter_fileName= $coverletterfile->getClientOriginalName();

        // }

        if ($request->hasFile('video_url')) {
            $video = $request->file('video_url');
            $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/'.$user->id,$filename,'s3');

            // $path_video = Storage::cloud()->url($storage_path);

            $path_video = Storage::disk('s3')->url('tutor/profile/'.$filename);
            $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
            $thumbnail = "uploads/tutor/" . $filenameWithoutExtenstion . ".png";
            $sec = 10;
            $videofile = $request->file('video_url');
            $video_file_name= $videofile->getClientOriginalName();
           // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }
        if($request->skills){
        foreach ($request->skills as $key=>$value){
            $tutor_skills=TutorSkills::create([
'tutor_id'=> $user->id,
'skill_id'=>$value
            ]);
        }
    }
      
        $tutor_detail=TutorDetail::create([
            'user_id'=>$user->id,
            'category'=>2,
            'sub_category'=>3,
            'skills'=>1,
            // 'cover_letter'=>$path_letter,
            // 'cover_letter_fileName'=>$cover_letter_fileName,
            'video_url' => $path_video,
            'video_thumbnail' => $thumbnail,
            'video_file_name' => $video_file_name,
            // 'about'=>$request->about,
            // 'experience'=>json_encode($experience),
        ]);



        return redirect()->route('tutor.package.create',['id'=> $user->id,'showNext'=>'true'])->with('message', 'saved success!');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $TutorDetail=TutorDetail::where('user_id',$user->id)->first();
        $TutorSkills = TutorSkills::where('tutor_id',$id)->get();
        return view('admin.tutor.show', \compact('user','TutorDetail','TutorSkills','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $showNext = $request->showNext;
        $userId = $request->id;
        $user = User::findOrFail($userId);
        $tutorskill =TutorSkills::where('tutor_id',$userId)->pluck('skill_id');
      
        $subset=Category::all();
        $TutorDetail=TutorDetail::where('user_id',$userId)->first();
        // $tutorskill = $tutorskills->map(function ($tutorskills) {
        //     return collect($tutorskills->toArray())
        //         ->only(['skill_id'])
        //         ->all();
        // });
        // dd( $tutorskills);
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        // $category_object=Category::where('id',$user->tutor_detail->category)->first();
        // $sub_category=array();
        // if($category_object) {
        //     $sub_cat = SubCategory::where('category_id', $category_object->id)->get();
        //     $sub_category = $sub_cat->map(function ($sub_cat) {
        //         return collect($sub_cat->toArray())
        //             ->only(['id', 'sub_category_name'])
        //             ->all();
        //     });
        // }
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $skill_subset=CategorySkills::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });

        return view('admin.tutor.create_edit', \compact('user','category','countries','skills','TutorDetail','skill_subset','tutorskill','showNext'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id =$request->id;
        // dd($request->all());
        $request->validate([
            'email' => 'required|email|unique:users,email,'.$id,
//            'slug' => 'required|unique:users,slug,'.$id,
//            'phone_number' => 'required|unique:users,phone_number,'.$id,
// 'category' => ['required'],
'dob' => 'required',
            // 'sub_category' => ['required'],
            'name' => 'required',
            'description'=>'required',
            'commision'=>'required',
            'location' => 'required',
            'password' => 'sometimes|confirmed',
           
            // 'video_url' => 'required|mimes:mp4,mov,ogg,qt|max:20000',
         
        ]);
        if($request->profile_image)
        $request->validate([
            'profile_image' => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ]);

        if ($request->video_url) {
            $request->validate([
                'video_url' => 'required|mimes:mp4,jpeg',
            ]);
        }
        $user = User::find($id);
        $path_image = '';
        if($request->hasFile('profile_image')){
            if(User::where('id', $id)->exists()){
                $user = User::find($id);
                if($user->profile_image){
                    if(Storage::disk('s3')->exists($user->profile_image)){
                        Storage::disk('s3')->delete($user->profile_image);
                    }
                }
            }
            $pro_image = $request->file('profile_image');
            
            $filename =  $pro_image->getClientOriginalName();
            $storage_path = $pro_image->storeAs('tutor/'.$id,$filename,'s3');
 
            $path_image = Storage::cloud()->url($storage_path);


        }

        if($request->password)
        {
            $data = [
                'name' => $request->name,
                'commision' => $request->commision,
                'description'=>$request->description,
                'email' => $request->email,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'password' => Hash::make($request->password),
                // 'profile_image' => $path_image,
            ];
        }
        else{
            $data = [
                'name' => $request->name,
                'commision' => $request->commision,
                'description'=>$request->description,
                'email' => $request->email,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                // 'profile_image' => $path_image,
            ];
        }
        if(isset($request->college_name))
        {

        foreach ($request->college_name as $key=>$value){
            if($value){
                $experience[$key]=[
                    'name' => $value,
                    'designation' => $request->designation[$key]
                ];
            }
        }
    }
    // if($request->hasFile('cover_letter')) {
    //     if(User::where('id', $id)->exists()){
    //         $TutorDetail = TutorDetail::where('user_id',$user->id)->first();
    //         if($TutorDetail->cover_letter){
    //             if(Storage::disk('s3')->exists($TutorDetail->cover_letter)){
    //                 Storage::disk('s3')->delete($TutorDetail->cover_letter);
    //             }
    //         }
    //     }
    //     $coverletter = $request->file('cover_letter');
            
    //     $filename =  $coverletter->getClientOriginalName();
    //     $storage_path = $coverletter->storeAs('tutor/'.$request->user_id,$filename,'s3');

    //     $path_letter = Storage::cloud()->url($storage_path);


    

    // }

    if ($request->hasFile('video_url')) {
        if(User::where('id', $id)->exists()){
            $TutorDetail = TutorDetail::where('user_id',$user->id)->first();
            if($TutorDetail->video_url){
                if(Storage::disk('s3')->exists($TutorDetail->video_url)){
                    Storage::disk('s3')->delete($TutorDetail->video_url);
                }
            }
        }
     
        // $path_video = $video->store('/uploads/tutor/', ['disk' => 'public']);

        $video = $request->file('video_url');
            
        $filename =  $video->getClientOriginalName();
        // $storage_path = $video->storeAs('tutor/'.$request->user_id,$filename,'s3');

        // $path_video = Storage::cloud()->url($storage_path);
        $path_video = Storage::disk('s3')->url('tutor/'.$filename);

        $filename = str_replace("uploads/tutor/", "", $path_video);
        $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
        $thumbnail = "uploads/tutor/" . $filenameWithoutExtenstion . ".png";
        $sec = 10;

       // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

    }

        $tutor_data = [
            'category'=>2,
            'sub_category'=>1,
            // 'cover_letter'=>$path_letter,
            // 'video_url' => $path_video,
            // 'video_thumbnail' => $thumbnail,
            // 'skills'=>$request->skills,
            // 'about'=>$request->about,
            // 'experience'=>json_encode($experience),
        ];

        $user =  User::updateOrCreate(
            ['id' =>$id],
            $data
        );
        if ($request->hasFile('profile_image')) {
            $user->update([
                'profile_image' => $path_image
            ]);
        }
      
        $tutor_skills=TutorSkills::where('tutor_id',$user->id)->get();
        if (!empty($tutor_skills)) {
            $tutor_skills=TutorSkills::where('tutor_id',$user->id)->delete();
      }
     
      if($request->skills){
        foreach ($request->skills as $key=>$value){
            $tutor_skills=TutorSkills::create([
'tutor_id'=> $user->id,
'skill_id'=>$value
            ]);
        }
    }
        $tutor_detail =  TutorDetail::updateOrCreate(
            ['user_id' =>$id],
            $tutor_data
        );
        // if ($request->hasFile('cover_letter')) {
        //     $tutor_detail->update([
        //         'cover_letter'=>$path_letter
        //     ]);
        // }
        if ($request->hasFile('video_url')) {
            $tutor_detail->update([
                'video_url' => $path_video,
                'video_thumbnail' => $thumbnail
            ]);
        }

        if($request->showNext=='true'){
           
            return redirect()->route('tutor.package.create',['id'=> $id,'showNext'=>'true'])->with('message', 'saved success!');
           
        }else{
           
            return redirect()->route('tutor.show',$id)->with('message', 'saved success!');
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

       
        $user = User::find($id);

        $tutor_details = TutorDetail::where('user_id',$id)->get();

        foreach ($tutor_details as $tutor_detail) {
            $tutor_detail->delete();
        }

        // $batches = Batch::where('user_id',$id)->get();
        // foreach ($batches as $batch) {
        //     $packageBatch = PackageBatch::where('batch_id',$batch->id)->delete();
        //     $session = TutorSession::where('batch_id',$batch->id)->delete();
        //     $batch->delete();
        // }

        $packages = Package::where('user_id',$id)->get();
        // $packageDetails = PackageDetail::where('user_id',$id)->get();
        foreach ($packages as $package) {
            $packageDetails = PackageDetail::where('package_id',$package->id)->delete();
            $package->delete();
        }
        $liveClass = LiveClass::where('user_id',$id)->delete();
        $help = HelpDesk::where('user_id',$id)->delete();

        $userPortfolio = TutorPortfolio::where('user_id',$id)->delete();


        $user->delete();
        return redirect()->route('tutor.index');
    }

    /**
     * Show the form for tutor portfolio.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createPortfolio(Request $request)
    {
        $id = $request->id;
    $ShowNext = $request->showNext;
    // $course_id = \Crypt::decrypt($ShowNext);   
    // dd($course_id);
   
        $userPortfolios = TutorPortfolio::where('user_id',$id)->get();
      
        return view('admin.tutor.create_edit_portfolio', \compact('id','userPortfolios','ShowNext'));
    }

    public function storePortfolio(Request $request)
    {
       
     
        $portfolio = new TutorPortfolio();
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'description' => 'required',
             'video_url' => 'required|mimes:mp4,mov,ogg,qt',
           
        ]);

       
        if ($request->hasFile('video_url')) {
            $video = $request->file('video_url');
            
           
            // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
$filename =  $video->getClientOriginalName();
$filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
           
$thumbnail = "uploads/tutor/portfolio/" . $filenameWithoutExtenstion . ".png";
$sec = 10;
// $path_video = $video->storeAs('tutor/'.$request->user_id,'portfolio/'.$filename,'s3');
// $storage_path = Storage::cloud()->url($path_video);
$storage_path = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'portfolio/'.$filename);


     
        }

        // if ($request->video_thumbnail) {
        //     $video = $request->file('video_thumbnail');
        //     $path_video_thumbnail = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);
        // }

        $userPortfolio =  TutorPortfolio::create([
            'user_id' => $request->user_id,
            'title' => $request->title,
            'description' => $request->description,
            'video_url' => $storage_path,
            'video_thumbnail' => $thumbnail,
        ]);
        if($request->showNext=='true'){
           
            return redirect()->route('tutor.package.create',['id'=> $request->user_id,'showNext'=>'true'] )->with('message', 'saved success!');
           
        }else{
           
            return redirect()->back()->with('message', 'saved success!');
        }
       


    }

public function editPortfolio(Request $request,$id){
   
    $userPortfolio =  TutorPortfolio::find($id);
    return view('admin.tutor.edit_portfolio', \compact('userPortfolio'));
}

    public function updatePortfolio(Request $request,$id)
    {
        
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            // 'video_url' => 'sometimes|mimes:mp4,mov,ogg,qt|max:20000',
            // 'video_thumbnail' => 'sometimes|mimes:jpeg,bmp,png',
        ]);
        if ($request->hasFile('video_url')) {
            if(User::where('id', $id)->exists()){
                $TutorPortfolio = TutorPortfolio::where('user_id',$id)->first();
                if($TutorPortfolio->video_url){
                    if(Storage::disk('s3')->exists($TutorPortfolio->video_url)){
                        Storage::disk('s3')->delete($TutorPortfolio->video_url);
                    }
                }
            }
           
            $video = $request->file('video_url');
            $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/'.$request->user_id,'portfolio/'.$filename,'s3');

            // $path_video = Storage::cloud()->url($storage_path);
            $path_video = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'portfolio/'.$filename);
            
           
$filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
$thumbnail = "uploads/tutor/portfolio/" . $filenameWithoutExtenstion . ".png";

        }

       
        $userPortfolio =  TutorPortfolio::updateOrCreate(   
            ['id' => $request->id],
        [
            'title' => $request->title,
            'description' => $request->description,
            
        ]);
        if(($request->hasFile('video_url'))){
            $userPortfolio =  TutorPortfolio::updateOrCreate( 
                ['id' => $request->id],[ 
'video_url' => ($request->video_url) ? $path_video : $request->temp_video_url,
            'video_thumbnail' =>  $thumbnail
            ]);
        }
       

        // if($request->showNext=='true'){
           
        //     return redirect()->route('tutor.package.create',['id'=> $userPortfolio->user_id,'showNext'=>'true'] )->with('message', 'saved success!');
           
        // }else{
           
            return redirect()->route('tutor.portfolio.create',['id'=> $userPortfolio->user_id,'showNext'=>'false'] );
        // }
    }

    public function deletePortfolio($id)
    {
        $userPortfolio = TutorPortfolio::find($id);
        $userPortfolio->delete();
        return redirect()->back();
    }

    /**
     * Show the form for tutor package.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showPackage($id)
    {
     $packageDetails = PackageDetail::where([
         ['package_id',$id]
         ])->get();
         $package = Package::where([
            ['id',$id]
            ])->first();
        return view('admin.tutor.show_guru', \compact('packageDetails','package'));
    }
    public function viewPackage($id){
        $userPackages = Package::where([
            ['user_id',$id],['batch_id',1]
            ])->get();
        $packageDetails = PackageDetail::where([
            ['user_id',$id],['batch_id',1]
            ])->get();  
            return view('admin.tutor.package_index', \compact('id','userPackages','packageDetails'));
    }
    public function createPackage(Request $request)
    {
        // dd('hyy');
        $id = $request->id;
        $ShowNext = $request->showNext;
        $userPackages = Package::where('user_id',$id)->get();
        $packageDetails = PackageDetail::where([
            ['user_id',$id],['batch_id',1]
            ])->get();
        $userBatches = Batch::where('user_id',$id)->get();
        $userClasses = DB::table('classes')->whereIn('class_id',[1])->get();
        return view('admin.tutor.create_edit_package', \compact('id','userPackages','userBatches','userClasses','packageDetails','ShowNext'));
    }

    public function storePackage(Request $request)
    {
      
     
        $validate =  $request->validate([
            // 'user_id' => 'required',
             'title' => 'required',
            'title.*' => 'required',
            // 'video_url' => 'required|mimes:mp4,mov,ogg,qt|max:20000',
            // 'video_url.*' => 'required|mimes:mp4,mov,ogg,qt|max:20000',
           // 'thumbnail' => 'required',
            'total_price'=>'required'
        ]);

       $title = $request->title;
       $description = $request->description;
       if(count($request->video_url)>0)
       {
         foreach($request->video_url as $key=>$value)
         { 
            $filename[$key] =  $value->getClientOriginalName();
            $path_video[$key] = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'guruclass/'.$filename[$key]);
            // $storage_path[$key] = $value->storeAs('tutor/'.$request->user_id,'moduleClass/'.$filename[$key],'s3');
$thumbpath =Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'guruclass/');
            // $path_video[$key] = Storage::cloud()->url($storage_path[$key]);
          
     $filenameWithoutExtenstion[$key] = pathinfo($filename[$key], PATHINFO_FILENAME);
    $thumbnail[$key] = $thumbpath.$filenameWithoutExtenstion[$key] . ".png";
    $sec = 10;
    // $videofile = $request->file('video_url');
    // $video_file_name= $videofile->getClientOriginalName();
            //    $result = $this->getImageFromVideo($sec, $path_video[$key], $thumbnail[$key]);
         }
        }

        if ($request->hasFile('intro_video')) {
            $video = $request->file('intro_video');
            $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/'.$request->user_id,'moduleClass/'.$filename,'s3');

            // $path_video = Storage::cloud()->url($storage_path);
            $path_intro_video = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'guruclass/'.$filename);
            $thumbpathTrailer =Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'guruclass/');
            $filename = str_replace("uploads/tutor", "", $path_intro_video);
             $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
           
            $thumbnail =   $thumbpathTrailer . $filenameWithoutExtenstion . ".png";
            $sec = 10;
            // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }
        $userPackage =  Package::create([
            'user_id' => $request->user_id,
            'batch_id' => $request->batch_id,
            'intro_title'=>$request->intro_title,
            'intro_description'=>$request->intro_description,
            'video_url' => $path_intro_video,
            'video_thumbnail' => $thumbnail,
            'total_price'=>$request->total_price,
        ]);
       
       for($i=0;$i<count($title);$i++){
      
        $userPackageDetail=PackageDetail::create([
            'user_id' => $request->user_id,
            'batch_id' => $request->batch_id,
            'package_id'=>$userPackage->id,
            'title' => $title[$i],
            'description' => $description[$i],
            'video_url' => $path_video[$i],
          'thumbnail'=>$thumbnail[$i],
          'price'=>0
        //   'video_file_name'=>$video_file_name
        ]);
    
       }
       

       if($request->showNext=='true'){
        "<script>
        localStorage.setItem('id',{$userPackage->id});
        localStorage.setItem('user_id',{$userPackage->user_id});
        localStorage.setItem('intro_title', {$userPackage->request_id});
        localStorage.setItem('intro_description', {$userPackage->intro_description});
        localStorage.setItem('total_price', {$userPackage->total_price});
 
        </script>";
        return redirect()->route('tutor.module.create',['id'=>  $request->user_id,'showNext'=>'true'] )->with('message', 'saved success!');
        // return redirect()->back()->with('res', 'Success');
    }else{
       
        return redirect()->route('tutor.package.view', $request->user_id)->with('message', 'Saved Success!');
    }
}
    

    public function editPackage(Request $request, $id)
    {
      
        // $packageDetail = PackageDetail::find($id);
        // $package = Package::where('id',$packageDetail->package_id)->first();
        $package = Package::find($id);
       
        $packageDetails = PackageDetail::where('package_id',$package->id)->get();

        $userClasses = DB::table('classes')->whereIn('class_id',[1])->get();
        return view('admin.tutor.edit_class', \compact('packageDetails','userClasses','package'));

    }
    public function updatePackage(Request $request)
    {
    //    dd($request->all());
        $id =$request->id;
        $request->validate([
            'title' => 'required',
            'total_price'=>'required'
            // 'subscription_fee' => 'required',
        ]);
      
        $package = Package::find($id);
       
        $packageDetails = PackageDetail::where('package_id',$package->id)->get();
        // dd($packageDetails);
$title = $request->title;
$description = $request->description;
if($request->hasFile('video_url')){
    $video_url = $request->video_url;

}
if($request->hasFile('video_url'))
       {
         foreach($request->video_url as $key=>$value)
         {    
           
            $filename[$key] =  $value->getClientOriginalName();
            // $storage_path[$key] = $value->storeAs('tutor/'.$request->user_id,'portfolio/'.$filename[$key],'s3');

            // $path_video[$key] = Storage::cloud()->url($storage_path[$key]);
            $path_video[$key] = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'guruclass/'.$filename[$key]);
    // $filenameWithoutExtenstion[$key] = pathinfo($filename[$key], PATHINFO_FILENAME);
    $thumbnail[$key] = "uploads/tutor/package" . $path_video[$key] . ".png";
    $sec = 10;
    // $videofile = $request->file('video_url');
    // $video_file_name= $videofile->getClientOriginalName();
              // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
         }
        }
      
        if ($request->hasFile('intro_video')) {
            if(Storage::disk('s3')->exists($package->video_url)){
                Storage::disk('s3')->delete($package->video_url);
            }
            $video = $request->file('intro_video');
            $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/moduleclass/'.$request->user_id,$filename,'s3');

            // $path_video = Storage::cloud()->url($storage_path);
            $path_intro_video = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'guruclass/'.$filename);

            // $filename = str_replace("uploads/tutor", "", $path_video);
            // $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
           
            $thumbnail = "uploads/tutor" . $path_intro_video . ".png";
            $sec = 10;
            // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }
        $package->update([
'total_price'=>$request->total_price,
'intro_title'=>$request->intro_title,
'intro_description'=>$request->intro_description,
        ]);
        if(($request->hasFile('intro_video'))){
            $package->update([
               
                'video_url' => $path_intro_video,
          'thumbnail'=>$thumbnail
            ]);
        }
        // if(($request->hasFile('video_url'))){
        //     $totalcount =count($title)+count($video_url);
        // }
        // else{
            $totalcount =count($title);
        // }
        // dd(count($title));
        for($i=0;$i< $packageDetails[$i];$i++){
            if(isset( $packageDetails[$i])){
                if(isset($title[$i])){
                    $packageDetails[$i]->update([
                        'user_id' => $package->user_id,
                        'batch_id' => $request->batch_id,
                        'package_id'=>$package->id,
                        'title' => $title[$i],
                        'description' => $description[$i],
            
                    ]);
                    if(($request->hasFile('video_url'))){
               
                        if(isset($path_video[$i])){
                            $packageDetails[$i]->update([
                                'video_url' => $path_video[$i],
                          'thumbnail'=>$thumbnail[$i]
                            ]);
                        }
                        
                     
                    }
                }
                else{
                    $totalcount += 1;
                    $packageDetails[$i]->delete();
                }
            
           
        }
        else{
            PackageDetail::create([
                'user_id' =>$package->user_id,
                'batch_id' => $request->batch_id,
                'package_id'=>$package->id,
                'title' => $title[$i],
                'description' => $description[$i],
                'video_url' => $path_video[$i],
              'thumbnail'=>$thumbnail[$i],
             
            //   'video_file_name'=>$video_file_name
            ]);
        
        }
            
           }
       


       

        // if($request->showNext=='true'){
           
        //     return redirect()->route('tutor.module.create',['id'=> $package->user_id,'showNext'=>'true'] )->with('message', 'saved success!');
           
        // }else{
           
            return redirect()->route('tutor.package.view', $package->user_id)->with('message', 'Saved Success!');
        // }
    }

    public function deletePackage($id)
    {
        //$packageBatch = PackageBatch::where('package_id',$id)->delete();
        $userPackages = Package::find($id);
    
        $packageDetail = PackageDetail::where('package_id',$id)->delete();
        $userPackages->delete();
       
        return redirect()->back();
    }
   
   //module class
   public function showModule($id)
   {
    $packageDetails = PackageDetail::where([
        ['package_id',$id]
        ])->get();
        $package = Package::where([
            ['id',$id]
            ])->first();
       return view('admin.tutor.show_module', \compact('packageDetails','package'));
   }

   public function viewModule($id){
  
    $userPackages = Package::where([
        ['user_id',$id],['batch_id',2]
        ])->get();
    $packageDetails = PackageDetail::where([
        ['user_id',$id],['batch_id',2]
        ])->get();  
        return view('admin.tutor.module_index', \compact('id','userPackages','packageDetails'));
}
   public function createModule(Request $request)
    {
        $id = $request->id;
        $ShowNext = $request->showNext;
        $userPackages = Package::where([
            ['user_id',$id],['batch_id',2]
            ])->get();
            // dd($userPackages);
        $packageDetails = PackageDetail::where([
            ['user_id',$id],['batch_id',2]
            ])->get();
          
        $userBatches = Batch::where('user_id',$id)->get();
        $userClasses = DB::table('classes')->whereIn('class_id',[2])->get();
        return view('admin.tutor.create_edit_module', \compact('id','userPackages','userBatches','userClasses','packageDetails','ShowNext'));
    }


    public function storeModule(Request $request)
    {
      
        // $data= $request->all();
       // dd($data);
      //$batchID =$request->batch_id;
    
      $validate =  $request->validate([
        // 'user_id' => 'required',
         'title' => 'required',
        'title.*' => 'required',
        // 'video_url' => 'required|mimes:mp4,jpeg',
        // 'intro_video' => 'required|mimes:mp4,jpeg',
       // 'thumbnail' => 'required',
        'total_price'=>'required'
    ]);

       $title = $request->title;
       $description = $request->description;
       $price = $request->total_price;
       if($request->hasFile('video_url'))
       {
         foreach($request->video_url as $key=>$value)
         { 
            $filename[$key] =  $value->getClientOriginalName();
            $path_video[$key] = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'moduleclass/'.$filename[$key]);
            // $storage_path[$key] = $value->storeAs('tutor/'.$request->user_id,'moduleClass/'.$filename[$key],'s3');

            // $path_video[$key] = Storage::cloud()->url($storage_path[$key]);
          
    // $filenameWithoutExtenstion[$key] = pathinfo($filename[$key], PATHINFO_FILENAME);
    $thumbnail[$key] = "uploads/tutor/package" . $path_video[$key] . ".png";
    $sec = 10;
    // $videofile = $request->file('video_url');
    // $video_file_name= $videofile->getClientOriginalName();
              // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
         }
        }
      
    //     if($request->hasFile('multi_trailer'))
    //     {
    //       foreach($request->multi_trailer as $key=>$value)
    //       { 
    //          $filename_trailer[$key] =  $value->getClientOriginalName();
    //          $path_trailer_video[$key] = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'moduleclass/'.$filename_trailer[$key]);
    //          // $storage_path[$key] = $value->storeAs('tutor/'.$request->user_id,'moduleClass/'.$filename[$key],'s3');
 
    //          // $path_video[$key] = Storage::cloud()->url($storage_path[$key]);
           
    //  // $filenameWithoutExtenstion[$key] = pathinfo($filename[$key], PATHINFO_FILENAME);
    //  $thumbnail_trailer[$key] = "uploads/tutor/module" . $path_trailer_video[$key] . ".png";
    //  $sec = 10;
    //  // $videofile = $request->file('video_url');
    //  // $video_file_name= $videofile->getClientOriginalName();
    //            // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
    //       }
    //      }

        if ($request->hasFile('intro_video')) {
            $video = $request->file('intro_video');
            $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/'.$request->user_id,'moduleClass/'.$filename,'s3');

            // $path_video = Storage::cloud()->url($storage_path);
            $path_intro_video = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'moduleclass/'.$filename);

            $filename = str_replace("uploads/tutor", "", $path_intro_video);
             $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
           
            $thumbnail = "uploads/tutor" . $filenameWithoutExtenstion . ".png";
            $sec = 10;
            // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }
        $userPackage =  Package::create([
            'user_id' => $request->user_id,
            'batch_id' => $request->batch_id,
            'intro_title'=>$request->intro_title,
            'intro_description'=>$request->intro_description,
            'video_url'=>  $path_intro_video,
            'thumbnail'=>  $thumbnail,
            
            'total_price'=> $price,
        ]);
       
       for($i=0;$i<count($title);$i++){
      
        $userPackageDetail=PackageDetail::create([
            'user_id' => $request->user_id,
            'batch_id' => $request->batch_id,
            'package_id'=>$userPackage->id,
            'title' => $title[$i],
            'description' => $description[$i],
            // 'price' => $price[$i],
            'video_url' => $path_video[$i],
          'thumbnail'=>$thumbnail[$i],
        //   'trailer' => $path_trailer_video[$i],
        //   'thumbnail_trailer'=>$thumbnail_trailer[$i]

        //   'video_file_name'=>$video_file_name
        ]);
    
       }
   
    if($request->showNext=='true'){
        return redirect()->route('tutor.live_class.create',['id'=>  $request->user_id,'showNext'=>'true'] )->with('message', 'saved success!');
        // return redirect()->back()->with('res', 'Success');
    }else{
       
        return redirect()->route('tutor.module.view', $request->user_id)->with('message', 'saved success!');
    }
      

    }
    public function editModule(Request $request, $id)
    {
       
        $package = Package::find($id);
       
        $packageDetails = PackageDetail::where('package_id',$package->id)->get();
       
      
        $userClasses = DB::table('classes')->whereIn('class_id',[2])->get();
        return view('admin.tutor.edit_module_class', \compact('packageDetails','userClasses','package'));

    }
    public function updateModule(Request $request)
    {
        $id =$request->id;
        $request->validate([
            'title' => 'required',
            'total_price'=>'required'
            // 'subscription_fee' => 'required',
        ]);
        $package = Package::find($id);
       
        $packageDetails = PackageDetail::where('package_id',$package->id)->get();
$title = $request->title;
$description = $request->description;
$price = $request->total_price;
// if($request->hasFile('multi_trailer'))
//        {
//          foreach($request->multi_trailer as $key=>$value)
//          { 
           
//             $filename_trailer[$key] =  $value->getClientOriginalName();
//             // $storage_path[$key] = $value->storeAs('tutor/'.$request->user_id,'portfolio/'.$filename[$key],'s3');

//             // $path_video[$key] = Storage::cloud()->url($storage_path[$key]);
//             $path_video_trailer[$key] = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'moduleclass/'.$filename_trailer[$key]);
//     // $filenameWithoutExtenstion[$key] = pathinfo($filename[$key], PATHINFO_FILENAME);
//     $thumbnail_trailer[$key] = "uploads/tutor/module" . $path_video_trailer[$key] . ".png";
//     $sec = 10;
//     // $videofile = $request->file('video_url');
//     // $video_file_name= $videofile->getClientOriginalName();
//               // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
//          }
//         }
if($request->hasFile('video_url'))
       {
         foreach($request->video_url as $key=>$value)
         { 
           
            $filename[$key] =  $value->getClientOriginalName();
            // $storage_path[$key] = $value->storeAs('tutor/'.$request->user_id,'portfolio/'.$filename[$key],'s3');

            // $path_video[$key] = Storage::cloud()->url($storage_path[$key]);
            $path_video[$key] = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'moduleclass/'.$filename[$key]);
    // $filenameWithoutExtenstion[$key] = pathinfo($filename[$key], PATHINFO_FILENAME);
    $thumbnail[$key] = "uploads/tutor/module" . $path_video[$key] . ".png";
    $sec = 10;
    // $videofile = $request->file('video_url');
    // $video_file_name= $videofile->getClientOriginalName();
              // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
         }
        }
        if ($request->hasFile('intro_video')) {
            if(Storage::disk('s3')->exists($package->video_url)){
                Storage::disk('s3')->delete($package->video_url);
            }
            $video = $request->file('intro_video');
            $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/moduleclass/'.$request->user_id,$filename,'s3');

            // $path_video = Storage::cloud()->url($storage_path);
            $path_intro_video = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'moduleclass/'.$filename);

            // $filename = str_replace("uploads/tutor", "", $path_video);
            // $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
           
            $thumbnail = "uploads/tutor" . $path_intro_video . ".png";
            $sec = 10;
            // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }
        $package->update([
'total_price'=> $price,
'intro_title'=>$request->intro_title,
'intro_description'=>$request->intro_description,
        ]);
        if(($request->hasFile('intro_video'))){
            $package->update([
               
                'video_url' => $path_intro_video,
          'thumbnail'=>$thumbnail
            ]);
        }
        for($i=0;$i<count($title);$i++){
      
            $packageDetails[$i]->update([
                'user_id' => $package->user_id,
                'batch_id' => $request->batch_id,
                'package_id'=>$package->id,
                'title' => $title[$i],
                'description' => $description[$i],
                // 'price' => $price[$i],
         
            ]);
            if(($request->hasFile('video_url'))){
                if(isset($path_video[$i])){
                $packageDetails[$i]->update([
                    'video_url' => $path_video[$i],
              'thumbnail'=>$thumbnail[$i]
               
            ]);
        }
            }
        //     if(($request->hasFile('multi_trailer'))){
        //         if(isset($path_video_trailer[$i])){
        //         $packageDetails[$i]->update([
        //             'trailer' => $path_video_trailer[$i],
        //       'thumbnail_trailer'=>$thumbnail_trailer[$i]
        //         ]);
        //    }
        // }
        }
       


        
        // if($request->showNext=='true'){
           
        //     return redirect()->route('tutor.live_class.create',['id'=> $package->user_id,'showNext'=>'true'] )->with('message', 'saved success!');
           
        // }else{
           
            return redirect()->route('tutor.module.view', $package->user_id)->with('message', 'saved success!');
        // }

    }

    public function deleteModule($id)
    {
       
        //$packageBatch = PackageBatch::where('package_id',$id)->delete();
        $userPackages = Package::find($id);
    
        $packageDetail = PackageDetail::where('package_id',$id)->delete();
        $userPackages->delete();
        // $userPackages = Package::where('id',$packageDetail->package_id)->first();
        //$userPackages->delete();
        // $packageDetail->delete();
        return redirect()->back();
    }
    /**
     * Show the form for tutor batch.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function viewLiveClass($id)
     {
         
         $LiveClassDetails = LiveClass::where('user_id',$id)->get();
         $userClasses = DB::table('classes')->where('class_id','3')->get();
         return view('admin.tutor.live_class_index', \compact('id','LiveClassDetails','userClasses'));
     }

     public function showLiveClass($id)
     {
      $LiveClass = LiveClass::where([
          ['id',$id]
          ])->first();
         return view('admin.tutor.show_live_class', \compact('LiveClass'));
     }
     public function createLiveClass(Request $request)
    {
        // $userBatches = Batch::where('user_id',$id)->get();
        // $userPackages = Package::where('user_id',$id)->get();
        $id = $request->id;
        $ShowNext = $request->showNext;
        $LiveClassDetails = LiveClass::where('user_id',$id)->get();
        $userClasses = DB::table('classes')->where('class_id','3')->get();
        return view('admin.tutor.create_edit_live_class', \compact('id','LiveClassDetails','userClasses','ShowNext'));
    }

    public function storeLiveClass(Request $request)
    {
       
        $request->validate([
            'batch_id'=>'required',
            'title' => 'required',
            'description' => '',
            'start_date' => 'required',
            'end_date' => 'required',
            'video_url' => 'required',
            'price' => 'required',
            'no_of_participents'=>'required_if:class_type,==,2'
        ]);

        try
        {
            if ($request->hasFile('video_url')) {
                $video = $request->file('video_url');
                $filename =  $video->getClientOriginalName();
            // $storage_path = $video->storeAs('tutor/'.$request->user_id,'liveClass/'.$filename,'s3');

            // $path_video = Storage::cloud()->url($storage_path);
            $path_video = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'liveclass/'.$filename);
   
                // $filename = str_replace("uploads/tutor", "", $path_video);
                $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
                $thumbnail = "uploads/tutor" . $filenameWithoutExtenstion . ".png";
                $sec = 10;
                $videofile = $request->file('video_url');
                $video_file_name= $videofile->getClientOriginalName();
               // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
    
            }
                $liveClass = LiveClass::create([
                    'user_id' => $request->user_id,
                    'batch_id' => $request->batch_id,
                    'intro_title'=>$request->intro_title,
                    'intro_description'=>$request->intro_description,
                    'title' => $request->title,
                    'description' => $request->description,
                    'no_of_participents'=> $request->no_of_participents,
                    'video_url' => $path_video,
                    'video_file_name' => $video_file_name,
                  'thumbnail'=>$thumbnail,
                    'class_type'=>$request->class_type,
                    'start_date'=>$request->start_date,
                    'end_date'=>$request->end_date,
                    'start_time'=>$request->start_time,
                    'end_time'=>$request->end_time,
                    'duration'=>$request->duration,
                    'meeting_link'=>$request->meeting_link,
                    'no_of_class'=>$request->no_of_class,
                    'price'=>$request->price
                ]);
          

        // if($request->package_id)
        // {
        //     $packageBatch = new PackageBatch();
        //     $packageBatch->package_id = $request->package_id;
        //     $packageBatch->batch_id = $userBatch->id;
        //     $packageBatch->save();
        // }
        

      
        if($request->showNext=='true'){

            return redirect()->route('tutor.index')->with('message', 'saved success!');;
            // return redirect()->back()->with('res', 'Success');
        }else{
           
            return redirect()->route('tutor.live_class.view',$request->user_id)->with('message', 'saved success!');
        }
        
        // return redirect()->back();
} catch (\Illuminate\Database\QueryException $e) {
    $response['errors'] = 'error deleting batch data';
    return response()->json($response, 403);
} catch (\Exception $e) {
    $response['errors'] = 'error getting batch data';
    return response()->json($response, 403);
}



    }
public function editLiveClass(Request $request,$id){
    $packageDetail = LiveClass::find($id);
        $userClasses = DB::table('classes')->whereIn('class_id',[3])->get();
        return view('admin.tutor.edit_live_class', \compact('packageDetail','userClasses'));

}
    public function updateLiveClass(Request $request,$id)
    {
        $request->validate([
            'batch_id'=>'required',
            'title' => 'required',
            'description' => '',
            'start_date' => 'required',
            'end_date' => 'required',
           
            'price' => 'required',
            'no_of_participents'=>'required_if:class_type,==,2'
        ]);
        if($request->video_url){
            $request->validate([
                'video_url' => 'required|mimes:mp4,mkv,webm',
            ]);
        }
        $liveClass = LiveClass::find($id);
      
    //    return $request->file('video_url');
        try
        {
            if ($request->hasFile('video_url')) {
                if(Storage::disk('s3')->exists($liveClass->video_url)){
                    Storage::disk('s3')->delete($liveClass->video_url);
                }
                $video = $request->file('video_url');
                $filename =  $video->getClientOriginalName();
                // $storage_path = $video->storeAs('tutor/'.$request->user_id,'liveClass/'.$filename,'s3');
    
                // $path_video = Storage::cloud()->url($storage_path)
                $path_video = Storage::disk('s3')->url('tutor/'.$request->user_id.'/'.'liveclass/'.$filename);
                
                $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);
                $thumbnail = "uploads/tutor/" . $filenameWithoutExtenstion . ".png";
                $sec = 10;
    
               // $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);
    
            }
                $liveClass->update([
                    // 'user_id' => $request->user_id,
                    // 'batch_id' => $request->batch_id,
                    'intro_title'=>$request->intro_title,
                    'intro_description'=>$request->intro_description,
                    'title' => $request->title,
                    'description' => $request->description,
                //     'video_url' => $path_video,
                //   'thumbnail'=>$thumbnail,
                    'class_type'=>$request->class_type,
                    'start_date'=>$request->start_date,
                    'end_date'=>$request->end_date,
                    'start_time'=>$request->start_time,
                    'end_time'=>$request->end_time,
                    'duration'=>$request->duration,
                    'meeting_link'=>$request->meeting_link,
                    'no_of_participents'=> $request->no_of_participents,
                    'no_of_class'=>$request->no_of_class,
                    'price'=>$request->price
                ]);
                if(($request->hasFile('video_url'))){
                    $liveClass->update([
                        'video_url' => $path_video,
                        'thumbnail'=>$thumbnail
                    ]);
                }
            }
            catch (\Illuminate\Database\QueryException $e) {
                $response['errors'] = 'error deleting batch data';
                return response()->json($response, 403);
            } catch (\Exception $e) {
                $response['errors'] = 'error getting batch data';
                return response()->json($response, 403);
            }
            return redirect()->route('tutor.live_class.view',$liveClass->user_id)->with('message', 'saved success!');;
    
}

    public function deleteBatch($id)
    {
    //   $packageBatch = PackageBatch::where('batch_id',$id)->delete();
    //     $sessions = TutorSession::where('batch_id',$id)->delete();
    //     $userPackages = Batch::find($id);
    //     $userPackages->delete();
    $packageLiveClass = LiveClass::where('id',$id)->delete();
        return redirect()->back();
    }
}
