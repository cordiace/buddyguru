<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use App\TutorSession;
use App\HelpDesk;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Package;
use App\PackageDetail;
use App\LiveClass;



class ClassesController extends Controller
{
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowGuruClass(Request $request)
    {
        
        $userPackages = Package::where('user_id',$request->user_id)->get();
        $packageDetails = PackageDetail::where([
            ['user_id',$request->user_id],['batch_id',1]
            ])->get();
            return response([
                'status'=> "true",
                
                'data' => $packageDetails,
            ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ShowModuleClass(Request $request)
    {
        $userPackages = Package::where('user_id',$request->user_id)->get();
        $packageDetails = PackageDetail::where([
            ['user_id',$request->user_id],['batch_id',2]
            ])->get();
            return response([
                'status'=> "true",
                
                'data' => $packageDetails,
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ShowLiveClass(Request $request)
    {
        $LiveClassDetails = LiveClass::where([
            ['user_id',$request->user_id],['batch_id',3]
            ])->get();
            return response([
                'status'=> "true",
                
                'data' => $LiveClassDetails,
            ], 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    

}
