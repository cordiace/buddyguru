<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TutorPortfolio extends Model
{
    protected $table = "tutor_portfolio";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title','description','video_url','video_thumbnail'
    ];

    /**
     * Get the user that owns the TutorPortfolio
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_Id', 'id');
    }




}
