<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Sessions extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','type', 'grade','subject', 'skill','session_name','session_date', 'session_time','duration', 'amount','description','learn','session_link','session_email_status'
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\User');
    }

    public function payment()
    {
        return $this->hasMany(\App\Payment::class);
    }
    public function session()
    {
        return $this->hasMany(\App\Payment::class);
    }
}
