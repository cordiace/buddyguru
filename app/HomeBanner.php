<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class HomeBanner extends Authenticatable
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date','title', 'description','image','learn_more_link','watch_now_link','page'
    ];

    //Add extra attribute
    // protected $attributes = ['image_url'];

    //Make it available in the json response
    protected $appends = ['image_url'];

    //implement the attribute
    public function getImageUrlAttribute()
    {
      $file = asset('storage/admin-banner/'.$this->image);
      return $file;
    }
}
