<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class CurrencyRangeTier extends Model
{
    use Notifiable;

    public $timestamps = false;
    public $table = "currency_range_tier";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tier_id','class_id','price_range','update_date'
    ];


    public function classes()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Classes','class_id', 'class_id');
    }
    public function tier()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Tier','id', 'tier_id');
    }
}
