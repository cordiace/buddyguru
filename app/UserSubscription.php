<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class UserSubscription extends Model
{
    use Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','batch_id','package_id','stripe_subscription_id','stripe_customer_id','sub_schedule_id','created','plan_period_start','plan_period_end','status'
    ];

}
