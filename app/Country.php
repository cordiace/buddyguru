<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Country extends Model
{
    use Notifiable;
    protected $fillable = [
        'region_id','country_name'
    ];

    public function region()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Region','id', 'region_id');
    }
}
