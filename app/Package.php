<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Package extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','batch_id', 'intro_title','intro_description','video_url','thumbanail','total_price',
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\User');
    }
    public function PackageDetail()
    {
        return $this->hasMany('App\PackageDetail','package_id','id');
    }

    public function batches()
    {
        return $this->belongsToMany('App\Batch');
    }

}
