<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    use Notifiable;
    protected $fillable = [
        'category_name'
    ];

    public function subcategories()
    {
        return $this->hasMany(\App\SubCategory::class);
    }
    public function allsubcategories()
    {
        return $this->hasMany(\App\SubCategory::class,'category_id', 'id');
    }
}
