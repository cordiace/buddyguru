<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Payment;

class DailyCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      \Log::info("Cron is working fine!");

      $payments = Payment::where('payment_method','monthly')->get();

      foreach ($payments as $payment) {
        $next_due_date = date('d-m-Y', strtotime($payment->created_at. ' +30 days'));

        $today = date('d-m-Y');
        if($next_due_date < $today)
        {
          $payment->payment_status = 'expired';
          $payment->save();
        }
      }

      $this->info('Daily:Cron Command Run successfully!');
    }
}
