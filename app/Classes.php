<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Package;
use App\Payment;


class Classes extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'class_id','class_name','description',
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\User');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Package');
    }

    /**
     * Function used to get payments
     *
     * @var array
     */
    public function payments()
    {
        return Payment::where(['tutor_id' => $this->user_id,'batch_id' => $this->id, 'payment_status' => 1])->get();
        // return $this->belongsTo('App\Payment');
    }

    public function subscribers()
    {
        return $this->hasMany('App\Payment');
    }

    public function sessions()
    {
        return $this->hasMany('App\TutorSession');
    }

    public function upcomingSession()
    {
      return TutorSession::where('batch_id', $this->id)->where('date','>=',date('Y-m-d'))->get();
    }

}
