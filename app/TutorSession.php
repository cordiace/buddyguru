<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class TutorSession extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','category_id','grade_id','batch_id','session_name', 'description','date','time', 'meeting_link','start_url','meeting_id','what_learn',
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\User');
    }

    //SessionDetail.php
    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }



    //Add extra attribute
    // protected $attributes = ['what_we_learn'];

    // //Make it available in the json response
    // protected $appends = ['what_we_learn'];

    // //implement the attribute
    // public function getWhatWeLearnAttribute()
    // {
    //   return unserialize($this->what_learn);
    // }

}
