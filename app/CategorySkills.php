<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class CategorySkills extends Model
{
    use Notifiable;

    public $timestamps = false;
public $table = 'category_skills';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id','skill_name'
    ];

    public function category()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Category','id', 'category_id');
    }
}
