<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class PackageBatch extends Model
{
    use Notifiable;

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'batch_package';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'package_id','batch_id',
    ];

    //SessionDetail.php
    public function package()
    {
        return $this->belongsTo('App\Package');
    }
    //SessionDetail.php
    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

}
