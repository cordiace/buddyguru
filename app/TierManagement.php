<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class TierManagement extends Model
{
    use Notifiable;

    public $table = "tier_management";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tier_id','country_name','currency_type_id'
    ];
    public function currencySetting()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\CurrencySettings','id', 'currency_type_id');
    }
    public function tier()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Tier','id', 'tier_id');
    }
}
