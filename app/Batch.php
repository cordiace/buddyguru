<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Package;
use App\PackageDetail;
use App\Payment;


class Batch extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','package_id', 'batch_name','description', 'start','end','duration', 'no_of_participants','no_of_sessions', 'payment_type','subscription_fee',
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\User');
    }

    // public function packages()
    // {
    //     return $this->belongsToMany('App\Package');
    // }
    public function packageDetail()
    {
        return $this->belongsToMany('App\PackageDetail');
    }
   
    /**
     * Function used to get payments
     *
     * @var array
     */
    public function payments()
    {
        return Payment::where(['tutor_id' => $this->user_id,'batch_id' => $this->id, 'payment_status' => 1])->get();
        // return $this->belongsTo('App\Payment');
    }

    public function subscribers()
    {
        return $this->hasMany('App\Payment');
    }

    public function sessions()
    {
        return $this->hasMany('App\TutorSession');
    }

    public function upcomingSession()
    {
      return TutorSession::where('batch_id', $this->id)->where('date','>=',date('Y-m-d'))->get();
    }

}
