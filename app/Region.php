<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Region extends Model
{
    use Notifiable;
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];
    protected $table ='regions';

   
}
