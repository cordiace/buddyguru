<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class LiveClass extends Model
{
    use Notifiable;
    public $table = "live_class";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'user_id','price','batch_id','intro_title','intro_description', 'title','description','video_url','video_file_name','thumbnail','class_type','start_date','end_date',
        'start_time','end_time','duration','meeting_link','no_of_class','no_of_participents'
    ];

    //SessionDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //SessionDetail.php
    public function tutor()
    {
        return $this->belongsTo('App\User');
    }

    public function classes()
    {
        // return $this->belongsTo('App\Batch');
        return $this->hasOne('App\Classes','class_id', 'batch_id');
    }

}
