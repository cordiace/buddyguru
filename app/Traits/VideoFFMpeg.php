<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use FFMpeg\FFMpeg;
use FFMpeg\Coordinate\TimeCode;

trait VideoFFMpeg
{
    public function getImageFromVideo($sec , $video, $thumbnail)
    {
        $ffmpeg = FFMpeg::create([
            'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
            'ffprobe.binaries' => '/usr/bin/ffprobe'
        ]);
        $video = $ffmpeg->open($video);
        $frame = $video->frame(TimeCode::fromSeconds($sec));
        $frame->save($thumbnail);

    }

}
