<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CmsField extends Model
{
    use Notifiable;
    protected $fillable = [
        'cms_heading', 'cms_description', 'cms_image', 'display_order', 'button_text'
    ];
}
