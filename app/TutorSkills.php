<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class TutorSkills extends Model
{
    use Notifiable;

    public $table = "tutor_skills";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tutor_id','skill_id'
    ];

    public function CategorySkills()
    {
        return $this->hasOne('App\CategorySkills', 'id','skill_id');
    }
}
