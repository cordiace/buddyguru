<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('welcome');

Auth::routes();

Route::get('/home', 'Admin\AdminController@index')->name('admin.home');
Route::get('/terms-and-conditions', 'CommonController@terms')->name('common.terms');
Route::get('/privacy-policy', 'CommonController@policy')->name('common.policy');
Route::get('/get-batch-by-user', 'CommonController@getBatch')->name('common.getBatch');

//SuperAdmin Routes
Route::get('/admin', 'Admin\AdminController@index')->name('admin.home');
Route::get('/admin/help-desk', 'Admin\AdminController@helpDesk')->name('admin.helpdesk');
Route::resource('/admin/terms',  'Admin\TermsController')->except(['show']);
Route::resource('/admin/policy',  'Admin\PolicyController')->except(['show']);
Route::resource('/admin/user',  'Admin\UserController');
Route::resource('/admin/tutor',  'Admin\TutorController');
Route::resource('/admin/sessions',  'Admin\SessionsController')->except(['show']);
Route::resource('/admin/payment',  'Admin\PaymentController')->except(['show']);
Route::resource('/admin/category',  'Admin\CategoryController')->except(['show']);
Route::resource('/admin/banner',  'Admin\BannerController')->except(['show']);
Route::resource('/admin/subcategory',  'Admin\SubCategoryController')->except(['show']);
Route::resource('/admin/skill',  'Admin\SkillController')->except(['show']);
Route::resource('/admin/country',  'Admin\CountryController')->except(['show']);
Route::resource('/admin/grade',  'Admin\GradeController')->except(['show']);
Route::resource('/admin/cmsfield',  'Admin\CmsFieldController')->except(['show']);
Route::get('/admin/get_subcategory',  'Admin\SubCategoryController@getSubCategory')->name('admin.getsubcategory');
Route::get('/admin/get_adminsubcategory',  'Admin\SubCategoryController@getAdminSubCategory')->name('admin.getadminsubcategory');
Route::get('/admin/profile', 'Admin\AdminController@profile')->name('admin.profile');
Route::get('/admin/send', 'Admin\EmailController@send')->name('email.send');
Route::post('/admin/profile/update', 'Admin\AdminController@profileUpdate')->name('admin.profile.update');
Route::resource('/admin/admin-banner',  'Admin\BannerController')->except(['show']);

Route::resource('/admin/generalsetting',  'Admin\GeneralsettingController')->except(['show']);
Route::resource('/admin/recentbooking',  'Admin\RecentbookingController')->except(['show']);
Route::get('/get_admin_payment',  'Admin\PaymentController@get_admin_payment')->name('get_admin_payment');
Route::get('/admin/refund-payment/{id}', 'Admin\PaymentController@refundPayment')->name('buddy.refund.payment');
Route::get('/get_admin_recentbooking',  'Admin\RecentbookingController@get_admin_recentbooking')->name('get_admin_recentbooking');

Route::get('/admin/tutor/portfolio/{id}', 'Admin\TutorController@createPortfolio')->name('tutor.portfolio.create');
Route::post('/admin/tutor/portfolio', 'Admin\TutorController@storePortfolio')->name('tutor.portfolio.store');
Route::post('/admin/tutor/portfolio/update', 'Admin\TutorController@updatePortfolio')->name('tutor.portfolio.update');
Route::get('/admin/tutor/portfolio/delete/{id}', 'Admin\TutorController@deletePortfolio')->name('tutor.portfolio.delete');

Route::get('/admin/tutor/package/{id}', 'Admin\TutorController@createPackage')->name('tutor.package.create');
Route::post('/admin/tutor/package', 'Admin\TutorController@storePackage')->name('tutor.package.store');
Route::post('/admin/tutor/package/update', 'Admin\TutorController@updatePackage')->name('tutor.package.update');
Route::get('/admin/tutor/package/delete/{id}', 'Admin\TutorController@deletePackage')->name('tutor.package.delete');

Route::get('/admin/tutor/batch/{id}', 'Admin\TutorController@createBatch')->name('tutor.batch.create');
Route::post('/admin/tutor/batch', 'Admin\TutorController@storeBatch')->name('tutor.batch.store');
Route::post('/admin/tutor/batch/update', 'Admin\TutorController@updateBatch')->name('tutor.batch.update');
Route::get('/admin/tutor/batch/delete/{id}', 'Admin\TutorController@deleteBatch')->name('tutor.batch.delete');


