<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class StudentDetail extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'parent_name','parent_email','parent_mobile','grade','skills'
    ];

//StudentDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getSkill()
    {
        return $this->belongsTo('\App\Skill', 'skills', 'id');
    }

    public function getGrade()
    {
        return $this->belongsTo('\App\Grade', 'grade', 'id');
    }
}