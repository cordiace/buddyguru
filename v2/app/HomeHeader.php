<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class HomeHeader extends Authenticatable
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'logo', 'menu','social_links','copy_right','short_links','min_amount','title_terms','description_terms','title_stream','description_stream','play_store_link','app_store_link','footer_logo'
    ];

    //Add extra attribute
    // protected $attributes = ['logo_url'];

    //Make it available in the json response
    protected $appends = ['logo_url','social_links_unserialized','short_links_unserialized'];

    //implement the attribute
    public function getLogoUrlAttribute()
    {
      $file = asset('storage/home/'.$this->logo);
      return $file;
    }

    public function getSocialLinksUnserializedAttribute()
    {
      return unserialize($this->social_links);
    }

    public function getShortLinksUnserializedAttribute()
    {
      return unserialize($this->short_links);
    }
}
