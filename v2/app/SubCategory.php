<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SubCategory extends Model
{
    use Notifiable;
    protected $fillable = [
        'sub_category_name','category_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
