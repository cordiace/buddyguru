<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Package;


class HelpDesk extends Model
{
    use Notifiable;

    protected $table = 'help_desk';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','description','status',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
