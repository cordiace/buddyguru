<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Payment extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'batch_id','package_id','user_id', 'payment_method','charge_id','card_number', 'card_name','expiry_date', 'cvv'
    ];

    //Payment.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //Payment.php
    public function tutor()
    {
        return $this->belongsTo('App\User');
    }

    //Payment.php
    public function session()
    {
        return $this->belongsTo('App\TutorSession');
    }

    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
