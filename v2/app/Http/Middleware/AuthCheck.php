<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use DB;

class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->bearerToken()) {
            $bearer = $request->bearerToken();
            if ($token = DB::table('personal_access_tokens')->where('token', hash('sha256', $bearer))->first()) {

                $now = Carbon::now()->timestamp;
                $expire_date = Carbon::parse($token->expired_at)->timestamp;

                if ($now > $expire_date) {
                    $response['status'] = "true";
                    $response['data']['message'] = 'token expired';
                    return response()->json($response, 401);
                    // if ($user = \App\User::find($token->tokenable_id)) {
                    //     $token = $user->createRefreshToken('web', 20)->plainTextToken;

                    // }

                } else {
                    return $next($request);
                }

            } else {
                $response['errors'] = 'Invalid token';
                return response()->json($response, 403);
            }

        }

        return $next($request);
    }
}
