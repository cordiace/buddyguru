<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceTime;
use App\Service;
use App\HomeBanner;
use App\HomeAbout;
use App\HomeHeader;
use App\Contact;
use App\User;
use App\Banner;
use App\Church;
use App\Video;
use App\Event;
use App\Testimonial;
use App\Port;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $header = HomeHeader::first();
        $testimonials = Testimonial::all();
        $about = HomeAbout::first();
        $banners = HomeBanner::where('page','home')->get();
        $videos = Video::orderBy('created_at', 'desc')->get();
        $serviceTimes = ServiceTime::orderBy('id', 'desc')->get();
        return view('welcome',\compact('serviceTimes','banners','about','header','videos','testimonials'));
    }
    public function subscription()
    {
      $header = HomeHeader::first();
      return view('subscription',\compact('header'));
    }
    public function search(Request $request)
    {
      if($request->name)
      {
        $users = User::where('role','church')
        ->where('name', 'like', '%' . $request->name . '%')
        ->get();
      }
      if ($request->loc) {
        $users = User::where('role','church')
        ->where('location', 'like', '%' . $request->loc . '%')
        ->get();
      }
      if ($request->name && $request->loc) {
        $users = User::where('role','church')
        ->where('name', 'like', '%' . $request->name . '%')
        ->where('location', 'like', '%' . $request->loc . '%')
        ->get();
      }

      return response()->json($users, 200);
    }
    public function getAllChurch()
    {
        $header = HomeHeader::first();
        $banner = HomeBanner::where('id',1)->first();
        $users = User::where('role','church')->get();

        return view('church-list',\compact('banner','users','header'));
    }
    public function church(Request $request)
    {
        $header = HomeHeader::first();
        $user = User::where('slug',$request->slug)->first();

        $banner = Banner::where('user_id',$user->id)->first();
        $videos = Video::where('user_id',$user->id)->orderBy('id', 'desc')->get();
        $events = Event::where('user_id',$user->id)->orderBy('id', 'desc')->get();
        $church = Church::where('user_id',$user->id)->first();
        $services = Service::where('subscription',$user->subscription)->orderBy('id', 'desc')->get();
        $serviceTimes = ServiceTime::where('user_id',$user->id)->orderBy('id', 'desc')->get();
        return view('church',\compact('serviceTimes','header','user','services','banner','church','videos','events'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        $header = HomeHeader::first();
        $about = HomeAbout::first();
        return view('about',\compact('header','about'));
    }
    public function contact()
    {
        $header = HomeHeader::first();
        $contact = Contact::first();
        return view('contact',\compact('header','contact'));
    }
    public function watchOnline()
    {
        $videos = Video::orderBy('created_at', 'desc')->get();
        $header = HomeHeader::first();
        $user = User::where('role', 'admin')->first();
        $events = Event::where('user_id',$user->id)->get();
        $serviceTimes = ServiceTime::orderBy('id', 'desc')->get();
        return view('watch-online',\compact('header','videos','serviceTimes','events'));
    }
    public function termsAndConditions()
    {
        $header = HomeHeader::first();
        return view('terms-and-conditions',\compact('header'));
    }
    public function donateNow()
    {
        $header = HomeHeader::first();
        $serviceTimes = ServiceTime::orderBy('id', 'desc')->get();
        $user = User::where('role', 'admin')->first();
        $events = Event::where('user_id',$user->id)->get();
        return view('donate-now',\compact('header','serviceTimes','events'));
    }
    public function ourStream()
    {
        $header = HomeHeader::first();
        $serviceTimes = ServiceTime::orderBy('id', 'desc')->get();
        return view('ourstream',\compact('header','serviceTimes'));
    }
    public function checkLiveStream(Request $request)
    {
      $port = Port::where('user_email',$request->email)->first();
      return response()->json(['data' => $port], 200);
    }
}
