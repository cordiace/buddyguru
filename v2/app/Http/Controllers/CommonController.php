<?php

namespace App\Http\Controllers;

use App\Batch;
use App\HomeBanner;
use App\HomeHeader;
use App\Policy;
use App\Terms;
use Illuminate\Http\Request;
use DB;

class CommonController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getAccessToken(Request $request)
    {
        $bearer = $request->bearerToken();
        if ($token = DB::table('personal_access_tokens')->where('token', hash('sha256', $bearer))->first()) {
            if ($user = \App\User::find($token->tokenable_id)) {
                $token = $user->createAuthToken('auth_token', env('ACCESS_TOKEN_TIME', 10))->plainTextToken;
                $user->token = $token;
                $user->save();
                $response['status'] = "true";
                $response['data']['token'] = $token;

                return response()->json($response, 200);
            }
        }

        $response['errors'] = 'error getting data';
        return response()->json($response, 401);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function terms()
    {
        $header = HomeHeader::first();
        $banner = HomeBanner::where('page', 'login')->first();
        $terms = Terms::all();
        return view('terms', \compact('terms', 'header', 'banner'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function policy()
    {
        $header = HomeHeader::first();
        $banner = HomeBanner::where('page', 'login')->first();
        $policies = Policy::all();
        return view('policy', \compact('policies', 'header', 'banner'));
    }

    public function getBatch(Request $request)
    {
        $batches = Batch::where('user_id', $request->id)->get();

        return response()->json($batches, 200);
    }

}
