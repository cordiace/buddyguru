<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\TutorSession;
use App\HelpDesk;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;



class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('is.admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function helpDesk()
    {
        $help = HelpDesk::all();

        return view('admin.help.index',\compact('help'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $now = Carbon::now();

        $sessions = TutorSession::all();
        $tutors = User::where('role','tutor')->get();
        $users = User::where('role','student')->get();
        $latest_tutors = User::where('role','tutor')->orderBy('created_at', 'desc')->take(5)->get();
        $latest_students = User::where('role','student')->orderBy('created_at', 'desc')->take(5)->get();
        $last_tutor= collect($tutors)->last();
        $last_student= collect($users)->last();
        $tutors_monthly = User::select('id', 'created_at')->where('role','tutor')->whereYear('created_at',$now->year)->get()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m');
        });
        $student_monthly = User::select('id', 'created_at')->where('role','student')->whereYear('created_at',$now->year)->get()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m');
        });
        $grapghTwo = $this->getMonthData($tutors_monthly);
        $grapghOne = $this->getMonthData($student_monthly);

        return view('admin.home',\compact('sessions','users','tutors', 'last_tutor', 'last_student', 'latest_tutors', 'latest_students','grapghTwo','grapghOne'));

    }

    public function getMonthData($data)
    {
        $datamcount = [];
        $dataArr = [];
        foreach ($data as $key => $value) {
            $datamcount[(int)$key] = count($value);
        }
        for($i = 1; $i <= 12; $i++){
            if(!empty($datamcount[$i])){
                $dataArr[$i] = $datamcount[$i];
            }else{
                $dataArr[$i] = 0;
            }
        }

        return $dataArr;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.profile',\compact('user'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profileUpdate(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email,'.$request->id,
            'name' => 'required',
            'password' => 'sometimes|confirmed',
        ]);

        if($request->password)
        {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ];
        }
        else{
            $data = [
                'name' => $request->name,
                'email' => $request->email,
            ];
        }

        $user =  User::updateOrCreate(
            ['id' =>$request->id],
            $data
        );

        return redirect()->back();
    }

}
