<?php

namespace App\Http\Controllers\Admin;

use App\Generalsetting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GeneralsettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $banners=Generalsetting::all();
        // return view('admin.generalsetting.index',\compact('banners'));
        $id=1;  
        $banner = Generalsetting::find($id);
        if($banner){
            return view('admin.generalsetting.create_edit', \compact('banner'));
        }
        else{
            return view('admin.generalsetting.create_edit');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.generalsetting.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'commision' => ['required', 'numeric', 'max:99'],
        ]);
       
        $banner =  Generalsetting::create([
            'commision' => $request->commision,
          

        ]);
        return redirect()->route('generalsetting.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $id=1;
        
        $banner = Generalsetting::findOrFail($id);
        return view('admin.generalsetting.create_edit', \compact('banner'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'commision' => ['required', 'numeric', 'max:99'],
           
        ]);
       
        $banner =  Generalsetting::updateOrCreate(
            ['id' =>$id],
            [ 
             'commision' => $request->commision,

            ]
        );
        return redirect()->route('generalsetting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Generalsetting::find($id);
        $categories->delete();

        return redirect()->route('generalsetting.index');
    }
}
