<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_categories=SubCategory::all();
        return view('admin.subcategory.index',\compact('sub_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subset=Category::all();
        $category_list = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        return view('admin.subcategory.create_edit',\compact('category_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => ['required' ],
            'sub_category_name' => ['required', 'string', 'max:255'],
        ]);
        $sub_category =  SubCategory::create([
            'category_id' => $request->category_id,
            'sub_category_name' => $request->sub_category_name,

        ]);
        return redirect()->route('subcategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = SubCategory::findOrFail($id);
        $subset=Category::all();
        $category_list = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        return view('admin.subcategory.create_edit', \compact('subcategory','category_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id' => ['required' ],
            'sub_category_name' => ['required', 'string', 'max:255'],
        ]);
        $sub_category =  SubCategory::updateOrCreate(
            ['id' =>$id],
            [
            'category_id' => $request->category_id,
            'sub_category_name' => $request->sub_category_name,

            ]
        );
        return redirect()->route('subcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub_categories = SubCategory::find($id);
        $sub_categories->delete();

        return redirect()->route('subcategory.index');
    }

    public function getSubCategory()
    {
        $category_id=$_GET['category'];

        $subset=SubCategory::where('category_id',$category_id)->get();
        $sub_categories = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'sub_category_name'])
                ->all();
        });


        return response()->json($sub_categories, 200);
    }

    public function getAdminSubCategory()
    {
        $category_id=$_GET['category'];
        $category=Category::where('id',$category_id)->first();

        $subset=SubCategory::where('category_id',$category->id)->get();
        $sub_categories = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'sub_category_name'])
                ->all();
        });


        return response()->json($sub_categories, 200);
    }
}
