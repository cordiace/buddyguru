<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Country;
use App\Skill;
use App\SubCategory;
use App\Http\Controllers\Controller;
use App\TutorDetail;
use App\TutorPortfolio;
use App\TutorSession;
use Illuminate\Http\Request;
use App\User;
use App\Package;
use App\PackageBatch;
use App\Batch;
use App\HelpDesk;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Traits\VideoFFMpeg;


class TutorController extends Controller
{
  use VideoFFMpeg;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::where('role','tutor')->get();
        return view('admin.tutor.index',\compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $sub_category=array();
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $skill_subset=Skill::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        return view('admin.tutor.create_edit',\compact('category','sub_category','countries','skills'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'category' => ['required'],
            'dob' => ['required', 'date'],
            // 'sub_category' => ['required'],
//            'slug' => ['required', 'string', 'max:255', 'unique:users'],
//            'phone_number' => ['required', 'numeric', 'digits:10','unique:users'],
            'location' => ['required', 'string',],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
        ]);
        do {
            $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
        } while (User::where("token", "=", $token_key)->first() instanceof User);

        $path_image = '';
        if($request->profile_image){
            $pro_image = $request->file('profile_image');
            $path_image = $pro_image->store('/uploads/tutor',['disk' => 'public']);
        }

        $user =  User::create([
            'name' => $request->name,
            'slug' => Str::slug($request->slug),
            'dob' => $request->dob,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'location' => $request->location,
            'token' => $token_key,
            'role' => 'tutor',
            'password' => Hash::make($request->password),
            'profile_image' => $path_image,
        ]);

        if(isset($request->college_name))
        {
            foreach ($request->college_name as $key=>$value){
                if($value){
                    $experience[$key]=[
                        'name' => $value,
                        'designation' => $request->designation[$key]
                    ];
                }
            }
        }



        $tutor_detail=TutorDetail::create([
            'user_id'=>$user->id,
            'category'=>$request->category,
            'sub_category'=>3,
            'skills'=>1,
            // 'about'=>$request->about,
            // 'experience'=>json_encode($experience),
        ]);



        return redirect()->route('tutor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('admin.tutor.show', \compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $userId)
    {
        $user = User::findOrFail($userId);
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $category_object=Category::where('id',$user->tutor_detail->category)->first();
        $sub_category=array();
        if($category_object) {
            $sub_cat = SubCategory::where('category_id', $category_object->id)->get();
            $sub_category = $sub_cat->map(function ($sub_cat) {
                return collect($sub_cat->toArray())
                    ->only(['id', 'sub_category_name'])
                    ->all();
            });
        }
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $skill_subset=Skill::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });

        return view('admin.tutor.create_edit', \compact('user','category','sub_category','countries','skills'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email,'.$id,
//            'slug' => 'required|unique:users,slug,'.$id,
//            'phone_number' => 'required|unique:users,phone_number,'.$id,
'category' => ['required'],
'dob' => 'required',
            // 'sub_category' => ['required'],
            'name' => 'required',
            'location' => 'required',
            'password' => 'sometimes|confirmed',
            'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
        ]);

        $path_image = '';
        if($request->profile_image){
            if(User::where('id', $id)->exists()){
                $user = User::find($id);
                if($user->profile_image){
                    if(Storage::disk('public')->exists($user->profile_image)){
                        Storage::disk('public')->delete($user->profile_image);
                    }
                }
            }
            $pro_image = $request->file('profile_image');
            $path_image = $pro_image->store('/uploads/tutor',['disk' => 'public']);
        }

        if($request->password)
        {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'password' => Hash::make($request->password),
                'profile_image' => $path_image,
            ];
        }
        else{
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'profile_image' => $path_image,
            ];
        }
        if(isset($request->college_name))
        {

        foreach ($request->college_name as $key=>$value){
            if($value){
                $experience[$key]=[
                    'name' => $value,
                    'designation' => $request->designation[$key]
                ];
            }
        }
    }

        $tutor_data = [
            'category'=>$request->category,
            'sub_category'=>1,
            // 'skills'=>$request->skills,
            // 'about'=>$request->about,
            // 'experience'=>json_encode($experience),
        ];

        $user =  User::updateOrCreate(
            ['id' =>$id],
            $data
        );

        $tutor_detail =  TutorDetail::updateOrCreate(
            ['user_id' =>$id],
            $tutor_data
        );

        return redirect()->route('tutor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $tutor_details = TutorDetail::where('user_id',$id)->get();

        foreach ($tutor_details as $tutor_detail) {
            $tutor_detail->delete();
        }

        $batches = Batch::where('user_id',$id)->get();
        foreach ($batches as $batch) {
            $packageBatch = PackageBatch::where('batch_id',$batch->id)->delete();
            $session = TutorSession::where('batch_id',$batch->id)->delete();
            $batch->delete();
        }

        $packages = Package::where('user_id',$id)->get();
        foreach ($packages as $package) {
            $packageBatch = PackageBatch::where('package_id',$package->id)->delete();
            $package->delete();
        }

        $help = HelpDesk::where('user_id',$id)->delete();

        $userPortfolio = TutorPortfolio::where('user_id',$id)->delete();


        $user->delete();
        return redirect()->route('tutor.index');
    }

    /**
     * Show the form for tutor portfolio.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createPortfolio($id)
    {
        $userPortfolios = TutorPortfolio::where('user_id',$id)->get();
        return view('admin.tutor.create_edit_portfolio', \compact('id','userPortfolios'));
    }

    public function storePortfolio(Request $request)
    {
        $portfolio = new TutorPortfolio();
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'video_url' => 'required|mimes:mp4,mov,ogg,qt|max:20000',
            // 'video_thumbnail' => 'required|mimes:jpeg,bmp,png',
        ]);

        // if ($request->video_url) {
        //     $video = $request->file('video_url');
        //     $path_video = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);
        // }

        if ($request->video_url) {
            $video = $request->file('video_url');
            $path_video = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);

            $filename = str_replace("uploads/tutor/portfolio/", "", $path_video);
            $filenameWithoutExtenstion = pathinfo($filename, PATHINFO_FILENAME);

            $thumbnail = "uploads/tutor/portfolio/" . $filenameWithoutExtenstion . ".png";
            $sec = 10;

            $result = $this->getImageFromVideo($sec, $path_video, $thumbnail);

        }

        // if ($request->video_thumbnail) {
        //     $video = $request->file('video_thumbnail');
        //     $path_video_thumbnail = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);
        // }

        $userPortfolio =  TutorPortfolio::create([
            'user_id' => $request->user_id,
            'title' => $request->title,
            'description' => $request->description,
            'video_url' => $path_video,
            'video_thumbnail' => $thumbnail,
        ]);

        return redirect()->back();


    }

    public function updatePortfolio(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'video_url' => 'sometimes|mimes:mp4,mov,ogg,qt|max:20000',
            'video_thumbnail' => 'sometimes|mimes:jpeg,bmp,png',
        ]);


        if ($request->video_url) {
            $video = $request->file('video_url');
            $path_video = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);
        }

        if ($request->video_thumbnail) {
            $video = $request->file('video_thumbnail');
            $path_video_thumbnail = $video->store('/uploads/tutor/portfolio', ['disk' => 'public']);
        }

        $userPortfolio =  TutorPortfolio::updateOrCreate(
            ['id' => $request->id],
        [
            'title' => $request->title,
            'description' => $request->description,
            'video_url' => ($request->video_url) ? $path_video : $request->temp_video_url,
            'video_thumbnail' => ($request->video_thumbnail) ? $path_video_thumbnail : $request->temp_video_thumbnail,
        ]);

        return redirect()->back();


    }

    public function deletePortfolio($id)
    {
        $userPortfolio = TutorPortfolio::find($id);
        $userPortfolio->delete();
        return redirect()->back();
    }

    /**
     * Show the form for tutor package.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createPackage($id)
    {
        $userPackages = Package::where('user_id',$id)->get();
        $userBatches = Batch::where('user_id',$id)->get();
        return view('admin.tutor.create_edit_package', \compact('id','userPackages','userBatches'));
    }

    public function storePackage(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'package_name' => 'required',
            'description' => '',
            // 'subscription_fee' => 'required|numeric',
        ]);

        $userPackage =  Package::create([
            'package_name' => $request->package_name,
            'description' => $request->description,
            // 'subscription_fee' => $request->subscription_fee,
            'user_id' => $request->user_id,
        ]);

        if(isset($request->batch_id))
        {
            foreach($request->batch_id as $id)
            {
                $packageBatch = new PackageBatch();
                $packageBatch->package_id = $userPackage->id;
                $packageBatch->batch_id = $id;
                $packageBatch->save();
            }
        }

        return redirect()->back();

    }

    public function updatePackage(Request $request)
    {


        $request->validate([
            'package_name' => 'required',
            'description' => '',
            // 'subscription_fee' => 'required',
        ]);

        try {
        $userPackage =  Package::updateOrCreate(
            ['id' => $request->id],
        [
            'package_name' => $request->package_name,
            'description' => $request->description,
            // 'subscription_fee' => $request->subscription_fee,
        ]);
        } catch (\Illuminate\Database\QueryException $e) {
        dd($e);
        } catch (\Exception$e) {
        dd($e);
        }

        $packageBatch = PackageBatch::where('package_id',$request->id)->delete();
        if(isset($request->batch_id))
        {
            foreach($request->batch_id as $id)
            {
                $packageBatch = new PackageBatch();
                $packageBatch->package_id = $userPackage->id;
                $packageBatch->batch_id = $id;
                $packageBatch->save();
            }
        }


        return redirect()->back();


    }

    public function deletePackage($id)
    {
        $packageBatch = PackageBatch::where('package_id',$id)->delete();
        $userPackages = Package::find($id);
        $userPackages->delete();
        return redirect()->back();
    }

    /**
     * Show the form for tutor batch.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createBatch($id)
    {
        $userBatches = Batch::where('user_id',$id)->get();
        $userPackages = Package::where('user_id',$id)->get();
        return view('admin.tutor.create_edit_batch', \compact('id','userBatches','userPackages'));
    }

    public function storeBatch(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'batch_name' => 'required',
            'description' => '',
            'start' => 'required',
            'end' => 'required',
            'duration' => 'required',
            'no_of_participants' => 'required',
            'no_of_sessions' => 'required',
            'payment_type' => 'required',
            'subscription_fee' => 'required',
        ]);

        try
        {

        $userBatch =  Batch::create([
            'user_id' => $request->user_id,
            'batch_name' => $request->batch_name,
            'description' => $request->description,
            'start' => $request->start,
            'end' => $request->end,
            'duration' => $request->duration,
            'no_of_participants' => $request->no_of_participants,
            'no_of_sessions' => $request->no_of_sessions,
            'payment_type' => $request->payment_type,
            'subscription_fee' => $request->subscription_fee,
        ]);

        if($request->package_id)
        {
            $packageBatch = new PackageBatch();
            $packageBatch->package_id = $request->package_id;
            $packageBatch->batch_id = $userBatch->id;
            $packageBatch->save();
        }

        return redirect()->back();
} catch (\Illuminate\Database\QueryException $e) {
    $response['errors'] = 'error deleting batch data';
    return response()->json($response, 403);
} catch (\Exception $e) {
    $response['errors'] = 'error getting batch data';
    return response()->json($response, 403);
}



    }

    public function updateBatch(Request $request)
    {
        $request->validate([
            'batch_name' => 'required',
            'description' => '',
            'start' => 'required',
            'end' => 'required',
            'duration' => 'required',
            'no_of_participants' => 'required',
            'no_of_sessions' => 'required',
            'payment_type' => 'required',
            'subscription_fee' => 'required',
        ]);


        $userBatch =  Batch::updateOrCreate(
            ['id' => $request->id],
        [
            'batch_name' => $request->batch_name,
            'description' => $request->description,
            'start' => $request->start,
            'end' => $request->end,
            'duration' => $request->duration,
            'no_of_participants' => $request->no_of_participants,
            'no_of_sessions' => $request->no_of_sessions,
            'payment_type' => $request->payment_type,
            'subscription_fee' => $request->subscription_fee,
        ]);

        // $packageBatch = PackageBatch::where('package_id',$request->id)->delete();
        // foreach($request->batch_id as $id)
        // {
        //     $packageBatch = new PackageBatch();
        //     $packageBatch->package_id = $package->id;
        //     $packageBatch->batch_id = $id;
        //     $packageBatch->save();
        // }

        return redirect()->back();


    }

    public function deleteBatch($id)
    {
      $packageBatch = PackageBatch::where('batch_id',$id)->delete();
        $sessions = TutorSession::where('batch_id',$id)->delete();
        $userPackages = Batch::find($id);
        $userPackages->delete();
        return redirect()->back();
    }
}
