<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Grade;
use App\Skill;
use App\SubCategory;
use App\Http\Controllers\Controller;
use App\Sessions;
use App\User;
use App\Batch;
use Illuminate\Http\Request;

class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sessions=Sessions::all();
        return view('admin.sessions.index',\compact('sessions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subset=User::where('role','tutor')->get();
        $tutors_list = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $grade = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $sub_category=array();
        $skill_subset=Skill::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        $batches = Batch::all();
        return view('admin.sessions.create_edit',\compact('batches','tutors_list','category','sub_category','skills','grade'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => ['required', 'string', 'max:255'],
            'user_id' => ['required', 'numeric'],
        ]);
        $learn=array();
        foreach ($request->learn as $key=>$value){
            if($value!=""){
                array_push($learn,$value);
            }
        }
//        $request->learn=array_filter($request->learn);
        $user =  Sessions::create([
            'user_id' => $request->user_id,
            'type' => $request->type,
            'grade' => $request->grade,
            'subject' => $request->subject,
            'skill' => $request->skill,
            'session_name' => $request->session_name,
            'session_date' => $request->session_date,
            'session_time' => $request->session_time,
            'duration' => $request->duration,
            'amount' => $request->amount,
            'session_link' => $request->session_link,
            'description' => $request->description,
            'learn' => json_encode($learn),

        ]);
        return redirect()->route('sessions.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($session_id)
    {
        $sessions = Sessions::findOrFail($session_id);
        $subset=User::where('role','tutor')->get();
        $tutors_list = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $grade = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });
        $cat_subset=Category::all();
        $category = $cat_subset->map(function ($cat_subset) {
            return collect($cat_subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $sub_cat=SubCategory::where('category_id',$sessions->grade)->get();
        $sub_category = $sub_cat->map(function ($sub_cat) {
            return collect($sub_cat->toArray())
                ->only(['id', 'sub_category_name'])
                ->all();
        });
        $sessions->learn=json_decode($sessions->learn);
        $skill_subset=Skill::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        return view('admin.sessions.create_edit', \compact('sessions','tutors_list','category','sub_category', 'skills', 'grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type' => ['required', 'string', 'max:255'],
            'user_id' => ['required', 'numeric',],
            'session_date' => ['required', 'date', ''],
        ]);
        $learn=array();
        foreach ($request->learn as $key=>$value){
            if($value!=""){
                array_push($learn,$value);
            }
        }
//        $request->learn=array_filter($request->learn);
            $data = [
                'user_id' => $request->user_id,
                'type' => $request->type,
                'grade' => $request->grade,
                'subject' => $request->subject,
                'skill' => $request->skill,
                'session_name' => $request->session_name,
                'session_date' => $request->session_date,
                'session_time' => $request->session_time,
                'duration' => $request->duration,
                'amount' => $request->amount,
                'session_link' => $request->session_link,
                'description' => $request->description,
                'learn' => json_encode($learn),
            ];

        $user =  Sessions::updateOrCreate(
            ['id' =>$id],
            $data
        );

        return redirect()->route('sessions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sessions = Sessions::find($id);
        $sessions->delete();

        return redirect()->route('sessions.index');
    }
}
