<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Batch;
use App\User;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $payments=Payment::all();
        return view('admin.payment.index',\compact('payments'));
    }

    public function refundPayment(Request $request, $id)
    {
        $payment = Payment::find($id);

        $batch = Batch::where("id", $payment->batch_id)->first();
        $user = User::where("id", $payment->user_id)->first();

        if ($batch->payment_type != 'monthly') {

            $existsPayment = Payment::where('batch_id', $request->batch_id)
                ->where('package_id', $request->package_id)
                ->where('user_id', $user->id)
                ->first();

            if ($existsPayment) {

                if ($existsPayment->charge_id) {
                    $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
                    $refund = $stripe->refunds->create([
                        'charge' => $existsPayment->charge_id,
                    ]);

                    if ($refund['status'] = 'succeeded') {
                        $existsPayment->payment_status = 2;
                        $existsPayment->save();

                        $subscriber = Subscriber::where(['tutor_id' => $batch->user_id, 'user_id' => $user->id])->first();
                        if ($subscriber) {
                            $subscriber->status = 0;
                            $subscriber->save();
                        }

                        return redirect()->back()->with('message', 'Refund success');
                    }
                }
            } else {
                return redirect()->back()->with('message-error', 'Refund failed');
            }

        }
        else{
            return redirect()->back()->with('message-error', 'Refund failed');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
