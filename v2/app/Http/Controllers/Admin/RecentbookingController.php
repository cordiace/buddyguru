<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payment;

use Illuminate\Http\Request;

class RecentbookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fdate='';
        $tdate='';
        if(isset($request->fdate)){
            $fdate=$request->fdate;  
            $fdate = date('Y-m-d', strtotime($fdate));
        }

        if(isset($request->tdate)){
            $tdate=$request->tdate;  
            
           $tdate = date('Y-m-d', strtotime($tdate));
        }

        $payments=Payment::select();
        // if(isset($fdate) && isset($tdate)){
        // $payments=$payments->WhereBetween('created_at', [$fdate, $tdate]);
        // }
        
        $payments=$payments->get();
        
        return view('admin.recentbooking.index',\compact('payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      


        
    }
    public function get_admin_recentbooking(Request $request)
    {
        $fdate='';
        $tdate='';

        if(isset($request->fdate)){
            $fdate=$request->fdate;  
            $fdate = date('Y-m-d', strtotime($fdate));
        }

        if(isset($request->tdate)){
            $tdate=$request->tdate;  
            
           $tdate = date('Y-m-d', strtotime($tdate));
        }
        
        $payments=Payment::select();

        if(isset($fdate) && isset($tdate)){
        $payments=$payments->WhereBetween('created_at', [$fdate." 00:00:00", $tdate." 23:59:59"]);
        }

        $payments=$payments->get();
      
        return view('admin.recentbooking.index',\compact('payments'));
        
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
