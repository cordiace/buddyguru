<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Country;
use App\Grade;
use App\Skill;
use App\Http\Controllers\Controller;
use App\StudentDetail;
use App\TutorDetail;
use Illuminate\Http\Request;
use App\User;
//use App\Church;
//use App\Banner;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::where('role','student')->get();
        return view('admin.user.index',\compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $grade = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $skill_subset=Skill::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        return view('admin.user.create_edit',\compact('category','skills','countries','grade'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'dob' => ['required', 'date'],
            'location' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'numeric', 'digits:10','unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
        ]);
        do {
            $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
        } while (User::where("token", "=", $token_key)->first() instanceof User);

        $path_image = '';
        if($request->profile_image){
            $pro_image = $request->file('profile_image');
            $path_image = $pro_image->store('/uploads/student',['disk' => 'public']);
        }

        $user =  User::create([
            'name' => $request->name,
            'slug' => Str::slug($request->slug),
            'location' => $request->location,
            'email' => $request->email,
            'dob' => $request->dob,
            'phone_number' => $request->phone_number,
            'token' => $token_key,
            'role' => 'student',
            'password' => Hash::make($request->password),
            'profile_image' => $path_image,
        ]);

        $user_detail=StudentDetail::create([
            'user_id'=>$user->id,
            // 'parent_name'=>$request->parent_name,
            // 'parent_email'=>$request->parent_email,
            // 'parent_mobile'=>$request->parent_mobile,
            'grade'=>$request->grade,
            'skills'=>$request->skills,
        ]);

        // Send Credentials to Church
//        Mail::to($request->email)->send(new SendCredentialsToChurch(
//            $request->input('email'),
//            $request->input('password'),
//            $request->input('name')
//        ));

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('admin.user.show', \compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $userId)
    {
        $user = User::findOrFail($userId);
        $subset=Category::all();
        $category = $subset->map(function ($subset) {
            return collect($subset->toArray())
                ->only(['id', 'category_name'])
                ->all();
        });
        $grade_subset=Grade::all();
        $grade = $grade_subset->map(function ($grade_subset) {
            return collect($grade_subset->toArray())
                ->only(['id', 'grade_name'])
                ->all();
        });
        $country_subset=Country::all();
        $countries = $country_subset->map(function ($country_subset) {
            return collect($country_subset->toArray())
                ->only(['id', 'country_name'])
                ->all();
        });
        $skill_subset=Skill::all();
        $skills = $skill_subset->map(function ($skill_subset) {
            return collect($skill_subset->toArray())
                ->only(['id', 'skill_name'])
                ->all();
        });
        return view('admin.user.create_edit', \compact('user','category','skills','countries','grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email,'.$id,
//            'slug' => 'required|unique:users,slug,'.$id,
            'dob' => 'required',
            'phone_number' => 'required|unique:users,phone_number,'.$id,
            'name' => 'required',
            'password' => 'sometimes|confirmed',
            'profile_image' => ['nullable','mimes:jpg,jpeg,png','max:1024'],
        ]);

        $path_image = '';
        if($request->profile_image){
            if(User::where('id', $id)->exists()){
                $user = User::find($id);
                if($user->profile_image){
                    if(Storage::disk('public')->exists($user->profile_image)){
                        Storage::disk('public')->delete($user->profile_image);
                    }
                }
            }
            $pro_image = $request->file('profile_image');
            $path_image = $pro_image->store('/uploads/student',['disk' => 'public']);
        }

        if($request->password)
        {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'dob' => $request->dob,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'password' => Hash::make($request->password),
                'profile_image' => $path_image,
            ];
        }
        else{
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'dob' => $request->dob,
                'slug' => Str::slug($request->slug),
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'profile_image' => $path_image,
            ];
        }

        $detail_data = [
            // 'parent_name'=>$request->parent_name,
            // 'parent_email'=>$request->parent_email,
            // 'parent_mobile'=>$request->parent_mobile,
            'grade'=>$request->grade,
            'skills'=>$request->skills,
        ];

        $user =  User::updateOrCreate(
            ['id' =>$id],
            $data
        );
        $user_detail =  StudentDetail::updateOrCreate(
            ['user_id' =>$id],
            $detail_data
        );

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user_details = StudentDetail::where('user_id',$id)->get();
        foreach ($user_details as $user_detail) {
            $user_detail->delete();
        }

        $user->delete();
        return redirect()->route('user.index');
    }

    public function logoutApi(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'token' => 'required|exists:users,token|bail',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['errors' => $validatedData->errors()->first('token')], 400);
        } else {
            $user = User::where("token", "=", $request->token)->first();
            do {
                $token_key = Str::random(24); ///api_token' => Str::random(60),use Illuminate\Support\Str;
            } while (User::where("token", "=", $token_key)->first() instanceof User);
            $user->token = $token_key;
            $user->save();
        }
    }

}
