<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Skill extends Model
{
    use Notifiable;
    protected $fillable = [
        'skill_name'
    ];

    public function session_skill()
    {
        return $this->hasMany(\App\Sessions::class);
    }
}
