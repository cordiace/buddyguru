<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class TutorDetail extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'category','sub_category','skills', 'about', 'experience'
    ];

    //TutorDetail.php
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tutorskill()
    {
        return $this->hasOne('App\Skill', 'skills','id');
    }

    /**
     * Get the category that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('\App\Category', 'category', 'id');
    }

    /**
     * Get the sub-category that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSubCategory()
    {
        return $this->belongsTo('\App\SubCategory', 'sub_category', 'id');
    }

    /**
     * Get the skill that owns the TutorDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSkill()
    {
        return $this->belongsTo('\App\Skill', 'skills', 'id');
    }
}
