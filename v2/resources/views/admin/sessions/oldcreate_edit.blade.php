@php
$formAction = isset($sessions) ? route('sessions.update', $sessions->id) : route('sessions.store');
$issetSessions = isset($sessions) ? 1 : 0;
$subjects=array('subject'=>'Subject','skill'=>'Skill');
$grades=$grade;
$class_subjects=$category;
//$skills=array('cooking'=>'Cooking','painting'=>'Painting','craft'=>'Craft','singing'=>'Singing');
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">
                            @isset($sessions)
                            Edit Session Details
                            @else
                            Add New Session
                            @endisset
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8"
                            enctype="multipart/form-data">
                            @isset($sessions) @method('PUT') @endisset
                            @csrf
                            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Tutor</label>
                    <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror">
                      {{--<option value="india">India</option>--}}
                      {{--<option value="uk">UK</option>--}}
                      {{--<option value="us">US</option>--}}
                      @foreach ($tutors_list as $key => $value)
                        <option value="{{ $value['id'] }}"
                                @if ($value['id'] == old('user_id', ($issetSessions?$sessions->user_id:'')))
                                selected="selected"
                                @endif
                        >{{ $value['name'] }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div> 
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Session Type</label>
                                        <select name="type" id="type"
                                            class="form-control @error('type') is-invalid @enderror">
                                            @foreach ($subjects as $key => $value)
                                            <option value="{{ $key }}" @if ($value==old('type',
                                                ($issetSessions?$sessions->type:'')))
                                                selected="selected"
                                                @endif
                                                >{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> 
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Grade</label>
                                        <select name="grade" id="grade"
                                            class="form-control @error('grade') is-invalid @enderror">
                                            <option value=""></option>
                                            @foreach ($grades as $key => $value)
                                            <option value="{{ $value['grade_name'] }}" @if
                                                ($value['grade_name']==old('grade', ($issetSessions?$sessions->
                                                grade:'')))
                                                selected="selected"
                                                @endif
                                                >{{ $value['grade_name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Subject</label>
                                        <select name="subject" id="subject"
                                            class="form-control @error('subject') is-invalid @enderror">
                                            <option value=""></option>
                                            @foreach ($class_subjects as $key => $value)
                                            <option value="{{ $value['category_name'] }}" @if
                                                ($value['category_name']==old('subject', ($issetSessions?$sessions->
                                                subject:'')))
                                                selected="selected"
                                                @endif
                                                >{{ $value['category_name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Your Skill</label>
                                        <select name="skill" id="skill"
                                            class="form-control @error('skill') is-invalid @enderror">
                                            <option value=""></option>
                                            @foreach ($skills as $key => $value)
                                            <option value="{{ $value['skill_name'] }}" @if
                                                ($value['skill_name']==old('skills', ($issetSessions?$sessions->
                                                skill:'')))
                                                selected="selected"
                                                @endif
                                                >{{ $value['skill_name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Session Title</label>
                                        <input id="skill" type="text" name="session_name" class="form-control"
                                            value="{{ old('session_name') ?? ($issetSessions ? $sessions->session_name : '')  }}">
                                        @error ('session_name')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{--<div class="col-md-12">--}}
                                <div class="col-md-3">
                                    <label class="bmd-label-floating">Session Date</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input id="session_date" type="date" name="session_date" class="form-control"
                                            value="{{ old('session_date') ?? ($issetSessions ? $sessions->session_date : '')  }}">
                                        @error ('session_date')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Session Time</label>
                                        <input id="session_time" type="text" name="session_time" class="form-control"
                                            value="{{ old('session_time') ?? ($issetSessions ? date('h:i a', strtotime($sessions->session_time)) : '')  }}">
                                        @error ('session_time')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Duration(Minutes)</label>
                                        <input id="duration" type="number" name="duration" class="form-control"
                                            value="{{ old('duration') ?? ($issetSessions ? $sessions->duration : '')  }}">
                                        @error ('duration')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Amount($)</label>
                                        <input id="amount" type="text" name="amount" class="form-control"
                                            value="{{ old('amount') ?? ($issetSessions ? number_format((float)$sessions->amount, 2, '.', '') : '')  }}">
                                        @error ('amount')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Session Link</label>
                                        <input id="session_link" type="text" name="session_link" class="form-control"
                                            value="{{ old('session_link') ?? ($issetSessions ? $sessions->session_link : '')  }}">
                                        @error ('session_link')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Describe your session</label>
                                        <input id="description" type="text" name="description" class="form-control"
                                            value="{{ old('description') ?? ($issetSessions ? $sessions->description : '')  }}">
                                        @error ('session_name')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-md-12">
                                    <div class="form-group add_another">
                                        <label class="bmd-label-floating">What student will learn</label>
                                        @isset($sessions->learn)
                                        @php
                                        $i = 0;
                                        $len = count($sessions->learn);
                                        @endphp
                                        @foreach ($sessions->learn as $learn)
                                        @if($learn!="")
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input id="learn" type="text" name="learn[]" class="form-control"
                                                        value="{{ old('learn') ?? ($issetSessions ? $learn : '')  }}">
                                                    @error ('learn')
                                                    <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if($i != 0)
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <button type="button" rel="tooltip" title=""
                                                        class="remove btn btn-primary btn-link btn-sm">
                                                        <i class="material-icons">remove</i>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        @php
                                        $i++;
                                        @endphp
                                        @endif
                                        @endforeach
                                        @endisset
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" {{($issetSessions) ? '':'required' }}
                                                        name="learn[]" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <button type="button" rel="tooltip" title=""
                                                        class="add btn btn-primary btn-link btn-sm">
                                                        <i class="material-icons">add</i>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="bmd-label-floating">What student will learn</label>--}}
                            {{--<input id="learn" type="text" name="learn" class="form-control"--}}
                            {{--value="{{ old('learn') ?? ($issetSessions ? $sessions->learn : '')  }}">--}}
                            {{--@error ('learn')--}}
                            {{--<p class="text-danger">{{ $message }}</p>--}}
                            {{--@enderror--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        @isset($sessions)
        <div class="row d-flex justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">
                            Delete this Session
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('sessions.destroy', $sessions->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <div class="row">
                                <div class="col-md-12">
                                    Do you really want to do this ? Make sure you are selected the right item. This
                                    action is irreversible.
                                    All data associated with this item will be erased permanantly.
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Delete</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endisset
    </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$(document).ready(function() {
    $(".add").on("click", function(e) {
        e.preventDefault();
        $('.add_another').append(
            '<div class="row"> <div class="col-md-8"> <div class="form-group"> <input type="text" {{(isset($issetSessions)) ? '
            ':'
            required ' }} name="learn[]" class="form-control"> </div> </div> <div class="col-md-4"> <div class="form-group"> <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button> </div> </div> </div>'
            );
    });
    $("body").on("click", ".remove", function(e) {
        e.preventDefault();
        $(this).parent().parent().parent().remove();
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    {
        {
            --$("#grade").on('change', function(e) {
                    --
                }
            } {
                {
                    --e.preventDefault();
                    --
                }
            } {
                {
                    --
                    var category = $('#grade').val();
                    --
                }
            } {
                {
                    --console.log(category) --
                }
            } {
                {
                    --$.ajax({
                            --
                        }
                    } {
                        {
                            --url: "{{route('admin.getsubcategory')}}", --
                        }
                    } {
                        {
                            --type: "GET", --
                        }
                    } {
                        {
                            --data: {
                                --
                            }
                        } {
                            {
                                --"_token": "{{ csrf_token() }}", --
                            }
                        } {
                            {
                                --"category": category, --
                            }
                        } {
                            {
                                --
                            }, --
                        }
                    } {
                        {
                            --success: function(data) {
                                --
                            }
                        } {
                            {
                                --$('#subject').empty();
                                --
                            }
                        }

                        {
                            {
                                --$.each(data, function(index, subcatObj) {
                                        --
                                    }
                                } {
                                    {
                                        --$('#subject').append('<option value ="' + subcatObj.id +
                                            '">' + subcatObj.sub_category_name + '</option>');
                                        --
                                    }
                                } {
                                    {
                                        --
                                    });
                                --
                            }
                        }

                        {
                            {
                                --
                            }, --
                        }
                    } {
                        {
                            --
                        });
                    --
                }
            } {
                {
                    --
                });
            --
        }
    }
});
</script>

@endsection