@php
    $formAction = isset($cmsfield) ? route('cmsfield.update', $cmsfield->id) : route('cmsfield.store');
    $issetCmsfield = isset($cmsfield) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @isset($cmsfield)
                  Edit Cmsfield Details
              @else
                  Add New Cmsfield<Tutor></Tutor>
              @endisset
            </h4>
            {{--<p class="card-cmsfield">Complete your profile</p>--}}
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($cmsfield) @method('PUT') @endisset
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Cmsfield Heading</label>
                    <input id="cms_heading" type="text" name="cms_heading" class="form-control"
                        value="{{ old('cms_heading') ?? ($issetCmsfield ? $cmsfield->cms_heading : '')  }}" required>
                    @error ('cms_heading')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Display Order</label>
                    <input id="display_order" type="number" name="display_order" class="form-control"
                        value="{{ old('display_order') ?? ($issetCmsfield ? $cmsfield->display_order : '')  }}">
                    @error ('display_order')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Button Text</label>
                    <input id="button_text" type="text" name="button_text" class="form-control"
                        value="{{ old('button_text') ?? ($issetCmsfield ? $cmsfield->button_text : '')  }}" required>
                    @error ('button_text')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Cmsfield Description</label>
                    <textarea class="form-control" id="cms_description" name="cms_description" rows="10" required>{{ old('cms_description') ?? ($issetCmsfield ? $cmsfield->cms_description : '')  }}</textarea>
                    @error ('cms_description')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">CMS Image (329x248) : {{isset($cmsfield->cms_image) ? $cmsfield->cms_image : ''}}</label>
                    <input id="cms_image" type="file" name="cms_image" class="form-control"
                           value="{{ old('cms_image') ?? ($issetCmsfield ? $cmsfield->cms_image : '')  }}">
                    <input type="hidden" name="image_url" value="{{isset($cmsfield->cms_image) ? $cmsfield->cms_image : ''}}">
                    @error ('cms_image')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-cmsfield text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>
    @isset($cmsfield)
    {{--<div class="row">--}}
      {{--<div class="col-md-8">--}}
        {{--<div class="card">--}}
          {{--<div class="card-header card-header-primary">--}}
            {{--<h4 class="card-title">--}}
              {{--Delete this Cmsfield--}}
            {{--</h4>--}}
          {{--</div>--}}
          {{--<div class="card-body">--}}
            {{--<form action="{{ route('cmsfield.destroy', $cmsfield->id) }}" method="POST">--}}
                {{--@csrf--}}
                {{--@method('DELETE')--}}
              {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                  {{--Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.--}}
                  {{--All data associated with this item will be erased permanantly.--}}
                {{--</div>--}}
              {{--</div>--}}
              {{--<button type="submit" class="btn btn-primary pull-right">Delete</button>--}}
              {{--<div class="clearfix"></div>--}}
            {{--</form>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
    @endisset
  </div>
</div>
@endsection
