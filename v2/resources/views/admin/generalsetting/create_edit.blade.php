@php
    $formAction = isset($banner) ? route('generalsetting.update', $banner->id) : route('generalsetting.store');
    $issetBanner = isset($banner) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @isset($banner)
                  Edit Details
              @else
                  Add New
              @endisset
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8">
              @isset($banner) @method('PUT') @endisset
              @csrf



              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Commision</label>
                    <input name="commision" id="commision" class="form-control"  value="{{ old('banner') ?? ($issetBanner ? $banner->commision: '')  }}">  
                    @error ('commision')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

             

              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>
@endsection
