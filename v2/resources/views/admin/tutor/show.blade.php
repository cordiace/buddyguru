@extends('layouts.dashboard')

@section('content')
    @include('partials.nav')
    <div class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">
                                Guru Profile
                            </h4>
                            <p class="card-category">Guru profile details</p>
                        </div>
                        <div class="card-body">

                            <div class="row justify-content-md-center">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <img src="{{ asset($user->profile_image) }}" class="img-thumbnail" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Name</label><br>
                                        {{ $user->name }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Email</label><br>
                                        {{ $user->email }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Phone Number</label><br>
                                        {{ $user->phone_number }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Country</label><br>
                                        {{ $user->country->country_name }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Discipline</label><br>
                                        {{ $user->tutor_detail->getCategory->category_name}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Date of birth</label><br>
                                        {{ substr($user->dob,0,-9)}}
                                    </div>
                                </div>
                                {{-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Sub Category</label><br>
                                        {{ $user->tutor_detail->getSubCategory->sub_category_name}}
                                    </div>
                                </div> --}}
                            </div>

                            {{-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Skills</label><br>
                                        {{ $user->tutor_detail->getSkill->skill_name}}
                                    </div>
                                </div>


                            </div> --}}

                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">About Us</label><br>
                                        {{ $user->tutor_detail->about}}
                                    </div>
                                </div>


                            </div> --}}

                            {{-- <div class="row">
                                <div class="col-md-12">
                                    <label class="bmd-label-floating">Experience</label>
                                    <div class="form-group add_another">
                                        @isset($user->tutor_detail->experience)
                                            @php
                                                $exp = json_decode($user->tutor_detail->experience, true);
                                            @endphp
                                            @foreach ($exp as $experience)
                                                <div class="row">
                                                    <div class="col-md-5">
                                                            <label class="">Name</label><br>
                                                            {{ $experience['name'] }}
                                                    </div>
                                                    <div class="col-md-5">
                                                       <label class="">Designation</label><br>
                                                        {{ $experience['designation'] }}
                                                    </div>

                                                    
                                                </div>
                                            @endforeach
                                        @endisset
                                        
                                    </div>
                                </div>
                            </div> --}}




                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
