@php
    $formAction = route('tutor.batch.store', $id);
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
                  Guru Batch
            </h4>
            <p class="card-category">Manage guru batch</p>
            
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}">
                    <label class="bmd-label-floating">Package</label>
                    <select id="package_id" name="package_id" class="form-control">
                      @isset($userPackages)
                      @foreach($userPackages as $value)
                      <option value="{{$value->id}}">{{$value->package_name}}</option>
                      @endforeach
                      @endisset
                    </select>
                    @error ('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}">
                    <label class="bmd-label-floating">Batch Name</label>
                    <input id="batch_name" type="text" name="batch_name" class="form-control"
                        value="">
                    @error ('batch_name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input id="description" type="text" name="description" class="form-control"
                    value="">
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Start</label>
                    <input id="start" type="date" name="start" class="form-control"
                    value="">
                    @error('start')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">End</label>
                    <input id="end" type="date" name="end" class="form-control"
                    value="">
                    @error('end')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Duration</label>
                    <input id="duration" type="number" name="duration" class="form-control"
                    value="">
                    @error('duration')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Payment Type</label>
                    <select id="payment_type" name="payment_type" class="form-control">
                      <option value="monthly">Monthly</option>
                    </select>
                    @error('payment_type')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">No of Participants</label>
                    <input id="no_of_participants" type="number" name="no_of_participants" class="form-control"
                    value="">
                    @error('no_of_participants')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">No of Sessions</label>
                    <input id="no_of_sessions" type="number" name="no_of_sessions" class="form-control"
                    value="">
                    @error('no_of_sessions')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Subscription fee</label>
                    <input id="subscription_fee" type="number" name="subscription_fee" class="form-control"
                    value="">
                    @error('subscription_fee')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
              </div>
              
                  {{-- <div class="row"> --}}
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              {{-- <div class="clearfix"></div> --}}
                  {{-- </div> --}}
            </form>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Package</th>
                  <th>Name</th>
                  <th>Start</th>
                  <th>End</th>
                  <th>Duration</th>
                  <th>Participants</th>
                  <th>Sessions</th>
                  <th class="text-right">Action</th>
                </thead>
                <tbody>
                  @isset($userBatches)
                  @foreach($userBatches as $userBatche)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>
                      @isset($userBatche->packages)
                      @foreach($userBatche->packages as $value)
                      
                      {{$value->package_name}},

                      @endforeach
                      @endisset
                    </td>
                    <td>{{$userBatche->batch_name}}</td>
                    <td>{{$userBatche->start}}</td>
                    <td>{{$userBatche->end}}</td>
                    <td>{{$userBatche->duration}}</td>
                    <td>{{$userBatche->no_of_participants}}</td>
                    <td>{{$userBatche->no_of_sessions}}</td>
                    
                    <td class="td-actions text-right">
                      <a href="{{ route('tutor.batch.delete', $userBatche->id) }}">
                        <button type="button" rel="tooltip" title="Delete Portfolio" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">delete</i>
                        </button>
                      </a>
                      <a href="" class="edit-portfolio" data-id="{{$userBatche->id}}" data-batch_name="{{$userBatche->batch_name}}" data-description="{{$userBatche->description}}" data-start="{{$userBatche->start}}" data-end="{{$userBatche->end}}" data-duration="{{$userBatche->duration}}" data-no_of_participants="{{$userBatche->no_of_participants}}" data-no_of_sessions="{{$userBatche->no_of_sessions}}" data-payment_type="{{$userBatche->payment_type}}" data-subscription_fee="{{$userBatche->subscription_fee}}">
                        <button type="button" rel="tooltip" title="Edit Portfolio" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div id="editModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Batch</h4>
          </div>
          <div class="modal-body">
            <form action="{{ route('tutor.batch.update') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input id="edit-id" type="hidden" name="id" class="form-control" value="">
                  <label class="bmd-label-floating">Batch Name</label>
                  <input id="edit-batch_name" type="text" name="batch_name" class="form-control" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Description</label>
                  <input id="edit-description" type="text" name="description" class="form-control" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Start</label>
                  <input id="edit-start" type="date" name="start" class="form-control" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">End</label>
                  <input id="edit-end" type="date" name="end" class="form-control" value="">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Duration</label>
                  <input id="edit-duration" type="number" name="duration" class="form-control" value="">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Participants</label>
                  <input id="edit-no_of_participants" type="number" name="no_of_participants" class="form-control" value="">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Sessions</label>
                  <input id="edit-no_of_sessions" type="number" name="no_of_sessions" class="form-control" value="">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Payment Type</label>
                  <select id="edit-payment_type" name="payment_type" class="form-control">
                    <option value="monthly">Monthly</option>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Subscription Fee</label>
                  <input id="edit-subscription_fee" type="number" name="subscription_fee" class="form-control" value="">
                </div>
              </div>
              
            </div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
          </form>

          </div>
          
        </div>

      </div>
    </div>

  </div>
</div>
@endsection

@section('custom-scripts')
  <script type="text/javascript">
      $( document ).ready(function() {
        $( ".edit-portfolio" ).on( "click", function(e) {
          e.preventDefault();

          $('#edit-id').val($(this).attr('data-id'));
          $('#edit-batch_name').val($(this).attr('data-batch_name'));
          $('#edit-description').val($(this).attr('data-description'));

          $('#edit-start').val($(this).attr('data-start'));
          $('#edit-end').val($(this).attr('data-end'));
          $('#edit-duration').val($(this).attr('data-duration'));
          $('#edit-no_of_participants').val($(this).attr('data-no_of_participants'));
          $('#edit-no_of_sessions').val($(this).attr('data-no_of_sessions'));
          $('#edit-payment_type').val($(this).attr('data-payment_type'));
          $('#edit-subscription_fee').val($(this).attr('data-subscription_fee'));


          $('#editModal').modal('show');
          });

      });
  </script>
@endsection
