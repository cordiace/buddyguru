@php
    $formAction = route('tutor.portfolio.store', $id);
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
                  Guru Portfolio
            </h4>
            <p class="card-category">Manage guru portfolio</p>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}">
                    <label class="bmd-label-floating">Title</label>
                    <input id="title" type="text" name="title" class="form-control"
                        value="">
                    @error ('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input id="description" type="text" name="description" class="form-control"
                    value="">
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Video</label>
                    <input id="video_url" type="file" name="video_url" class="form-control"
                    value="">
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>

              </div>

                  {{-- <div class="row"> --}}
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              {{-- <div class="clearfix"></div> --}}
                  {{-- </div> --}}
            </form>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Video</th>
                  <th>Video Thumbnail</th>
                  <th class="text-right">Action</th>
                </thead>
                <tbody>
                  @isset($userPortfolios)
                  @foreach($userPortfolios as $userPortfolio)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$userPortfolio->title}}</td>
                    <td>{{$userPortfolio->description}}</td>

                    <td>
                      <a href="" class="view-portfolio" data-video_url="{{asset($userPortfolio->video_url)}}">
                      <button type="button" rel="tooltip" title="Play Video" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">play_arrow</i>
                      </button>
                    </a>
                  </td>
                  <td><img src="{{url('/' .$userPortfolio->video_thumbnail)}}" width=50 height=50 /></td>
                    <td class="td-actions text-right">
                      <a href="{{ route('tutor.portfolio.delete', $userPortfolio->id) }}">
                        <button type="button" rel="tooltip" title="Delete Portfolio" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">delete</i>
                        </button>
                      </a>
                      <a href="" class="edit-portfolio" data-id="{{$userPortfolio->id}}" data-title="{{$userPortfolio->title}}" data-description="{{$userPortfolio->description}}" data-video_url="{{$userPortfolio->video_url}}" data-video_thumbnail="{{$userPortfolio->video_thumbnail}}">
                        <button type="button" rel="tooltip" title="Edit Portfolio" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div id="editModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Portfolio</h4>
          </div>
          <div class="modal-body">
            <form action="{{ route('tutor.portfolio.update') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input id="edit-id" type="hidden" name="id" class="form-control" value="">
                  <label class="bmd-label-floating">Title</label>
                  <input id="edit-title" type="text" name="title" class="form-control" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Description</label>
                  <input id="edit-description" type="text" name="description" class="form-control" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Video</label>
                  <input id="video_url" type="file" name="video_url" class="form-control" value="">
                  <input id="temp_video_url" type="hidden" name="temp_video_url" class="form-control" value="">
                </div>
              </div>

            </div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
          </form>

          </div>

        </div>

      </div>
    </div>

    <div id="playModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">View Video</h4>
          </div>
          <div class="modal-body" id="divVideo">
            <video width="320" height="240" controls>
              <source src="" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          </div>

        </div>

      </div>
    </div>
    {{-- @isset($user)
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this Tutor
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('tutor.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset --}}
  </div>
</div>
@endsection

@section('custom-scripts')
  <script type="text/javascript">
      $( document ).ready(function() {
        $( ".edit-portfolio" ).on( "click", function(e) {
          e.preventDefault();

          $('#edit-id').val($(this).attr('data-id'));
          $('#edit-title').val($(this).attr('data-title'));
          $('#edit-description').val($(this).attr('data-description'));
          $('#temp_video_url').val($(this).attr('data-video_url'));

          $('#editModal').modal('show');
          });

          $( ".view-portfolio" ).on( "click", function(e) {
          e.preventDefault();


          $('source').attr('src',$(this).attr('data-video_url'));
          $("#divVideo video")[0].load();
          $('#playModal').modal('show');

        });
      });
  </script>
@endsection
