@php
    $formAction = route('tutor.package.store', $id);
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
                  Guru Packages
            </h4>
            <p class="card-category">Manage guru package</p>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input id="user_id" type="hidden" name="user_id" class="form-control" value="{{$id}}">
                    <label class="bmd-label-floating">Package Name</label>
                    <input id="package_name" type="text" name="package_name" class="form-control"
                        value="">
                    @error ('package_name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input id="description" type="text" name="description" class="form-control"
                    value="">
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
                {{-- <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Subscription Fee</label>
                    <input id="subscription_fee" type="number" name="subscription_fee" class="form-control"
                    value="">
                    @error('subscription_fee')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div> --}}

                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Batch</label>
                    
                    <select id="batch_id" name="batch_id[]" multiple class="form-control">
                      @isset($userBatches)
                      @foreach($userBatches as $value)
                      <option value="{{$value->id}}">{{$value->batch_name}}</option>
                      @endforeach
                      @endisset
                    </select>
                    
                    
                    @error('batch_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div><div style="font-size: 12px;">Not required,You can add it, Manage batch is in Guru Actions (Previous Menu).</div>
                </div>
              </div>
              
                  {{-- <div class="row"> --}}
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              {{-- <div class="clearfix"></div> --}}
                  {{-- </div> --}}
            </form>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Package Name</th>
                  <th>Description</th>
                  {{-- <th>Subcription fee</th> --}}
                  <th>Batch</th>
                  <th class="text-right">Action</th>
                </thead>
                <tbody>
                  @isset($userPackages)
                  @foreach($userPackages as $userPackage)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$userPackage->package_name}}</td>
                    <td>{{$userPackage->description}}</td>
                    {{-- <td>{{$userPackage->subscription_fee}}</td> --}}
                    <td>
                      @isset($userPackage->batches)
                      @foreach($userPackage->batches as $value)
                      {{$value->batch_name}},
                      @endforeach
                      @endisset
                      
                    </td>
                    <td class="td-actions text-right">
                      <a href="{{ route('tutor.package.delete', $userPackage->id) }}">
                        <button type="button" rel="tooltip" title="Delete package" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">delete</i>
                        </button>
                      </a>
                      <a href="" class="edit-package" data-id="{{$userPackage->id}}" data-name="{{$userPackage->package_name}}" data-description="{{$userPackage->description}}" data-fee="{{$userPackage->subscription_fee}}">
                        <button type="button" rel="tooltip" title="Edit package" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div id="editModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Package</h4>
          </div>
          <div class="modal-body">
            <form action="{{ route('tutor.package.update') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input id="edit-id" type="hidden" name="id" class="form-control" value="">
                  <label class="bmd-label-floating">Package Name</label>
                  <input id="edit-name" type="text" name="package_name" class="form-control" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Description</label>
                  <input id="edit-description" type="text" name="description" class="form-control" value="">
                </div>
              </div>
              {{-- <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Subcription Fee</label>
                  <input id="fee" type="number" name="subscription_fee" class="form-control" value="">
                </div>
              </div> --}}
              <div class="col-md-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Batch</label>
                  <select id="batch_id" name="batch_id[]" multiple class="form-control">
                    @isset($userBatches)
                    @foreach($userBatches as $value)
                    <option value="{{$value->id}}">{{$value->batch_name}}</option>
                    @endforeach
                    @endisset
                  </select>
                
                </div>
              </div> 
            </div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
          </form>

          </div>
          
        </div>

      </div>
    </div>

    {{-- @isset($user)
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this Tutor
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('tutor.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset --}}
  </div>
</div>
@endsection

@section('custom-scripts')
  <script type="text/javascript">

      $( document ).ready(function() {
        $( ".edit-package" ).on( "click", function(e) {
          e.preventDefault();

          $('#edit-id').val($(this).attr('data-id'));
          $('#edit-name').val($(this).attr('data-name'));
          $('#edit-description').val($(this).attr('data-description'));
          // $('#fee').val($(this).attr('data-fee'));

          $('#editModal').modal('show');
          });

      });
  </script>
@endsection
