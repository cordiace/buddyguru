@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <!-- <h4 class="card-title ">Users</h4>
            <p class="card-category"> Here is a subtitle for this table</p> -->
            <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="{{route('tutor.create')}}">
                            <i class="material-icons">supervisor_account</i> Add Guru
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul>

          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th class="text-right">Action</th>
                </thead>
                <tbody>
                  @isset($users)
                  @foreach($users as $user)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td class="td-actions text-right">
                      <a href="{{ route('tutor.edit', $user->id) }}">
                        <button type="button" rel="tooltip" title="Edit Guru" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                      <a href="{{ route('tutor.show', $user->id) }}">
                        <button type="button" rel="tooltip" title="View Profile" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">assignment_ind</i>
                        </button>
                      </a>
                      <a href="{{ route('tutor.portfolio.create', $user->id) }}">
                        <button type="button" rel="tooltip" title="Manage Portfolio" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">face</i>
                        </button>
                      </a>

                      <a href="{{ route('tutor.package.create', $user->id) }}">
                        <button type="button" rel="tooltip" title="Manage Package" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">book</i>
                        </button>
                      </a>

                      <a href="{{ route('tutor.batch.create', $user->id) }}">
                        <button type="button" rel="tooltip" title="Manage Batch" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">source</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
