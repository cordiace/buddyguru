@php
    $formAction = isset($user) ? route('tutor.update', $user->id) : route('tutor.store');
    $issetUser = isset($user) ? 1 : 0;
    $options=$countries;

@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @isset($user)
                  Edit Guru Details
              @else
                  Add New Guru
              @endisset
            </h4>
            <p class="card-category">Complete your profile</p>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($user) @method('PUT') @endisset
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Name</label>
                    <input id="name" type="text" name="name" class="form-control"
                        value="{{ old('name') ?? ($issetUser ? $user->name : '')  }}">
                    @error ('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Country</label>
                    <select name="location" id="location" class="form-control @error('location') is-invalid @enderror">
                      {{--<option value="india">India</option>--}}
                      {{--<option value="uk">UK</option>--}}
                      {{--<option value="us">US</option>--}}
                        <option value=""></option>
                        @foreach ($options as $key => $value)
                        <option value="{{ $value['id'] }}" {{($issetUser && $user->location == $value['id']) ? 'selected' : ''}}>{{ $value['country_name'] }}</option>
                      @endforeach
                    </select>
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? ($issetUser ? $user->email : '')  }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Mobile Number</label>
                    <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') ?? ($issetUser ? $user->phone_number : '')  }}" required autocomplete="phone_number">

                    @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Discipline</label>
                      <select name="category" id="category" class="form-control @error('category') is-invalid @enderror">
                        <option value=""></option>
                          @foreach ($category as $key => $value)
                          <option value="{{ $value['id'] }}" {{($issetUser && $user->tutor_detail->category == $value['id']) ? 'selected' : ''}} >{{ $value['category_name'] }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="bmd-label-floating">Dob</label>
                        <input id="dob" type="date" name="dob" class="form-control"
                               value="{{ old('dob') ?? ($issetUser ? substr($user->dob,0,-9) : '')  }}">
                        @error ('dob')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                  {{-- <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Sub Category </label>
                      <select name="sub_category" id="sub_category" class="form-control @error('sub_category') is-invalid @enderror">
                          <option value=""></option>
                          @isset($user)
                          @foreach ($sub_category as $key => $value)
                          <option value="{{ $value['id'] }}" {{($issetUser && $user->tutor_detail->sub_category == $value['id']) ? 'selected' : ''}} >{{ $value['sub_category_name'] }}</option>
                        @endforeach
                        @endisset
                      </select>
                    </div>
                  </div> --}}
                </div>
                {{-- <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Skills</label>
                      <select name="skills" id="skills" class="form-control @error('skills') is-invalid @enderror">
                          <option value=""></option>
                          
                          @foreach ($skills as $key => $value)
                          <option value="{{ $value['id'] }}" {{($issetUser && $user->tutor_detail->skills == $value['id']) ? 'selected' : ''}}>{{ $value['skill_name'] }}</option>
                        @endforeach
                        
                      </select>
                    </div>
                  </div>
                </div> --}}
                  {{-- <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">About</label>
                      <input id="about" type="text" name="about" class="form-control"
                             value="{{ old('about') ?? ($issetUser ? $user->tutor_detail->about : '')  }}">
                      @error ('about')
                      <p class="text-danger">{{ $message }}</p>
                      @enderror
                    </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                      <label class="bmd-label-floating">Experience</label>
                      <div class="form-group add_another">
                      @isset($user->tutor_detail->experience)
                        @php
                          $i = 0;
                          $exp=json_decode($user->tutor_detail->experience, true);
                          $len = count($exp);
                        @endphp
                        @foreach ( $exp as $experience)
                          <div class="row">
                          <div class="col-md-5">
                              @if($i == 0)
                              <label class="">Name</label>
                              @endif
                              <input id="experience" type="text" name="college_name[]" class="form-control"
                             value="{{ old('experience') ?? ($issetUser ? $experience['name'] : '')  }}">
                      @error ('experience')
                          <p class="text-danger">{{ $message }}</p>
                          @enderror
                          </div>
                          <div class="col-md-5">
                              @if($i == 0)
                              <label class="">Designation</label>
                              @endif
                              <input id="experience" type="text" name="designation[]" class="form-control"
                             value="{{ old('experience') ?? ($issetUser ? $experience['designation'] : '')  }}">
                      @error ('experience')
                      <p class="text-danger">{{ $message }}</p>
                      @enderror
                          </div>

                          @if($i != 0)
                              <div class="col-md-2">
                                <div class="form-group">
                                  <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm">
                                    <i class="material-icons">remove</i>
                                    <div class="ripple-container"></div></button>
                                </div>
                              </div>
                            @endif
                          </div>
                          @php
                            $i++;
                          @endphp
                        @endforeach
                        @endisset
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="text" {{(isset($exp)) ? '':'required' }} name="college_name[]" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="text" {{(isset($exp)) ? '':'required' }} name="designation[]" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button type="button" rel="tooltip" title="" class="add btn btn-primary btn-link btn-sm">
                              <i class="material-icons">add</i>
                              <div class="ripple-container"></div></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    </div>
                  </div>
                </div> --}}
                  <div class="row">
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              <div class="clearfix"></div>
                  </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @isset($user)
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this Guru
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('tutor.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection

@section('custom-scripts')
  <script type="text/javascript">
      $( document ).ready(function() {
          $( ".add" ).on( "click", function(e) {
              e.preventDefault();
              $('.add_another').append('<div class="row"> <div class="col-md-5"> <div class="form-group"> <input type="text" {{(isset($exp)) ? '':'required' }} name="college_name[]" class="form-control"> </div> </div> <div class="col-md-5"> <div class="form-group"> <input type="text" {{(isset($exp)) ? '':'required' }} name="designation[]" class="form-control"> </div> </div> <div class="col-md-2"> <div class="form-group"> <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button> </div> </div> </div>');
          });
          $( "body" ).on( "click",".remove", function(e) {
              e.preventDefault();
              $(this).parent().parent().parent().remove();
          });
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function() {
           $("#category").on('change', function(e) {
             e.preventDefault();
             var category = $('#category').val();
             console.log(category)
             $.ajax({
                 url: "{{route('admin.getadminsubcategory')}}",
                 type:"GET",
                 data:{
                     "_token": "{{ csrf_token() }}",
                     "category": category,
                 },
                 success:function(data){
                     $('#sub_category').empty();

                     $.each(data,function(index,subcatObj){
                         $('#sub_category').append('<option value ="'+subcatObj.id+'">'+subcatObj.sub_category_name+'</option>');
                     });

                 },
           });
      });
      });
  </script>
@endsection
