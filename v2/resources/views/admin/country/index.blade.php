@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            {{--<h4 class="card-title ">Categories</h4>--}}
            <ul class="nav nav-tabs" data-tabs="tabs">
              <li class="nav-item">
                <a class="nav-link active" href="{{route('country.create')}}" >
                  <i class="material-icons">supervisor_account</i> Add Country
                  <div class="ripple-container"></div>
                  <div class="ripple-container"></div></a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Country</th>
                  <th class="text-right" width="">Action</th>
                </thead>
                <tbody>
                  @isset($countries)
                  @foreach($countries as $country)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$country->country_name}}</td>
                    <td class="td-actions" width="5%">
                      <a href="{{ route('country.edit', $country->id) }}">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
