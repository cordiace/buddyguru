@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
@php
$tutor_count=isset($tutor_emails)?count(array_unique($tutor_emails)):0;
$student_count=isset($sessions)?count($sessions):0;
@endphp
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <!-- <h4 class="card-title ">Users</h4>-->
            @if(isset($tutor_count)||isset($student_count))
              @if($student_count>0||$tutor_count>0)
            <p class="card-category"> Email sent successfully to {{$tutor_count}} tutors and {{$student_count}} students</p>
              @else
                <p class="card-category"> No entries found for the time</p>
              @endif
            @else
              <p class="card-category"> No entries found for the time</p>
            @endif

          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".delete-banner" ).on( "click", function(e) {
    e.preventDefault();
    $('#deleteBanner').attr('action',$(this).data("route"));
    $('#deleteBanner').attr('method','POST');
    $('#delete-banner').modal('show');
    });
});
</script>
@endsection
