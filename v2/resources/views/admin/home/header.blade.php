@php
    $issetHeader = isset($header) ? 1 : 0;
@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              General Settings
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('admin.header.update') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" value="{{ isset($header->id) ? $header->id : ''  }}" >
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">(269 x 84) Logo : {{isset($header->logo) ? $header->logo : ''}}</label>
                    <input type="file" name="logo" class="form-control">
                    <input type="hidden" name="logo_temp" value="{{isset($header->logo) ? $header->logo : ''}}">
                    @error ('logo')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Min Donation Amount</label>
                    <input type="number" name="min_amount" class="form-control" value="{{ isset($header->min_amount) ? $header->min_amount : ''  }}">
                    @error ('min_amount')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Copy Right</label>
                    <input type="text" name="copy_right" class="form-control" value="{{ isset($header->copy_right) ? $header->copy_right : ''  }}">
                    @error ('copy_right')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="add_another">
                <label class="bmd-label-floating">Menu</label>
                @isset($header->menu)
                @php
                $menus = unserialize($header->menu);
                @endphp
                @foreach($menus as $menu)
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="text" value="{{$menu['name']}}" required name="name[]" placeholder="Name">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="text" value="{{$menu['link']}}" required name="link[]" placeholder="Link">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm">
                                  <i class="material-icons">remove</i>
                                <div class="ripple-container"></div></button>
                    </div>
                  </div>
                </div>
                @endforeach
                @endisset
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" {{($issetHeader) ? '':'required' }} name="name[]" placeholder="Name">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" {{($issetHeader) ? '':'required' }} name="link[]" placeholder="Link">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <button type="button" rel="tooltip" title="" class="add btn btn-primary btn-link btn-sm">
                                <i class="material-icons">add</i>
                              <div class="ripple-container"></div></button>
                  </div>
                </div>
              </div>
              </div>

              <!-- Social Links -->
              <div class="add_another_social_link">
                <label class="bmd-label-floating">Social Links</label>
                @isset($header->social_links)
                @php
                $social_links = unserialize($header->social_links);
                @endphp
                @foreach($social_links as $social_link)
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="text" value="{{$social_link['name']}}" required name="social_name[]" placeholder="Name">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="text" value="{{$social_link['link']}}" required name="social_link[]" placeholder="Link">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm">
                                  <i class="material-icons">remove</i>
                                <div class="ripple-container"></div></button>
                    </div>
                  </div>
                </div>
                @endforeach
                @endisset
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" {{($issetHeader) ? '':'required' }} name="social_name[]" placeholder="Name">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" {{($issetHeader) ? '':'required' }} name="social_link[]" placeholder="Link">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <button type="button" rel="tooltip" title="" class="add_social btn btn-primary btn-link btn-sm">
                                <i class="material-icons">add</i>
                              <div class="ripple-container"></div></button>
                  </div>
                </div>
              </div>
              </div>

              <!-- Short Links -->
              <div class="add_another_short_link">
                <label class="bmd-label-floating">Short Links</label>
                @isset($header->short_links)
                @php
                $short_links = unserialize($header->short_links);
                @endphp
                @foreach($short_links as $short_link)
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="text" value="{{$short_link['name']}}" required name="short_name[]" placeholder="Name">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <input type="text" value="{{$short_link['link']}}" required name="short_link[]" placeholder="Link">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm">
                                  <i class="material-icons">remove</i>
                                <div class="ripple-container"></div></button>
                    </div>
                  </div>
                </div>
                @endforeach
                @endisset
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" {{($issetHeader) ? '':'required' }} name="short_name[]" placeholder="Name">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" {{($issetHeader) ? '':'required' }} name="short_link[]" placeholder="Link">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <button type="button" rel="tooltip" title="" class="add_short btn btn-primary btn-link btn-sm">
                                <i class="material-icons">add</i>
                              <div class="ripple-container"></div></button>
                  </div>
                </div>
              </div>
              </div>

              <!-- footer app links -->
              <label class="bmd-label-floating">Footer App Links</label>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Play Store link</label>
                    <input type="text" name="play_store_link" class="form-control" value="{{ isset($header->play_store_link) ? $header->play_store_link : ''  }}">
                    @error ('play_store_link')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label class="bmd-label-floating">App Store link</label>
                    <input type="text" name="app_store_link" class="form-control" value="{{ isset($header->app_store_link) ? $header->app_store_link : ''  }}">
                    @error ('app_store_link')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">(65 x 54) Footer Logo : {{isset($header->footer_logo) ? $header->footer_logo : ''}}</label>
                    <input type="file" name="footer_logo" class="form-control">
                    <input type="hidden" name="footer_logo_temp" value="{{isset($header->footer_logo) ? $header->footer_logo : ''}}">
                    @error ('footer_logo')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <!-- terms and conditions -->
              <label class="bmd-label-floating">Terms and conditions</label>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Title</label>
                    <input type="text" name="title_terms" class="form-control" value="{{ isset($header->title_terms) ? $header->title_terms : ''  }}">
                    @error ('title_terms')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <textarea rows="5" name="description_terms" class="form-control">{{ isset($header->description_terms) ? $header->description_terms : ''  }}</textarea>
                    @error ('description_terms')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>

              <!-- our stream -->
              <label class="bmd-label-floating">Our Stream</label>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Title</label>
                    <input type="text" name="title_stream" class="form-control" value="{{ isset($header->title_stream) ? $header->title_stream : ''  }}">
                    @error ('title_stream')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label class="bmd-label-floating">Description</label>
                    <textarea rows="5" name="description_stream" class="form-control">{{ isset($header->description_stream) ? $header->description_stream : ''  }}</textarea>
                    @error ('description_stream')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>


              <button type="submit" class="btn btn-primary btn-brown pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="../assets/img/faces/marc.jpg" />
            </a>
          </div>
          <div class="card-body">
            <h6 class="card-category text-gray">CEO / Co-Founder</h6>
            <h4 class="card-title">Alec Thompson</h4>
            <p class="card-description">
              Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
            </p>
            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
          </div>
        </div>
      </div> -->
    </div>

  </div>
</div>
@endsection
@section('custom-scripts')
<script type="text/javascript">
$( document ).ready(function() {
  $( ".add" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( ".add_social" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another_social_link').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="social_name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="social_link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( ".add_short" ).on( "click", function(e) {
    e.preventDefault();
    $('.add_another_short_link').append('<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" required name="short_name[]" placeholder="Name"></div></div><div class="col-md-4"><div class="form-group"><input type="text" required name="short_link[]" placeholder="Link"></div></div><div class="col-md-4"><div class="form-group"><button type="button" rel="tooltip" title="" class="remove btn btn-primary btn-link btn-sm"><i class="material-icons">remove</i><div class="ripple-container"></div></button></div></div></div>');
  });
  $( "body" ).on( "click",".remove", function(e) {
    e.preventDefault();
    $(this).parent().parent().parent().remove();
  });
});
</script>
@endsection
