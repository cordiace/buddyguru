@php
    $formAction = isset($user) ? route('user.update', $user->id) : route('user.store');
    $issetUser = isset($user) ? 1 : 0;
        $options=$countries;
    $grades=$grade;

@endphp
@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              @isset($user)
                  Edit Details
              @else
                  Add New User
              @endisset
            </h4>
            <p class="card-category">Complete your profile</p>
          </div>
          <div class="card-body">
            <form action="{{ $formAction }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
              @isset($user) @method('PUT') @endisset
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Name</label>
                    <input id="name" type="text" name="name" class="form-control"
                        value="{{ old('name') ?? ($issetUser ? $user->name : '')  }}">
                    @error ('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label class="bmd-label-floating">Dob</label>
                          <input id="dob" type="date" name="dob" class="form-control"
                                 value="{{ old('dob') ?? ($issetUser ? substr($user->dob,0,-9) : '')  }}">
                          @error ('dob')
                          <p class="text-danger">{{ $message }}</p>
                          @enderror
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? ($issetUser ? $user->email : '')  }}" required autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Phone Number</label>
                    <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') ?? ($issetUser ? $user->phone_number : '')  }}" required autocomplete="phone_number">

                    @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" {{($issetUser) ? '' : 'required'}} autocomplete="new-password">
                  </div>
                </div>
              </div>

              <!-- <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Parent Name</label>
                    <input id="parent_name" type="text" name="parent_name" class="form-control"
                           value="{{ old('parent_name') ?? ($issetUser ? $user->student_detail->parent_name : '')  }}">
                    @error ('parent_name')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Parent Email</label>
                    <input id="parent_email" type="text" name="parent_email" class="form-control"
                           value="{{ old('parent_email') ?? ($issetUser ? $user->student_detail->parent_email : '')  }}">
                    @error ('parent_email')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div> -->

              <div class="row">
                <!-- <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Parent Mobile</label>
                    <input id="parent_mobile" type="text" name="parent_mobile" class="form-control"
                           value="{{ old('parent_mobile') ?? ($issetUser ? $user->student_detail->parent_mobile : '')  }}">
                    @error ('parent_mobile')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                  </div>
                </div> -->
                  <div class="col-md-12">
                      <div class="form-group">
                          <label class="bmd-label-floating">Country</label>
                          <select name="location" id="location" class="form-control @error('location') is-invalid @enderror">
                              <option value=""></option>
                              @foreach ($options as $key => $value)
                                  <option value="{{ $value['id'] }}" {{($issetUser && $user->location == $value['id']) ? 'selected' : ''}}>{{ $value['country_name'] }}</option>
                              @endforeach
                          </select>
                          @error('location')
                          <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                      </div>
                  </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Skills</label>
                      <select name="skills" id="skills" class="form-control @error('skills') is-invalid @enderror">
                          <option value=""></option>
                          @foreach ($skills as $key => $value)
                              <option value="{{ $value['id'] }}" {{($issetUser && $user->student_detail->skills == $value['id']) ? 'selected' : ''}} >{{ $value['skill_name'] }}</option>
                          @endforeach
                      </select>
                  </div>
                </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label class="bmd-label-floating">Grade</label>
                          <select name="grade" id="grade" class="form-control @error('grade') is-invalid @enderror">
                              <option value=""></option>
                              @foreach ($grades as $key => $value)
                                  <option value="{{ $value['id'] }}" {{($issetUser && $user->student_detail->grade == $value['id']) ? 'selected' : ''}}>{{ $value['grade_name'] }}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>

              </div>


              <button type="submit" class="btn btn-primary pull-right">Submit</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @isset($user)
    <div class="row d-flex justify-content-center">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">
              Delete this User
            </h4>
          </div>
          <div class="card-body">
            <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <div class="row">
                <div class="col-md-12">
                  Do you really want to do this ? Make sure you are selected the right item. This action is irreversible.
                  All data associated with this item will be erased permanantly.
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Delete</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection
