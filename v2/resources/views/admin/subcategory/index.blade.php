@extends('layouts.dashboard')

@section('content')
@include('partials.nav')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            {{--<h4 class="card-title ">Sub Categories</h4>--}}
            <ul class="nav nav-tabs" data-tabs="tabs">
              <li class="nav-item">
                <a class="nav-link active" href="{{route('subcategory.create')}}" >
                  <i class="material-icons">supervisor_account</i> Add Sub Category
                  <div class="ripple-container"></div>
                  <div class="ripple-container"></div></a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>ID</th>
                  <th>Category</th>
                  <th>Sub Category</th>
                  <th class="text-right" width="">Action</th>
                </thead>
                <tbody>
                  @isset($sub_categories)
                  @foreach($sub_categories as $sub_category)
                  <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$sub_category->category->category_name}}</td>
                    <td>{{$sub_category->sub_category_name}}</td>
                    <td class="td-actions" width="5%">
                      <a href="{{ route('subcategory.edit', $sub_category->id) }}">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                  @endisset
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
