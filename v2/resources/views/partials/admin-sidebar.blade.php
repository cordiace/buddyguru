<ul class="nav">
  <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.home')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('admin.home')}}">
      <i class="material-icons">dashboard</i>
      <p>Dashboard</p>
    </a>
  </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'user')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('user.index')}}">
      <i class="material-icons">school</i>
      <p>Buddy Profile</p>
    </a>
  </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'tutor')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('tutor.index')}}">
      <i class="material-icons">record_voice_over</i>
      <p>Guru Profile</p>
    </a>
  </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'sessions') !== false) ? 'active' :''}}">
  <a class="nav-link" href="{{route('sessions.index')}}">
  <i class="material-icons">assessment</i>
  <p>Sessions</p>
  </a>
  {{-- </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'payment') !== false) ? 'active' :''}}">
  <a class="nav-link" href="{{route('payment.index')}}">
  <i class="material-icons">money</i>
  <p>Payments</p>
  </a>
</li> --}}
  <li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples" aria-expanded="{{(strpos(\Request::route()->getName(),'subcategory') !== false)||
              (strpos(\Request::route()->getName(),'category') !== false)||
              (strpos(\Request::route()->getName(),'skill') !== false)||
              (strpos(\Request::route()->getName(),'country') !== false)||
              (strpos(\Request::route()->getName(),'cmsfield') !== false)
              ? 'true' :'false'}}">
      <i class="material-icons">image</i>
      <p> CMS
        <b class="caret"></b>
      </p>
    </a>
    <div class="collapse {{(strpos(\Request::route()->getName(),'subcategory') !== false)||
              (strpos(\Request::route()->getName(),'category') !== false)||
              (strpos(\Request::route()->getName(),'skill') !== false)||
              (strpos(\Request::route()->getName(),'country') !== false)||
              (strpos(\Request::route()->getName(),'cmsfield') !== false)
              ? 'show' :''}}" id="pagesExamples" style="">
      <ul class="nav">
        <li class="nav-item {{(strpos(\Request::route()->getName(),'banner') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('banner.index')}}">
            <i class="material-icons">category</i>
            <p>Banners</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'category') !== false)&&(strpos(\Request::route()->getName(),'subcategory') !== 0) ? 'active' :''}}">
          <a class="nav-link" href="{{route('category.index')}}">
            <i class="material-icons">category</i>
            <p>Categories</p>
          </a>
        </li>
        {{--<li class="nav-item {{(strpos(\Request::route()->getName(),'subcategory') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('subcategory.index')}}">
            <i class="material-icons">subdirectory_arrow_left</i>
            <p>Sub Categories</p>
          </a>
        </li> --}}
        {{--<li class="nav-item {{(strpos(\Request::route()->getName(),'grade') !== false) ? 'active' :''}}">--}}
        {{--<a class="nav-link" href="{{route('grade.index')}}">--}}
        {{--<i class="material-icons">grade</i>--}}
        {{--<p>Grade</p>--}}
        {{--</a>--}}
        {{--</li>--}}
        <li class="nav-item {{(strpos(\Request::route()->getName(),'skill') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('skill.index')}}">
            <i class="material-icons">workspaces</i>
            <p>Skills</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'terms') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('terms.index')}}">
            <i class="material-icons">workspaces</i>
            <p>Terms and Conditions</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'policy') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('policy.index')}}">
            <i class="material-icons">workspaces</i>
            <p>Privacy Policy</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'country') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('country.index')}}">
            <i class="material-icons">map</i>
            <p>Countries</p>
          </a>
        </li>
        <li class="nav-item {{(strpos(\Request::route()->getName(),'cmsfield') !== false) ? 'active' :''}}">
          <a class="nav-link" href="{{route('cmsfield.index')}}">
            <i class="material-icons">picture_in_picture</i>
            <p>Info Screens</p>
          </a>
        </li>
      </ul>
    </div>
  </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.helpdesk')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('admin.helpdesk')}}">
      <i class="material-icons">record_voice_over</i>
      <p>Help Desk</p>
    </a>
  </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.generalsetting')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('generalsetting.index')}}">
      <i class="material-icons">record_voice_over</i>
      <p>Commission</p>
    </a>
  </li>
  <li class="nav-item {{(strpos(\Request::route()->getName(),'admin.recentbooking')!== false) ? 'active' :''}}">
    <a class="nav-link" href="{{route('recentbooking.index')}}">
      <i class="material-icons">record_voice_over</i>
      <p>Recent Booking</p>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('logout')}}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
      <i class="material-icons">person</i>
      <p>Log Out</p>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </a>
  </li>
</ul>
