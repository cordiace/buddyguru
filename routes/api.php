<?php
use App\Http\Controllers\Admin\TutorController;
use App\Http\Controllers\Admin\GeneralsettingController;
use App\Http\Controllers\APIController\ClassesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

 Route::middleware('auth:api')->get('/user', function (Request $request) {
     return $request->user();
     });
//Guru Registration

Route::post('tutor-register',[TutorController::class, 'tutorRegister']);
// Route::post('tutor-register','ApiController@tutorRegister');

Route::post('tutor-update',[TutorController::class, 'tutorUpdate']);
Route::get('tutor/delete/{id}',[TutorController::class, 'tutorDelete']);



//Classes
Route::get('tutor-guru-class',[ClassesController::class, 'ShowGuruClass']);
Route::get('tutor-module-class',[ClassesController::class, 'ShowModuleClass']);
Route::get('tutor-live-class',[ClassesController::class, 'ShowLiveClass']);


//portfolio
Route::get('portfolio-view',[TutorController::class, 'portfolioView']);
Route::post('portfolio-create',[TutorController::class, 'portfolioCreate']);

Route::post('portfolio-update',[TutorController::class, 'portfolioUpdate']);
Route::get('portfolio-delete/{id}',[TutorController::class, 'portfolioDelete']);

//currency setting

Route::post('currency-update',[GeneralsettingController::class, 'UpdateCurrencySetting']);
Route::get('view-currency-range',[GeneralsettingController::class, 'ShowCurrencyRange']);

Route::get('view-tier-management',[GeneralsettingController::class, 'ShowTierManagement']);


//
Route::get('get-regions', 'ApiController@getAllRegions');
Route::get('get-countries', 'ApiController@getAllCountries');
Route::get('get-skills', 'ApiController@getAllSkills');
Route::get('get-categories', 'ApiController@getAllCategories');


Route::get('get-access-token', 'CommonController@getAccessToken');

Route::get('get-all-tutors', 'ApiController@getAllTutor');

Route::post('get-payment', 'ApiController@getSubscription');
Route::get('get-terms-and-policy', 'ApiController@getTermsAndPolicy');

Route::post('user-login', 'ApiController@userLogin');
Route::post('student-add', 'ApiController@studentRegister');
Route::post('student-edit', 'ApiController@studentEditProfile');
Route::post('tutor-add', 'ApiController@tutorRegister');
Route::post('tutor-edit', 'ApiController@tutorEditProfile');
// Route::get('get-session', 'ApiController@getSession');
Route::post('forgot-password', 'ApiController@forgotPassword');
Route::post('reset-password', 'ApiController@resetPassword');
Route::post('change-password', 'ApiController@changePassword');
Route::get('get-tutor', 'ApiController@getTutor');

Route::get('get-student', 'ApiController@getStudent');
Route::get('get-earning', 'ApiController@getEarning');
// Route::post('create-session', 'ApiController@createSession');
// Route::post('update-session', 'ApiController@updateSession');
// Route::get('delete-session', 'ApiController@deleteSession');
Route::get('tutor-upcoming', 'ApiController@tutorUpcomingSessions');
Route::get('tutor-completed', 'ApiController@tutorCompletedSessions');
Route::get('tutor-all-session', 'ApiController@tutorAllSessions');
Route::get('tutor-session-by-id', 'ApiController@tutorSessionsById');
Route::get('tutor-upcoming-sessions', 'ApiController@tutorSessions');
Route::get('student-sessions', 'ApiController@studentSessions');
Route::get('student-booking', 'ApiController@studentBatches');
Route::get('get-countries', 'ApiController@getAllCountries');
Route::get('get-skills', 'ApiController@getAllSkills');
Route::get('get-grades', 'ApiController@getAllGrades');
Route::get('get-categories', 'ApiController@getAllCategories');
Route::get('get-sub-categories', 'ApiController@getSubCategories');
Route::get('get-cms', 'ApiController@getCms');
Route::get('recommended-tutors', 'ApiController@recommendedTutors');
Route::get('suggested-videos', 'ApiController@suggestedVideos');
Route::post('upload-image', 'ApiController@uploadImage');
Route::get('search-sessions', 'ApiController@searchSessions');
Route::post('payment-create', 'ApiController@createPayment');
Route::post('payment-refund', 'ApiController@createRefund');
Route::post('otp-verification', 'ApiController@verifyOtp');
Route::post('logout', 'UserController@logoutApi');
Route::post('create-portfolio', 'ApiController@createPortfolio');
Route::post('edit-portfolio', 'ApiController@editPortfolio');
Route::get('get-portfolio', 'ApiController@getPortfolio');
Route::get('delete-portfolio', 'ApiController@deletePortfolio');
Route::get('get-portfolio-by-tutor-id', 'ApiController@tutorPortfolioById');
Route::post('search-tutor', 'ApiController@searchPortfolio');
Route::post('add-rating', 'ApiController@addRating');

Route::post('create-package', 'ApiController@createPackage');
Route::post('update-package', 'ApiController@updatePackage');
Route::get('delete-package', 'ApiController@deletePackage');
Route::get('get-packages', 'ApiController@getPackages');
Route::get('get-packages-by-tutor', 'ApiController@getPackagesByTutor');
Route::get('get-packages-by-id', 'ApiController@getPackageById');

Route::post('create-batch', 'ApiController@createBatch');
Route::post('update-batch', 'ApiController@updateBatch');
Route::get('delete-batch', 'ApiController@deleteBatch');
Route::get('get-batches', 'ApiController@getBatches');
Route::get('get-batches-by-tutor', 'ApiController@getBatchesByTutor');
Route::get('get-batch-by-id', 'ApiController@getBatchById');
Route::get('get-video-by-category', 'ApiController@getVideoByCategory');

Route::post('create-session', 'ApiController@createSession');
Route::post('update-session', 'ApiController@updateSession');
Route::get('delete-session', 'ApiController@deleteSession');
Route::get('get-session', 'ApiController@getSessions');
Route::get('get-all-sessions', 'ApiController@getAllSessions');
Route::get('get-upcoming-sessions', 'ApiController@getAllUpcomingSessions');
Route::get('get-banners', 'ApiController@getBanners');
Route::get('get-session-by-id', 'ApiController@getSessionById');
Route::get('get-session-by-batch', 'ApiController@getSessionByBatch');
Route::post('create-review', 'ApiController@createReview');

Route::post('subscribe-tutor', 'ApiController@subscribeTutor');
Route::get('get-subscribed-tutor', 'ApiController@getSubscriber');
Route::get('get-all-subscribed-tutor', 'ApiController@getAllSubscriber');

Route::post('create-meeting', 'ApiController@createMeeting');
Route::get('zoom-callback', 'ApiController@zoomCallback');

// Get list of meetings.
Route::get('/meetings', 'Zoom\MeetingController@list');

// Create meeting room using topic, agenda, start_time.
Route::post('/meetings', 'Zoom\MeetingController@create');

// Get information of the meeting room by ID.
Route::get('/meetings/{id}', 'Zoom\MeetingController@get')->where('id', '[0-9]+');
Route::patch('/meetings/{id}', 'Zoom\MeetingController@update')->where('id', '[0-9]+');
Route::delete('/meetings/{id}', 'Zoom\MeetingController@delete')->where('id', '[0-9]+');
