<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test/page',function(){
    dd(phpinfo());
});
Route::get('/test/page/video','Admin\GeneralsettingController@createvideo');
Route::post('/admin/tutor/file','Admin\GeneralsettingController@uploadLargeFiles')->name('files.upload.large');

Route::post('/file-upload/profile', 'Admin\TutorController@storeProfileVideo')->name('profile.StoreVideo');
Route::post('/file-upload/{id}', 'Admin\TutorController@storePortFolioVideo')->name('portfolio.StoreVideo');
Route::post('/file-upload/package/{id}', 'Admin\TutorController@storePackageVideo')->name('package.StoreVideo');
Route::post('/file-upload/module/{id}', 'Admin\TutorController@storeModuleVideo')->name('module.StoreVideo');
Route::post('/file-upload/live/{id}', 'Admin\TutorController@storeLiveVideo')->name('live.StoreVideo');
  
Route::get('/', 'Auth\LoginController@showLoginForm')->name('welcome');
Route::get('/login1', 'Auth\LoginController@showLoginForm')->name('welcome');

Auth::routes();

Route::get('/home', 'Admin\AdminController@index')->name('admin.home');
Route::get('/terms-and-conditions', 'CommonController@terms')->name('common.terms');
Route::get('/privacy-policy', 'CommonController@policy')->name('common.policy');
Route::get('/get-batch-by-user', 'CommonController@getBatch')->name('common.getBatch');

//SuperAdmin Routes
Route::get('/admin', 'Admin\AdminController@index')->name('admin.home');
Route::get('/admin/help-desk', 'Admin\AdminController@helpDesk')->name('admin.helpdesk');
Route::resource('/admin/terms',  'Admin\TermsController')->except(['show']);
Route::resource('/admin/policy',  'Admin\PolicyController')->except(['show']);
Route::resource('/admin/user',  'Admin\UserController');

Route::middleware('auth')->group(function () {
Route::resource('/admin/tutor',  'Admin\TutorController');
 
// Route::get('/admin/tutor',  'Admin\TutorController@index')->name('tutor.index');
// Route::get('/admin/tutor/{id}',  'Admin\TutorController@show')->name('tutor.show');
// Route::get('/admin/tutor/create/{showNext}',  'Admin\TutorController@createProfile')->name('admin.tutor.create');
Route::get('/admin/tutor/edit/{id}/{showNext}',  'Admin\TutorController@edit')->name('admin.tutor.edit');
Route::post('/admin/tutor/update/{id}/{showNext}',  'Admin\TutorController@update')->name('admin.tutor.update');
Route::get('/admin/tutor/delete/{id}',  'Admin\TutorController@destroy')->name('admin.tutor.delete');

Route::resource('/admin/sessions',  'Admin\SessionsController')->except(['show']);
Route::resource('/admin/payment',  'Admin\PaymentController')->except(['show']);
//skills and category
Route::resource('/admin/category',  'Admin\CategoryController')->except(['show']); 
Route::get('/admin/category/delete/{id}',  'Admin\CategoryController@delete')->name('category.delete'); 

Route::resource('/admin/skill',  'Admin\SkillController')->except(['show']);
Route::get('/admin/skill/delete/{id}',  'Admin\SkillController@delete')->name('skill.delete');

Route::resource('/admin/banner',  'Admin\BannerController')->except(['show']);
Route::resource('/admin/subcategory',  'Admin\SubCategoryController')->except(['show']);



Route::resource('/admin/region',  'Admin\RegionController')->except(['show']);
Route::get('/admin/region/delete/{id}',  'Admin\RegionController@delete')->name('region.delete');
Route::resource('/admin/country',  'Admin\CountryController')->except(['show']);
Route::get('/admin/country/delete/{id}',  'Admin\CountryController@delete')->name('country.delete');

Route::resource('/admin/grade',  'Admin\GradeController')->except(['show']);
Route::resource('/admin/cmsfield',  'Admin\CmsFieldController')->except(['show']);
Route::get('/admin/get_subcategory',  'Admin\SubCategoryController@getSubCategory')->name('admin.getsubcategory');
Route::get('/admin/get_adminsubcategory',  'Admin\SubCategoryController@getAdminSubCategory')->name('admin.getadminsubcategory');
Route::get('/admin/profile', 'Admin\AdminController@profile')->name('admin.profile');
Route::get('/admin/send', 'Admin\EmailController@send')->name('email.send');
Route::post('/admin/profile/update', 'Admin\AdminController@profileUpdate')->name('admin.profile.update');
Route::resource('/admin/admin-banner',  'Admin\BannerController')->except(['show']);

Route::resource('/admin/generalsetting',  'Admin\GeneralsettingController')->except(['show']);
Route::get('/admin/generalsetting/view-currency',  'Admin\GeneralsettingController@viewCurrency')->name('admin.settings.viewCurrency');
Route::post('/admin/generalsetting/view-currency/store',  'Admin\GeneralsettingController@currencyStore')->name('admin.settings.currencyStore');
Route::post('/admin/generalsetting/view-currency/update',  'Admin\GeneralsettingController@currencyUpdate')->name('admin.settings.currencyUpdate');

Route::get('/admin/generalsetting/view-currency/create',  'Admin\GeneralsettingController@createCurrency')->name('admin.currency_settings.createCurrency');
Route::post('/admin/generalsetting/storeCurrency',  'Admin\GeneralsettingController@storeCurrency')->name('admin.currency_settings.storeCurrency');
Route::get('/admin/generalsetting/view-currency/{id}/edit',  'Admin\GeneralsettingController@editCurrency')->name('admin.currency_settings.editCurrency');
Route::post('/admin/generalsetting/updateCurrency/{id}',  'Admin\GeneralsettingController@updateCurrency')->name('admin.currency_settings.updateCurrency');
Route::get('/admin/generalsetting/deleteCurrency/{id}',  'Admin\GeneralsettingController@deleteCurrency')->name('admin.currency_settings.deleteCurrency');
Route::get('/admin/generalsetting/tier',  'Admin\GeneralsettingController@get_tier')->name('admin.tier.index');
Route::get('/admin/generalsetting/tier/create',  'Admin\GeneralsettingController@createTier')->name('admin.tier.create');
Route::post('/admin/generalsetting/tier',  'Admin\GeneralsettingController@storeTier')->name('admin.tier.storeTier');
Route::get('/admin/generalsetting/tier/{id}/edit', 'Admin\GeneralsettingController@editTier')->name('admin.tier.edit');
Route::post('/admin/generalsetting/tier/update/{id}',  'Admin\GeneralsettingController@updateTier')->name('admin.tier.update');
Route::get('/admin/generalsetting/tier/delete/{id}',  'Admin\GeneralsettingController@deleteTier')->name('admin.tier.delete');
Route::get('/admin/generalsetting/tier/currencyRange',  'Admin\GeneralsettingController@view_currency_range')->name('admin.tier.currency_range.index');
Route::get('/admin/generalsetting/tier/currencyRange/create',  'Admin\GeneralsettingController@createTierCurrency')->name('admin.tier.currency_range.create');
Route::post('/admin/generalsetting/tier/currencyRange/store',  'Admin\GeneralsettingController@StoreTierCurrency')->name('admin.tier.currency_range.store');
Route::get('/admin/generalsetting/tier/currencyRange/{id}/edit',  'Admin\GeneralsettingController@editTierCurrency')->name('admin.tier.currency_range.edit');
Route::post('/admin/generalsetting/tier/currencyRange/update/{id}',  'Admin\GeneralsettingController@updateTierCurrency')->name('admin.tier.currency_range.update');
Route::get('/admin/generalsetting/tier/currencyRange/delete/{id}',  'Admin\GeneralsettingController@deleteTierCurrency')->name('admin.tier.currency_range.delete');


Route::resource('/admin/recentbooking',  'Admin\RecentbookingController')->except(['show']);
Route::get('/get_admin_payment',  'Admin\PaymentController@get_admin_payment')->name('get_admin_payment');
Route::get('/admin/refund-payment/{id}', 'Admin\PaymentController@refundPayment')->name('buddy.refund.payment');
Route::get('/get_admin_recentbooking',  'Admin\RecentbookingController@get_admin_recentbooking')->name('get_admin_recentbooking');

Route::get('/admin/tutor/portfolio/create/{id}/{showNext}', 'Admin\TutorController@createPortfolio')->name('tutor.portfolio.create');
Route::post('/admin/tutor/portfolio/{showNext}', 'Admin\TutorController@storePortfolio')->name('tutor.portfolio.store');
Route::get('/admin/tutor/portfolio/{id}/edit', 'Admin\TutorController@editPortfolio')->name('tutor.portfolio.edit');
Route::post('/admin/tutor/portfolio/update/{id}', 'Admin\TutorController@updatePortfolio')->name('tutor.portfolio.update');
Route::get('/admin/tutor/portfolio/delete/{id}', 'Admin\TutorController@deletePortfolio')->name('tutor.portfolio.delete');

Route::get('/admin/tutor/package/index/{id}', 'Admin\TutorController@viewPackage')->name('tutor.package.view');
Route::get('/admin/tutor/package/view/{id}', 'Admin\TutorController@showPackage')->name('tutor.package.show');
Route::get('/admin/tutor/package/create/{id}/{showNext}', 'Admin\TutorController@createPackage')->name('tutor.package.create');
Route::get('/admin/tutor/package/{id}/edit', 'Admin\TutorController@editPackage')->name('tutor.package.edit');
Route::post('/admin/tutor/package/{showNext}', 'Admin\TutorController@storePackage')->name('tutor.package.store');
Route::post('/admin/tutor/package/update/{id}', 'Admin\TutorController@updatePackage')->name('tutor.package.update');
Route::get('/admin/tutor/package/delete/{id}', 'Admin\TutorController@deletePackage')->name('tutor.package.delete');

Route::get('/admin/tutor/module/view/{id}', 'Admin\TutorController@showModule')->name('tutor.module.show');
Route::get('/admin/tutor/module/index/{id}', 'Admin\TutorController@viewModule')->name('tutor.module.view');
Route::get('/admin/tutor/module/create/{id}/{showNext}', 'Admin\TutorController@createModule')->name('tutor.module.create');
Route::get('/admin/tutor/module/{id}/edit', 'Admin\TutorController@editModule')->name('tutor.module.edit');
Route::post('/admin/tutor/module/{showNext}', 'Admin\TutorController@storeModule')->name('tutor.module.store');
Route::post('/admin/tutor/module/update/{id}', 'Admin\TutorController@updateModule')->name('tutor.module.update');
Route::get('/admin/tutor/module/delete/{id}', 'Admin\TutorController@deleteModule')->name('tutor.module.delete');

Route::get('/admin/tutor/liveclass/view/{id}', 'Admin\TutorController@showLiveClass')->name('tutor.live_class.show');
Route::get('/admin/tutor/liveclass/index/{id}', 'Admin\TutorController@viewLiveClass')->name('tutor.live_class.view');
Route::get('/admin/tutor/liveclass/create/{id}/{showNext}', 'Admin\TutorController@createLiveClass')->name('tutor.live_class.create');
Route::post('/admin/tutor/liveclass/{showNext}', 'Admin\TutorController@storeLiveClass')->name('tutor.live_class.store');
Route::get('/admin/tutor/liveclass/edit/{id}', 'Admin\TutorController@editLiveClass')->name('tutor.live_class.edit');
Route::post('/admin/tutor/liveclass/update/{id}', 'Admin\TutorController@updateLiveClass')->name('tutor.live_class.update');
Route::get('/admin/tutor/liveclass/delete/{id}', 'Admin\TutorController@deleteBatch')->name('tutor.batch.delete');

  
});
